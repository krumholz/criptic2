# Makefile for criptic
.PHONY: all debug criptic criptic-debug clean setprob

# Specify which problem to build
PROB ?= NONE

# Set MPI or non-MPI mode
MPI ?= ENABLE_MPI

# Set openMP or non-openMP mode
OPENMP ?= ENABLE_OPENMP

# Optional verbosity level
VERBOSITY =

# Optional set neighbor number
NUMNGB =

# Optionally set machine name to pick machine-specific makefile
MACHINE =

# Set options to pass to next makefile
OPTIONS =
ifdef MACHINE
	OPTIONS += MACHINE=$(MACHINE)
endif
ifdef VERBOSITY
	OPTIONS += VERBOSITY=$(VERBOSITY)
endif
ifdef NUMNGB
	OPTIONS += NUMNGB=$(NUMNGB)
endif

all: criptic

debug: criptic-debug

criptic: setprob
	cd src && $(MAKE) MPI=$(MPI) OPENMP=$(OPENMP) $(OPTIONS)
	cp src/criptic run/

criptic-debug: setprob
	cd src && $(MAKE) debug MPI=$(MPI) OPENMP=$(OPENMP) $(OPTIONS)
	cp src/criptic run/

clean:
	cd src && $(MAKE) clean

setprob: run
ifneq ($(PROB),NONE)
	@echo "*** Setting problem to $(PROB) ***"
	@cmp -s prob/$(PROB)/Prob.cpp src/Prob.cpp; \
	RETVAL=$$?; \
	if [ $$RETVAL -ne 0 ]; then \
		cp -f prob/$(PROB)/Prob.cpp src/Prob.cpp; \
	fi
	@cmp -s prob/$(PROB)/definitions.H src/definitions.H; \
	RETVAL=$$?; \
	if [ $$RETVAL -ne 0 ]; then \
		cp -f prob/$(PROB)/definitions.H src/definitions.H; \
	fi
	@cmp -s prob/$(PROB)/criptic.in run/criptic.in; \
	RETVAL=$$?; \
	if [ $$RETVAL -ne 0 ]; then \
		cp -f prob/$(PROB)/criptic.in run/criptic.in; \
	fi
endif

run:
	mkdir run
