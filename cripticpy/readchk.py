"""
Routine to read a CRIPTIC checkpoint
"""

import numpy as np
import glob
import warnings
import sys
from ctypes import sizeof, c_int16, c_int32, c_int64, c_double, c_float, \
    c_uint16, c_uint32, c_uint64
try:
    import astropy.units as u
    import astropy.constants as const
    from astropy.constants import m_p, m_e, c
except:
    u = None

# Particle types used in criptic; note masses are in units of m_p,
# charges are in units of e, as used internally in criptic
parttypes = [
    { 'name' : 'null',
      'm' : -1.0,
      'Z' : 0 },
    { 'name' : 'proton',
      'm' : 1.0,
      'Z' : 1 },
    { 'name' : 'electron',
      'm' : 1.0 / 1836.15267343,  # CODATA 2018
      'Z' : -1 },
    { 'name' : 'positron',
      'm' : 1.0 / 1836.15267343,  # CODATA 2018
      'Z' : 1 }
    ]

def _Tp(pdim, ptype):
    """
    Compute a kinetic energy from a momentum for a given particle
    type

    Parameters
       pdim : array
          momentum in units where m_p = 1 and c = 1
       ptype : array
          array of particle types

    Returns
       T : array
          kinetic energies in units where m_p = 1 and c = 1
    """
    m = np.array([ parttypes[i]['m'] for i in ptype ])
    pm = pdim / m
    T = m * np.sqrt(1+pm*pm) - 1
    idx = pm < 1e-6
    T[idx] = pdim[idx]**2 / (2.0*m[idx]) * (1 - pm[idx]**2/4.0)
    return T


def readchk(chkname, units=True, ke=True):
    """
    Read a CRIPTIC checkpoint

    Parameters
       chkname : string
          The base name of the checkpoint to read, without any _rank
          or .chk extensions; the name of the metadata checkpoint file
          (of the form basenameNNNNN.txt) is also acceptable
       units : bool
          If True, all quantities are returned as astropy.Quantity
          objects with appropriate units; if False data are returned as
          flat numpy arrays.
       ke : bool
          If True, energies will be computed for all momentum
          quantities; only allowed if units is also True

    Returns
       t : float or astropy.Quantity
          time of output
       packets : dict
          a dict containing information on packets, with the following
          entries:
          "x" : ndarray or astropy.Quantity, size (n,3)
             positions of all packets
          "ptype" : ndarray or astropy.Quantity, size (n)
             particle types of all packets
          "uid" : ndarray, size (n)
             unique IDs of all packets
          "source" : ndarray, size (n)
             unique IDs of sources of all packets
          "p" : ndarray or astropy.Quantity, size (n)
             momenta of all packets
          "w" : ndarray, size (n)
             statistical weights of all packets
          "tinj" : ndarray or astropy.Quantity, size (n)
             times at which each packet was injected
          "wgtinj" : ndarray or astropy.Quantity, size (n)
             statistical weights of each packet at injection
          "gr" : ndarray or astropy.Quantity, size (n)
             grammage traversed by each packet
          (optional quantities, may or may not be included depending
           on contents of file and options):
          "T" : ndarray or astropy.Quantity, size (n)
             CR packet kinetic energies
          "pres" : ndarray or astropy.Quantity, size(n)
             CR pressure divided by k_B at position of each packet;
             pressures only include packets with gyroradii >= the
             gyroradius of this packet
          "presGrad" : ndarray or astropy.Quantity, size (n,3)
             gradient of pres
          "eden" : ndarray or astropy.Quantity, size(n)
             CR energy density at position of each packet, including only
             CRs with gyroradii >= the gyroradius of this packet
          "edenGrad" : ndarray or astropy.Quantity, size(n,3)
             gradient of eden
          "nden" : ndarray or astropy.Quantity, size(n)
             CR number density at position of each packet, including only
             CRs with gyroradii >= the gyroradius of this packet
          "ndenGrad" : ndarray or astropy.Quantity, size(n,3)
             gradient of nden
       delpackets : dict
          a dict containing information on packets that were deleted
          since the last checkpoint, with the following entries:
          "x" : ndarray or astropy.Quantity, size (m,3)
             positions of all packets
          "ptype" : ndarray, size (m)
             particle types of all packets
          "uid" : ndarray, size (m)
             unique IDs of all packets
          "source" : ndarray, size (m)
             unique IDs of sources of all packets
          "p" : ndarray or astropy.Quantity, size (m)
             momenta of all packets
          "w" : ndarray, size (m)
             statistical weights of all packets
          "tinj" : ndarray or astropy.Quantity, size (m)
             times at which each packet was injected
          "wgtinj" : ndarray, size (m)
             statistical weights of each packet at injection
          "gr" : ndarray or astropy.Quantity, size (m)
             grammage traversed by each packet
          (optional quantities, may or may not be included depending
           on contents of file and options):
          "T" : ndarray or astropy.Quantity, size (n)
             CR packet kinetic energies
       sources : dict
          a dict containing information on sources, with the following
          entries:
          "x" : ndarray or astropy.Quantity, size (i,3)
             positions of all sources
          "ptype" : ndarray or astropy.Quantity, size(i)
             particle types produced by sources
          "uid" : ndarray, size (i)
             unique IDs of all sources
          "p0" : ndarray or astropy.Quantity, size (i)
             lower limit of momentum of packets injected by each source
          "p1" : ndarray or astropy.Quantity, size (i)
             upper limit of momentum of packets injected by each source
          "q" : ndarray, size (i)
             index of source momentum distribution
          "coef" : ndarray or astropy.Quantity, size (i)
             coefficient of source momentum distribution
    """

    # If units have been requested, make sure we have astropy; issue
    # warning if not
    if units and u is None:
        warnings.warn("units requested but astropy not found")
        units = False

    # If we have been requested to get kinetic energy, make sure we
    # have astropy
    if ke and not units:
        warnings.warn("cannot compute kinetic energies without units")
        ke = False

    # If we were given the name of the metadata file, strip off the
    # final .txt
    if str(chkname).endswith('.txt'):
        cname = chkname[:-4]
    else:
        cname = chkname

    # Look for file names making required pattern
    files = glob.glob(cname+'_rank*.chk')
    if len(files) == 0:
        files = glob.glob(cname+'.chk')
        if len(files) == 0:
            raise FileNotFoundError(cname+".chk")

    # Read the metadata file
    metafile = cname+".txt"
    fp = open(metafile, 'r')
    for line in fp:
        if line == "Units = CGS\n":
            cgs = True
        elif line == "Units = MKS\n":
            cgs = False
        elif line.startswith("t = "):
            spl = line.split()
            t = float(spl[-1])
        elif line == "format = ASCII\n":
            fmt = "ASCII"
        elif line == "format = binary\n":
            fmt = "bin"
    fp.close()

    # Ingest header of first checkpoint file to get information about
    # the data present in it; this looks different for ASCII versus
    # binary formatted data
    if fmt == 'ASCII':

        # ASCII case: just read list of field quantities
        fp = open(files[0], 'r')
        for i in range(4):
            line = fp.readline()  # Burn 3 lines, info is on 4th
            fieldqty = line.split()[11:]
        fp.close()

        # Set types to default numpy types
        realtype = np.double
        inttype = np.int
        uinttype = np.uint

    else:

        # Binary case; here we need to read both the field quantities
        # and the descriptors for the sizes and endianness of the raw
        # binary data
        fp = open(files[0], 'rb')

        # Read field quantities
        for i in range(4):
            line = fp.readline()  # Burn 3 lines, info is on 4th
            fieldqty = [ l.decode('ASCII') for l in line.split() ]

        # Next line specifies endianness
        bigendian = int(fp.readline())

        # Next line specifies the sizes of data types and the
        # alignment of structures
        line = fp.readline()
        realsize = int(line.split()[0])
        intsize = int(line.split()[1])
        uintsize = int(line.split()[2])
        packetsize = int(line.split()[3])
        srcsize = int(line.split()[4])
        packetalign = int(line.split()[5])
        srcalign = int(line.split()[6])

        # Next lines gives offsets of parts of the c++ packet and
        # source structures
        line = fp.readline()
        packet_offset = [ int(s) for s in line.split() ]
        line = fp.readline()
        src_offset = [ int(s) for s in line.split() ]
        
        # Set types and endianness based on what we have read
        if realsize == sizeof(c_double):
            realtype = c_double
        elif realsize == sizeof(c_float):
            realtype = c_float
        else:
            raise IOError("unexpected size " + str(realsize) +
                          " for criptic::Real")
        if intsize == sizeof(c_int16):
            inttype = c_int16
        elif intsize == sizeof(c_int32):
            inttype = c_int32
        elif intsize == sizeof(c_int64):
            inttype = c_int64
        else:
            raise IOError("unexpected size " + str(intsize) +
                          " for int")
        if uintsize == sizeof(c_int16):
            uinttype = c_int16
        elif uintsize == sizeof(c_uint32):
            uinttype = c_uint32
        elif uintsize == sizeof(c_uint64):
            uinttype = c_uint64
        else:
            raise IOError("unexpected size " + str(intsize) +
                          " for uint")

        # Construct a numpy datatype representing CRPacket and
        # CRSource structs as described in the file header
        names = [ 'ptype', 'src', 'uniqueID', 'tinj',
                  'winj', 'p', 'w', 'gr' ]
        formats = [ inttype, uinttype, uinttype, realtype,
                    realtype, realtype, realtype, realtype ]
        offsets = packet_offset
        packettype = np.dtype({ 'names' : names,
                                'formats' : formats,
                                'offsets' : offsets })
        names = [ 'ptype', 'uniqueID', 'p0', 'p1', 'q', 'coef' ]
        formats = [ inttype, uinttype, realtype, realtype,
                    realtype, realtype ]
        offsets = src_offset
        srctype = np.dtype({ 'names' : names,
                             'formats' : formats,
                             'offsets' : offsets })
        if bigendian and sys.byteorder == 'little':
            packettype = packettype.newbyteorder('>')
            srctype = srctype.newbyteorder('>')
        elif not bigendian and sys.byteorder == 'big':
            packettype = packettype.newbyteorder('<')
            srctype = srctype.newbyteorder('<')
                
        # Close file
        fp.close()
    
    # Prepare data holders
    x = np.zeros((0,3), dtype=realtype)
    ptype = np.zeros(0, dtype=inttype)
    source = np.zeros(0, dtype=uinttype)
    uid = np.zeros(0, dtype=uinttype)
    tinj = np.zeros(0, dtype=realtype)
    winj = np.zeros(0, dtype=realtype)
    p = np.zeros(0, dtype=realtype)
    w = np.zeros(0, dtype=realtype)
    gr = np.zeros(0, dtype=realtype)
    fld = np.zeros((0,len(fieldqty)), dtype=realtype)
    xdel = np.zeros((0,3), dtype=realtype)
    ptypedel = np.zeros(0, dtype=uinttype)
    sourcedel = np.zeros(0, dtype=uinttype)
    uiddel = np.zeros(0, dtype=uinttype)
    tinjdel = np.zeros(0, dtype=realtype)
    winjdel = np.zeros(0, dtype=realtype)
    pdel = np.zeros(0, dtype=realtype)
    wdel = np.zeros(0, dtype=realtype)
    grdel = np.zeros(0, dtype=realtype)
    tdel = np.zeros(0, dtype=realtype)
    xsrc = np.zeros((0,3), dtype=realtype)
    ptypesrc = np.zeros(0, dtype=inttype)
    uidsrc = np.zeros(0, dtype=uinttype)
    p0src = np.zeros(0, dtype=realtype)
    p1src = np.zeros(0, dtype=realtype)
    qsrc = np.zeros(0, dtype=realtype)
    coefsrc = np.zeros(0, dtype=realtype)
    
    # Read files
    for f in files:

        # Choose format
        if fmt == "ASCII":
        
            # ASCII format

            # First read numbers of packets and sources
            fp = open(f, 'r')
            sp = fp.readline().split()
            if len(sp) != 3:
                raise IOError("unexpected format for file "+f)
            if sp[0] != "nPacket" or sp[1] != "=":
                raise IOError("unexpected format for file "+f)
            npacket = int(sp[2])
            sp = fp.readline().split()
            if len(sp) != 3:
                raise IOError("unexpected format for file "+f)
            if sp[0] != "nDeletedPacket" or sp[1] != "=":
                raise IOError("unexpected format for file "+f)
            ndel = int(sp[2])
            sp = fp.readline().split()
            if len(sp) != 3:
                raise IOError("unexpected format for file "+f)
            if sp[0] != "nSrc" or sp[1] != "=":
                raise IOError("unexpected format for file "+f)
            nsrc = int(sp[2])
            fp.close()

            # Read packets
            if npacket > 0:
                data = np.loadtxt(f, skiprows=4, max_rows=npacket)
                x = np.vstack((x, data[:,:3]))
                ptype = np.hstack((ptype, np.array(data[:,3], dtype=inttype)))
                source = np.hstack((source, np.array(data[:,4], dtype=uinttype)))
                uid = np.hstack((uid, np.array(data[:,5], dtype=np.uint)))
                tinj = np.hstack((tinj, data[:,6]))
                winj = np.hstack((winj, data[:,7]))
                p = np.hstack((p, data[:,8]))
                w = np.hstack((w, data[:,9]))
                gr = np.hstack((gr, data[:,10]))
                fld = np.vstack((fld, data[:,11:]))

            # Read deleted packets
            if ndel > 0:
                data = np.loadtxt(f, skiprows=6+npacket, max_rows=ndel)
                xdel = np.vstack((xdel, data[:,:3]))
                ptypedel = np.hstack((ptypedel, np.array(data[:,3], dtype=inttye)))
                sourcedel = np.hstack((sourcedel, np.array(data[:,4],
                                                           dtype=uinttype)))
                uiddel = np.hstack((uiddel, np.array(data[:,5], dtype=uinttype)))
                tinjdel = np.hstack((tinjdel, data[:,6]))
                winjdel = np.hstack((winjdel, data[:,7]))
                pdel = np.hstack((pdel, data[:,8]))
                wdel = np.hstack((wdel, data[:,9]))
                grdel = np.hstack((grdel, data[:,10]))
                tdel = np.hstack((tdel, data[:,11]))
            
            # Read sources
            if nsrc > 0:
                data = np.loadtxt(f, skiprows=7+npacket+ndel, max_rows=nsrc)
                xsrc = np.vstack((xsrc, data[:,:3]))
                ptypesrc = np.vstack((ptypesrc, np.array(data[:,3], dtype=inttype)))
                uidsrc = np.hstack((uidsrc, np.array(data[:,4], dtype=uinttype)))
                p0src = np.hstack((p0src, data[:,4]))
                p1src = np.hstack((p1src, data[:,5]))
                qsrc = np.hstack((qsrc, data[:,6]))
                coefsrc = np.hstack((coefsrc, data[:,7]))

        else:

            # Binary format

            # First read numbers of packets and sources
            fp = open(f, 'rb')
            sp = fp.readline().split()
            if len(sp) != 3:
                raise IOError("unexpected format for file "+f)
            if sp[0].decode('ASCII') != "nPacket" or \
               sp[1].decode('ASCII') != "=":
                raise IOError("unexpected format for file "+f)
            npacket = int(sp[2])
            sp = fp.readline().split()
            if len(sp) != 3:
                raise IOError("unexpected format for file "+f)
            if sp[0].decode('ASCII') != "nDeletedPacket" or \
               sp[1].decode('ASCII') != "=":
                raise IOError("unexpected format for file "+f)
            ndel = int(sp[2])
            sp = fp.readline().split()
            if len(sp) != 3:
                raise IOError("unexpected format for file "+f)
            if sp[0].decode('ASCII') != "nSrc" or \
               sp[1].decode('ASCII') != "=":
                raise IOError("unexpected format for file "+f)
            nsrc = int(sp[2])
            
            # Burn remainder of header, since we have already read it
            for i in range(5):
                fp.readline()

            # We are now at start of binary data; ingest each
            # component and store

            # Packets
            if npacket > 0:

                # Packet position
                x_ = np.fromfile(fp, dtype=realtype, count=3*npacket). \
                     reshape((npacket,3))
                x = np.vstack((x, x_))

                # Packet data
                pd = np.fromfile(fp, dtype=packettype, count=npacket)
                ptype = np.hstack((ptype, pd['ptype']))
                source = np.hstack((source, pd['src']))
                uid = np.hstack((uid, pd['uniqueID']))
                tinj = np.hstack((tinj, pd['tinj']))
                winj = np.hstack((winj, pd['winj']))
                p = np.hstack((p, pd['p']))
                w = np.hstack((w, pd['w']))
                gr = np.hstack((gr, pd['gr']))

                # Field quantities
                fld_ = np.fromfile(fp, dtype=realtype,
                                   count=len(fieldqty)*npacket). \
                                   reshape((npacket,len(fieldqty)))
                fld = np.vstack((fld, fld_))

            # Deleted packets
            if ndel > 0:
                
                # Deleted packet positions
                xdel_ = np.fromfile(fp, dtype=realtype, count=3*ndel). \
                        reshape((ndel,3))
                xdel = np.vstack((xdel, xdel_))

                # Deleted packet data
                pddel = np.fromfile(fp, dtype=packettype, count=ndel)
                ptypedel = np.hstack((ptypedel, pddel['ptype']))
                sourcedel = np.hstack((sourcedel, pddel['src']))
                uiddel = np.hstack((uiddel, pddel['uniqueID']))
                tinjdel = np.hstack((tinjdel, pddel['tinj']))
                winjdel = np.hstack((winjdel, pddel['winj']))
                pdel = np.hstack((pdel, pddel['p']))
                wdel = np.hstack((wdel, pddel['w']))
                grdel = np.hstack((grdel, pddel['gr']))

                # Deleted packet time
                tdel_ = np.fromfile(fp, dtype=realtype, count=ndel)
                tdel = np.hstack((tdel, tdel_))

            # Sources
            if nsrc > 0:

                # Source positions
                xsrc_ = np.fromfile(fp, dtype=realtype, count=3*nsrc). \
                        reshape((nsrc,3))
                xsrc = np.vstack((xsrc, xsrc_))

                # Source data
                src = np.fromfile(fp, dtype=srctype, count=nsrc)
                ptypesrc = np.hstack((ptypesrc, src['ptype']))
                uidsrc = np.hstack((uidsrc, src['uniqueID']))
                p0src = np.hstack((p0src, src['p0']))
                p1src = np.hstack((p1src, src['p1']))
                qsrc = np.hstack((qsrc, src['q']))
                coefsrc = np.hstack((coefsrc, src['coef']))

            # Close file
            fp.close()


    # Split up fields
    pres = None
    presGrad = None
    eden = None
    edenGrad = None
    nden = None
    ndenGrad = None
    ctr = 0
    while ctr < len(fieldqty):
        if fieldqty[ctr] == "P/kB":
            pres = fld[:,ctr]
            ctr = ctr+1
        elif fieldqty[ctr] == "P/kB_grad_x":
            presGrad = fld[:,ctr:ctr+3]
            ctr = ctr+3
        elif fieldqty[ctr] == "eden":
            eden = fld[:,ctr]
            ctr = ctr+1
        elif fieldqty[ctr] == "eden_grad_x":
            edenGrad = fld[:,ctr:ctr+3]
            ctr = ctr+3
        elif fieldqty[ctr] == "nden":
            nden = fld[:,ctr]
            ctr = ctr+1
        elif fieldqty[ctr] == "nden_grad_x":
            ndenGrad = fld[:,ctr:ctr+3]
            ctr = ctr+3

    # Apply units
    if units:
        t = t*u.s
        tinj = tinj*u.s
        tdel = tdel*u.s
        tinjdel = tinjdel*u.s
        if cgs:
            x = x * u.cm
            xdel = xdel * u.cm
            xsrc = xsrc * u.cm
            gr = gr * u.g
            grdel = grdel * u.g
            if nden is not None:
                nden = nden / u.cm**3
            if ndenGrad is not None:
                ndenGrad = ndenGrad / u.cm**4
        else:
            x = x*u.m
            xdel = xdel * u.m
            xscr = xsrc * u.m
            gr = gr * u.kg
            grdel = grdel * u.kg
            if nden is not None:
                nden = nden / u.m**3
            if ndenGrad is not None:
                ndenGrad = ndenGrad / u.m**4                
        if fmt == "ASCII":
            p = p * u.GeV/c
            pdel = pdel * u.GeV/c
            p0src = p0src * u.GeV/c
            p1src = p1src * u.GeV/c
            coefsrc = coefsrc / (u.s * u.GeV/c)
            if cgs:
                if pres is not None:
                    pres = pres * u.K * u.cm**-3
                if presGrad is not None:
                    presGrad = presGrad * u.K * u.cm**-4
                if eden is not None:
                    eden = eden * u.eV / u.cm**3
                if edenGrad is not None:
                    edenGrad = edenGrad * u.eV / u.cm**4
            else:
                if pres is not None:
                    pres = pres * u.K * u.m**-3
                if presGrad is not None:
                    presGrad = presGrad * u.K * u.m**-4
                if eden is not None:
                    eden = eden * u.eV / u.m**3
                if edenGrad is not None:
                    edenGrad = edenGrad * u.eV / u.m**4
        else:
            p = p*m_p*c
            pdel = pdel*m_p*c
            p0src = p0src*m_p*c
            p1src = p1src*m_p*c
            coefsrc = coefsrc / (u.s * m_p*c)
            if cgs:
                if pres is not None:
                    pres = (pres * m_p * c**2 / u.cm**3 / const.k_B). \
                           to(u.K / u.cm**3)
                if presGrad is not None:
                    presGrad = (presGrad * m_p * c**2 / u.cm**4 / const.k_B). \
                           to(u.K / u.cm**4)
                if eden is not None:
                    eden = (eden * m_p * c**2 / u.cm**3).to(u.eV/u.cm**3)
                if edenGrad is not None:
                    edenGrad = (edenGrad * m_p * c**2 / u.cm**4). \
                               to(u.eV/u.cm**4)
            else:
                if pres is not None:
                    pres = (pres * m_p * c**2 / u.m**3 / const.k_B). \
                           to(u.K / u.m**3)
                if presGrad is not None:
                    presGrad = (presGrad * m_p * c**2 / u.m**4 / const.k_B). \
                               to(u.K / u.m**3)
                if eden is not None:
                    eden = (eden * m_p * c**2 / u.m**3).to(u.eV / u.m**3)
                if edenGrad is not None:
                    edenGrad = (edenGrad * m_p * c**2 / u.m**4). \
                               to(u.eV / u.m**4)

    # Compute kinetic energies if requested
    if ke:
        T = (_Tp( np.array((p / (m_p*c)).to('')), ptype) * m_p*c**2).to('GeV')
        Tdel = (_Tp( np.array( pdel / (m_p*c)), ptypedel) *
                m_p*c**2).to('GeV')
        T0src = (_Tp( np.array(p0src / (m_p*c)), ptypesrc) *
                 m_p*c**2).to('GeV')
        T1src = (_Tp( np.array(p1src / (m_p*c)), ptypesrc) *
                 m_p*c**2).to('GeV')
              
    # Pack data into dicts and return
    packets = { "x" : x,
                "ptype" : ptype,
                "uid" : uid,
                "source" : source,
                "p" : p,
                "w" : w,
                "tinj" : tinj,
                "winj" : winj,
                "gr" : gr,
                "ptype" : ptype }
    if pres is not None:
        packets["pres"] = pres
    if presGrad is not None:
        packets["presGrad"] = presGrad
    if eden is not None:
        packets["eden"] = eden
    if edenGrad is not None:
        packets["edenGrad"] = edenGrad
    if nden is not None:
        packets["nden"] = nden
    if ndenGrad is not None:
        packets["ndenGrad"] = ndenGrad
    if ke:
        packets["T"] = T
    delpackets = { "x" : xdel,
                   "ptype" : ptypedel,
                   "uid" : uiddel,
                   "source" : sourcedel,
                   "p" : pdel,
                   "w" : wdel,
                   "tinj" : tinjdel,
                   "winj" : winjdel,
                   "gr" : grdel,
                   "ptype" : ptypedel }
    if ke:
        delpackets["T"] = Tdel
    sources = { "x" : xsrc,
                "ptype" : ptypesrc,
                "uid" : uidsrc,
                "p0" : p0src,
                "p1" : p1src,
                "q" : qsrc,
                "coef" : coefsrc }
    if ke:
        sources["T0"] = T0src
        sources["T1"] = T1src
    return t, packets, delpackets, sources
