// This is a problem setup file for the advection test problem

#include <cmath>
#include "Prob.H"
#include "gasmodel/GasModelUniform.H"
#include "propmodel/PropModelStreamFixedDir.H"
#include "utils/particleTypes.H"
#include "utils/sr.H"
#include "utils/MPIUtil.H"

using namespace std;
using namespace criptic;

// Routine to initialize CR sources; this test has no initial sources,
// so this routine is empty
void initSources(const ParmParser &pp,
		 vector<Real> &x,
		 vector<CRSource> &sources,
		 RngVec &rng)
{ }

// Routine to initialize CR packets
void initPackets(const ParmParser &pp,
		 vector<criptic::Real> &x,
		 vector<CRPacket> &packets,
		 RngVec &rng) {

  // Only create packets on the IO processor; they will be
  // redistributed to other processors automatically
  if (MPIUtil::IOProc) {
    
    // Read number of packets, the size of the region into which to
    // insert them, and the total CR energy
    int np;
    Real r_init, e_cr_tot, e_cr;
    pp.get("prob.n_packet", np);
    pp.get("prob.r_init", r_init);
    pp.get("prob.e_cr_tot", e_cr_tot);
    pp.get("prob.e_cr", e_cr);

    // Loop over packets
    for (int i=0; i<np; i++) {

      // Pick a random position within the sphere
      Real r = r_init * pow(rng.uniform(), 1./3.);
      Real mu = rng.uniform(-1,1);
      Real phi = rng.uniform(0, 2*M_PI);
      x.push_back(r * sqrt(1.0 - mu*mu) * cos(phi));
      x.push_back(r * sqrt(1.0 - mu*mu) * sin(phi));
      x.push_back(r * mu);
      
      // Create packet
      CRPacket pkt;
      pkt.type = partTypes::proton;
      pkt.src = -1;
      pkt.tInj = 0.0;
      pkt.p = sr::p_from_T(pkt.type, e_cr * units::GeV / constants::mp_c2);
      pkt.w = pkt.wInj = e_cr_tot / (e_cr * np);
      pkt.gr = 0.0;
      packets.push_back(pkt);
    }
  } 
}


// Function to return the gas model; we just use the standard uniform
// gas model class to do so
GasModel *initGas(const ParmParser &pp,
		  const Geometry &geom) {
  return new GasModelUniform(pp);
}


// Function to construct the propagation model; this problem as pure
// one-directional streaming
PropModel *initProp(const ParmParser &pp) {
  return new PropModelStreamFixedDir(pp);
}

// User work function. This is a no-op for this test.
void userWork(Real t, GasModel &gas, PropModel &prop, CRTree &tree) { }
