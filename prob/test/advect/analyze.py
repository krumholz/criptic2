"""
Analysis script for the advection test problem
"""

import argparse
import numpy as np
import astropy.units as u
from glob import glob
import os.path as osp
from cripticpy import readchk

# Name of the problem this script is for
prob_name = "advect"

# Check if we are supposed to produce full outputs
parser = argparse.ArgumentParser(
    description="Analysis script for the "+prob_name+" test")
parser.add_argument("--testonly",
                    help="report test results only, do not make plots",
                    default=False, action="store_true")
parser.add_argument("-c", "--chk",
                    help="number of checkpoint file to examine (default "
                    "is last checkpoint file in directory)",
                    default=-1, type=int)
parser.add_argument("-d", "--dir",
                    help="directory containing run to analyze",
                    default=".", type=str)
args = parser.parse_args()

# Extract parameters we need from the input file
fp = open(osp.join(args.dir, 'criptic.in'), 'r')
chkname = 'criptic_'
for line in fp:
    l = line.split('#')[0]    # Strip comments
    s = l.split()
    if len(s) == 0: continue
    if s[0] == 'output.chkname':
        chkname = s[1]
    elif s[0] == 'prob.velocity':
        v = np.array([float(s1) for s1 in s[1:]]) * u.cm / u.s
    elif s[0] == 'cr.vStream':
        vstr_va = float(s[1])
    elif s[0] == 'prob.magField':
        B = np.array([float(s1) for s1 in s[1:]]) * u.G
    elif s[0] == 'prob.ionDensity':
        rhoi = float(s[1]) * u.g / u.cm**3
    elif s[0] == 'boundary.prob_lo':
        plo = np.array([float(s1) for s1 in s[1:]]) * u.cm
    elif s[0] == 'boundary.prob_hi':
        phi = np.array([float(s1) for s1 in s[1:]]) * u.cm
fp.close()

# Figure out which checkpoint file to examine
if args.chk >= 0:
    chknum = "{:05d}".format(args.chk)
else:
    chkfiles = glob(osp.join(args.dir,
                             chkname+"[0-9][0-9][0-9][0-9][0-9].txt"))
    chknums = [ int(c[-9:-4]) for c in chkfiles ]
    chknum = "{:05d}".format(np.amax(chknums))

# Read the checkpoint
t, packets, delpackets, sources = readchk(osp.join(args.dir,
                                                   chkname+chknum))

# Read the initial state
t0, packets0, delpackets0, sources0 = readchk(osp.join(args.dir,
                                                       chkname+'00000'))

# Get packet positions
x0 = packets0['x'][np.argsort(packets0['uid'])]
x = packets['x'][np.argsort(packets['uid'])]

# Compute the streaming speed; note that we need to fix up the fact
# that astropy units doesn't correctly handle the equivalence of
# electromagnetic and mechanical units in the Gaussian unit system
Bmag = np.sqrt(np.sum(B**2))
Bcgs = Bmag.to(u.G).value
rhoicgs = rhoi.to(u.g/u.cm**3).value
va = Bcgs / np.sqrt(4. * np.pi * rhoicgs) * u.cm/u.s
vstr = vstr_va * va * B/Bmag

# Get expected positions, accounting for periodic wrap
xp = x0 + (v + vstr)*t
for i in range(3):
    while True:
        count = 0
        idx = xp[:,i] < plo[i]
        count += np.sum(idx)
        xp[idx,i] += phi[i] - plo[i]
        idx = xp[:,i] > phi[i]
        count += np.sum(idx)
        xp[idx,i] -= phi[i] - plo[i]
        if count == 0:
            break

# Compute error
dxrel = (xp - x) / (phi - plo)
err = np.sqrt(dxrel[:,0]**2 + dxrel[:,1]**2 + dxrel[:,2]**2)
meanerr = np.mean(np.abs(err))

# Error should be less than machine precision; assume floating point
# rather than double
if meanerr < 1.0e-8:
    print("PASS")
else:
    print("FAIL")

# Make plot if requested
if not args.testonly:
    plt.figure(1)
    plt.clf()
    plt.hist(np.array(err), bins='auto')
    plt.xlabel('Relative position error')
    plt.ylabel('Packet count')
    plt.subplots_adjust(left=0.2)
    plt.savefig(osp.join(args.dir, "result1.pdf"))
