// This is a problem setup file for the advection test problem

#include <cmath>
#include "Prob.H"
#include "utils/particleTypes.H"
#include "utils/sr.H"
#include "utils/MPIUtil.H"

using namespace std;
using namespace criptic;

// Routine to initialize CR sources; this test has no initial sources,
// so this routine is empty
void initSources(const ParmParser &pp,
		 vector<Real> &x,
		 vector<CRSource> &sources,
		 RngVec &rng)
{ }

// Routine to initialize CR packets
void initPackets(const ParmParser &pp,
		 vector<criptic::Real> &x,
		 vector<CRPacket> &packets,
		 RngVec &rng) {

  // Only create packets on the IO processor; they will be
  // redistributed to other processors automatically
  if (MPIUtil::IOProc) {
    
    // Read number of packets, the size of the region into which to
    // insert them, and the total CR energy
    int np;
    Real r_init, e_cr_tot;
    pp.get("prob.n_packet", np);
    pp.get("prob.r_init", r_init);
    pp.get("prob.e_cr_tot", e_cr_tot);

    // Read energy range and sampling; figure out corresponding
    // momentum range
    Real e_min, e_max, q;
    pp.get("prob.e_min", e_min);
    pp.get("prob.e_max", e_max);
    pp.get("cr.q_samp", q);
    Real p0 = sr::p_from_T(1.0, e_min * units::GeV / constants::mp_c2);
    Real p1 = sr::p_from_T(1.0, e_max * units::GeV / constants::mp_c2);
    Real p0q, p1q;
    p0q = pow(p0, 1+q);
    p1q = pow(p1, 1+q);

    // Loop over packets
    Real etot = 0.0;
    for (int i=0; i<np; i++) {

      // Pick a random position within the sphere
      Real r = r_init * pow(rng.uniform(), 1./3.);
      Real mu = rng.uniform(-1,1);
      Real phi = rng.uniform(0, 2*M_PI);
      x.push_back(r * sqrt(1.0 - mu*mu) * cos(phi));
      x.push_back(r * sqrt(1.0 - mu*mu) * sin(phi));
      x.push_back(r * mu);
      
      // Pick a random momentum
      Real y = rng.uniform();
      Real p;
      if (q == -1.0) {
	p = p0 * exp(y * log(p1/p0));
      } else {
	p = pow(y*p0q + (1-q)*p1q, 1.0/(1.0+q));
      }
	
      // Create packet
      Real t = 0.0;
      Real w = 1.0;          // Fix up weight below
      int src = -1;
      CRPacket pkt(partTypes::proton, t, w, p, src);
      packets.push_back(pkt);

      // Add to running energy total
      etot += pkt.T();
    }

    // Now recompute all the weights to get the target energy
    for (auto &p : packets)
      p.w = p.wInj = e_cr_tot / (etot * constants::mp_c2);
  } 
}


// Here we introduce a class to implement a gas with dipole magnetic
// field and a uniform gas density
class DipoleMagneticField : public GasModelAnalytic {
public:
  DipoleMagneticField(const ParmParser& pp) {
    // Read data from ParmParser
    pp.get("prob.density", rho);
    pp.get("prob.ionDensity", rhoi);
    pp.get("prob.dipoleMoment", moment);
    pp.get("prob.velocity", v);
  }
  virtual ~DipoleMagneticField() { }
  virtual void gas(const Real x[3],
		   const Real t,
		   Real &totalDensity,
		   Real &ionDensity,
		   Real velocity[3]) const {
    (void) x; (void) t;  // avoid compiler unused variable warnings
    totalDensity = rho;
    ionDensity = rhoi;
    for (int i=0; i<3; i++) velocity[i] = v[i];
  }
  virtual void magField(const Real x[3],
			const Real t,
			Real B[3]) const {
    (void) t;
    Real r = sqrt(x[0]*x[0] + x[1]*x[1] + x[2]*x[2]);
    Real costheta = x[2] / r;
    Real sintheta = sqrt(1.0 - costheta*costheta);
    Real cosphi = x[0] / sqrt(x[0]*x[0] + x[1]*x[1]);
    Real sinphi = sqrt(1.0 - cosphi*cosphi);
    Real er[3] = { sintheta * cosphi,
		   sintheta * sinphi,
		   costheta };
    Real etheta[3] = { costheta * cosphi,
		       costheta * sinphi,
		       -sintheta };
    for (int i=0; i<3; i++) {
      B[i] = moment / (r*r*r) *
	(2.0 * costheta * er[i] + sintheta * etheta[i]);
    }
  }
  virtual void magFieldGrad(const Real x[3],
			    const Real t,
			    Real gradB[9]) const {
    Real r2 = x[0]*x[0] + x[1]*x[1] + x[2]*x[2];
    Real B[3];
    magField(x, t, B);
    // dB_x / dx
    gradB[0] = (1/x[0] - 5*x[0]/r2) * B[0];
    // dB_x / dy
    gradB[1] = -5*x[1] / r2 * B[0];
    // dB_x / dz
    gradB[2] = (1/x[2] - 5*x[2]/r2) * B[0];
    // dB_y / dz
    gradB[3] = -5*x[0] / r2 * B[1];
    // dB_y / dy
    gradB[4] = (1/x[1] - 5*x[1]/r2) * B[1];
    // dB_y / dz
    gradB[5] = (1/x[2] - 5*x[2]/r2) * B[1];
    // dB_z / dx
    gradB[6] = x[0] * (2/(r2 - 3*x[2]*x[2]) - 5/r2) * B[2];
    // dB_z / dy
    gradB[7] = x[1] * (2/(r2 - 3*x[2]*x[2]) - 5/r2) * B[2];
    // dB_z / dz
    gradB[8] = -x[2] * (4/(r2 - 3*x[2]*x[2]) + 5/r2) * B[2];
  }
private:
  Real rho, rhoi, moment;
  vector<Real> v;
};

// Function to return the gas model; we just use the standard uniform
// gas model class to do so
GasModelAnalytic *initGas(const ParmParser &pp) {
  return new DipoleMagneticField(pp);
}
  
  
