// This is a problem setup file for the linear diffusion test problem

#include <cmath>
#include "Prob.H"
#include "gasmodel/GasModelUniform.H"
#include "propmodel/PropModelDiffPowerlaw.H"
#include "utils/particleTypes.H"
#include "utils/sr.H"
#include "utils/MPIUtil.H"

using namespace std;
using namespace criptic;

// Routine to initialize CR sources
void initSources(const ParmParser &pp,
		 vector<Real> &x,
		 vector<CRSource> &sources,
		 RngVec &rng)
{
  // Only one MPI rank does this
  if (MPIUtil::IOProc) {
  
    // Get source luminosities and energies
    vector<Real> LSrc, TSrc;
    pp.get("prob.L", LSrc);
    pp.get("prob.T", TSrc);
    if (LSrc.size() != TSrc.size()) {
      MPIUtil::haltRun("Prob: inconsistent number of sources found in input file!",
		       errBadInputFile);
    }

    // Create sources
    IdxType n = LSrc.size();
    x.resize(3*n);
    sources.resize(n);
    for (IdxType i=0; i<n; i++) {
      sources[i] = CRSrc::makeSource(partTypes::proton, LSrc[i],
				     TSrc[i], TSrc[i], 0);
    }
  }
}

// Routine to initialize CR packets; this test problem has no packets
// present initially so this routine is empty
void initPackets(const ParmParser &pp,
		 vector<criptic::Real> &x,
		 vector<CRPacket> &packets,
		 RngVec &rng)
{ }


// Function to return the gas model; we just use the standard uniform
// gas model class to do so
GasModel *initGas(const ParmParser &pp,
		  const Geometry &geom) {
  return new GasModelUniform(pp);
}

// Function to return the propagation model; this test uses the
// standard diffusive powerlaw model
PropModel *initProp(const ParmParser &pp) {
  return new PropModelDiffPowerlaw(pp);
}
  
// User work function. This is a no-op for this test.
void userWork(Real t, GasModel &gas, PropModel &prop, CRTree &tree) { }
