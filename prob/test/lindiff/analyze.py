"""
Analysis script for the linear diffusion test problem
"""

import argparse
import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u
import astropy.constants as const
from glob import glob
import os.path as osp
from scipy.special import erf, gammainc
from scipy.optimize import root_scalar
from cripticpy import readchk

# Name of the problem this script is for
prob_name = "lindiff"

# Little helper function we use to compute confidence intervals on bin
# counts
def _obj(nexpect, p, nobs):
    return gammainc(nobs+1, nexpect) - p
def _npct(p, nobs):
    sol = root_scalar(_obj, args=(p,nobs), x0=nobs, x1=nobs+1)
    return sol.root

# Check if we are supposed to produce full outputs
parser = argparse.ArgumentParser(
    description="Analysis script for the "+prob_name+" test")
parser.add_argument("--testonly",
                    help="report test results only, do not make plots",
                    default=False, action="store_true")
parser.add_argument("-c", "--chk",
                    help="number of checkpoint file to examine (default "
                    "is last checkpoint file in directory)",
                    default=-1, type=int)
parser.add_argument("-d", "--dir",
                    help="directory containing run to analyze",
                    default=".", type=str)
args = parser.parse_args()

# Extract parameters we need from the input file
fp = open(osp.join(args.dir, 'criptic.in'), 'r')
chkname = 'criptic_'
for line in fp:
    l = line.split('#')[0]    # Strip comments
    s = l.split()
    if len(s) == 0: continue
    if s[0] == 'output.chkname':
        chkname = s[1]
    elif s[0] == 'cr.kPar0':
        kPar0 = float(s[1]) * u.cm**2 / u.s
    elif s[0] == 'cr.kPerp0':
        kPerp0 = float(s[1]) * u.cm**2 / u.s
    elif s[0] == 'cr.kParIdx':
        kParIdx = float(s[1])
    elif s[0] == 'cr.kPerpIdx':
        kPerpIdx = float(s[1])
    elif s[0] == 'prob.L':
        Lsrc = np.array([ float(s1) for s1 in s[1:]]) * u.erg/u.s
    elif s[0] == 'prob.T':
        Tsrc = np.array([ float(s1) for s1 in s[1:]]) * u.GeV
fp.close()

# Figure out which checkpoint file to examine
if args.chk >= 0:
    chknum = "{:05d}".format(args.chk)
else:
    chkfiles = glob(osp.join(args.dir,
                             chkname+"[0-9][0-9][0-9][0-9][0-9].txt"))
    chknums = [ int(c[-9:-4]) for c in chkfiles ]
    chknum = "{:05d}".format(np.amax(chknums))

# Read the checkpoint
t, packets, delpackets, sources = readchk(osp.join(args.dir,
                                                   chkname+chknum))

# Compute dimensionless radius for all packets
chi = kPar0 / kPerp0
r = (np.sqrt(packets['x'][:,0]**2 / chi + packets['x'][:,1]**2 +
             packets['x'][:,2]**2)).to(u.cm)

# Sum packet energy in radial bins for each source, and normalise by
# bin volume to get energy density
U = []
Uup = []
Ulo = []
rbin = []
rc = []
for Ts in Tsrc:
    idx = np.abs((packets['T'] - Ts) / Ts) < 1.0e-6
    w = packets['w'][idx][0]
    T = packets['T'][idx][0]
    count, edges = np.histogram(r[idx], bins=50)
    clo = []
    chi = []
    for c in count:
        chi.append(_npct(0.95, c))
        if c > 0:
            clo.append(_npct(0.05, c))
        else:
            clo.append(0.0)
    Tsum = count *  w * T.to(u.erg)
    Tup = np.array(chi) * w * T.to(u.erg)
    Tlo = np.array(clo) * w * T.to(u.erg)
    edges = edges * u.cm   # Note: histogram destroys unit info
    rbin.append(edges)
    rc.append(0.5 * (edges[1:] + edges[:-1]))
    U.append(Tsum / (4./3. * np.pi * (edges[1:]**3 - edges[:-1]**3)))
    Uup.append(Tup / (4./3. * np.pi * (edges[1:]**3 - edges[:-1]**3)))
    Ulo.append(Tlo / (4./3. * np.pi * (edges[1:]**3 - edges[:-1]**3)))

# Generate corresponding analytic solutions; note that the analytic
# solution below is the analytic integral for the mean energy density
# in a finite-sized bin
Uth = []
for L, T, r_ in zip(Lsrc, Tsrc, rbin):
    idx = np.abs((packets['T'] - T) / T) < 1.0e-6
    p = packets['p'][idx][0]    # Get momentum correspond to this CR energy
    kappa = kPerp0 * (p / (const.m_p * const.c))**kPerpIdx
    r0 = r_[:-1]
    r1 = r_[1:]
    rk = np.sqrt(kappa * t)
    x0 = r0 / (2. * rk)
    x1 = r1 / (2. * rk)
    U_ = 3 * L / \
         (8. * np.pi * kappa * (r1**3 - r0**3)) * \
         (r1**2 - r0**2 -
          2.0 * rk / np.pi**0.5 * (
              r1 * np.exp(-x1**2) - r0 * np.exp(-x0**2)) -
          (r1**2 - 2*rk**2) * erf(x1) + (r0**2 - 2*rk**2) * erf(x0) )
    Uth.append(U_)

# Compute the L1 norm of the error
l1err = []
relerr = []
for L_, U_, Uth_, rb_ in zip(Lsrc, U, Uth, rbin):
    l1err.append(
        np.sum(
            np.abs((U_ - Uth_) * 4./3. * np.pi *
                   (rb_[1:]**3 - rb_[:-1]**3)))
        )
    relerr.append( l1err[-1] / (L*t) )
if np.amax(relerr) < 2e-2:
    print("PASS")
else:
    print("FAIL")

# Make plot if requested
if not args.testonly:
    plt.figure(1)
    plt.clf()
    for i in range(len(Tsrc)):
        plt.plot(rc[i].to(u.pc), Uth[i].to(u.eV/u.cm**3),
                 'C{:d}'.format(i),
                 label='{:d} GeV'.format(
                     int(np.round(Tsrc[i].to(u.GeV) / u.GeV))))
        idx = Ulo[i] == 0*u.eV/u.cm**3
        Utmp = U[i].to(u.eV/u.cm**3).value
        Uerr = np.zeros((2,Utmp.size))
        Uerr[0,:] = 1*(Utmp - Ulo[i].to(u.eV/u.cm**3).value)
        Uerr[1,:] = Uup[i].to(u.eV/u.cm**3).value - Utmp
        plt.errorbar(rc[i].to(u.pc).value, Utmp, yerr=Uerr,
                     fmt='o', ecolor='C{:d}'.format(i),
                     mfc='C{:d}'.format(i),
                     mec='k')
    plt.yscale('log')
    plt.legend()
    plt.xlabel(r'$r$ [pc]')
    plt.ylabel(r'$U_{\mathrm{CR}}$ [eV cm$^{-3}$]')
    plt.savefig(osp.join(args.dir, "result1.pdf"))
