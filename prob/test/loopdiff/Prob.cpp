// This is a problem setup file for the diffusion around a loop test problem

#include <cmath>
#include "Prob.H"
#include "gasmodel/GasModelCartesian.H"
#include "propmodel/PropModelDiffPowerlaw.H"
#include "utils/particleTypes.H"
#include "utils/sr.H"
#include "utils/MPIUtil.H"

using namespace std;
using namespace criptic;

// Routine to initialize CR sources
void initSources(const ParmParser &pp,
		 vector<Real> &x,
		 vector<CRSource> &sources,
		 RngVec &rng)
{
  // Only one MPI rank does this
  if (MPIUtil::IOProc) {
  
    // Get source luminosities, energies, and radius
    vector<Real> LSrc, TSrc;
    pp.get("prob.L", LSrc);
    pp.get("prob.T", TSrc);
    Real rSrc;
    pp.get("prob.r", rSrc);
    if (LSrc.size() != TSrc.size()) {
      MPIUtil::haltRun("Prob: inconsistent number of sources found in input file!",
		       errBadInputFile);
    }

    // Create sources
    IdxType n = LSrc.size();
    x.resize(3*n);
    sources.resize(n);
    for (IdxType i=0; i<n; i++) {
      x[3*i] = rSrc;
      sources[i] = CRSrc::makeSource(partTypes::proton, LSrc[i],
				     TSrc[i], TSrc[i], 0);
    }
  }
}

// Routine to initialize CR packets; this test problem has no packets
// present initially so this routine is empty
void initPackets(const ParmParser &pp,
		 vector<criptic::Real> &x,
		 vector<CRPacket> &packets,
		 RngVec &rng)
{ }


// Function to return the gas model; in this case the gas model
// consists of a uniform density with a magnetic field with field
// lines that form loops around the z axis in the xy plane.
GasModel *initGas(const ParmParser &pp,
		  const Geometry &geom) {
  
  // Construct grid
  GasModelCartesian *gas = new GasModelCartesian(pp, geom);

  // Fill cells; note that the only thing that matters here is the
  // direction of the magnetic field, since losses are turned off and
  // our propagation model is trivial.
  const IdxType *nGrid = gas->gridSize();
  for (IdxType i=0; i<nGrid[0]; i++) {
    for (IdxType j=0; j<nGrid[1]; j++) {
      for (IdxType k=0; k<nGrid[2]; k++) {
	vector<IdxType> idx = {i, j, k};
	vector<Real> pos(3);
	gas->idxToPos(idx, pos);
	(*gas)(idx,GasModelCartesian::totDenIdx) = 1.0;
	(*gas)(idx,GasModelCartesian::ionDenIdx) = 1.0;
	(*gas)(idx,GasModelCartesian::vxIdx) = 0.0;
	(*gas)(idx,GasModelCartesian::vyIdx) = 0.0;
	(*gas)(idx,GasModelCartesian::vzIdx) = 0.0;
	(*gas)(idx,GasModelCartesian::BxIdx) = -pos[1] /
	  sqrt(pos[0]*pos[0] + pos[1]*pos[1]);
	(*gas)(idx,GasModelCartesian::ByIdx) = pos[0] /
	  sqrt(pos[0]*pos[0] + pos[1]*pos[1]);
	(*gas)(idx,GasModelCartesian::BzIdx) = 0.0;
      }
    }
  }  
  return gas;
}

// Function to return the propagation model; this test uses the
// standard diffusive powerlaw model
PropModel *initProp(const ParmParser &pp) {
  return new PropModelDiffPowerlaw(pp);
}
  
// User work function. This is a no-op for this test.
void userWork(Real t, GasModel &gas, PropModel &prop, CRTree &tree) { }
