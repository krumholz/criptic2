"""
Analysis script for the diffusion around a loop test problem
"""

import argparse
import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u
import astropy.constants as const
from glob import glob
import os.path as osp
from scipy.special import erf, gammainc
from scipy.optimize import root_scalar
from cripticpy import readchk

# Name of the problem this script is for
prob_name = "loopdiff"

# Check if we are supposed to produce full outputs
parser = argparse.ArgumentParser(
    description="Analysis script for the "+prob_name+" test")
parser.add_argument("--testonly",
                    help="report test results only, do not make plots",
                    default=False, action="store_true")
parser.add_argument("-c", "--chk",
                    help="number of checkpoint file to examine (default "
                    "is last checkpoint file in directory)",
                    default=-1, type=int)
parser.add_argument("-d", "--dir",
                    help="directory containing run to analyze",
                    default=".", type=str)
args = parser.parse_args()

# Extract parameters we need from the input file
fp = open(osp.join(args.dir, 'criptic.in'), 'r')
chkname = 'criptic_'
for line in fp:
    l = line.split('#')[0]    # Strip comments
    s = l.split()
    if len(s) == 0: continue
    if s[0] == 'output.chkname':
        chkname = s[1]
    elif s[0] == 'cr.kPar0':
        kPar0 = float(s[1]) * u.cm**2 / u.s
    elif s[0] == 'cr.kParIdx':
        kParIdx = float(s[1])
    elif s[0] == 'prob.L':
        Lsrc = np.array([ float(s1) for s1 in s[1:]]) * u.erg/u.s
    elif s[0] == 'prob.T':
        Tsrc = np.array([ float(s1) for s1 in s[1:]]) * u.GeV
    elif s[0] == 'prob.r':
        rsrc = float(s[1]) * u.cm
fp.close()

# Figure out which checkpoint file to examine
if args.chk >= 0:
    chknum = "{:05d}".format(args.chk)
else:
    chkfiles = glob(osp.join(args.dir,
                             chkname+"[0-9][0-9][0-9][0-9][0-9].txt"))
    chknums = [ int(c[-9:-4]) for c in chkfiles ]
    chknum = "{:05d}".format(np.amax(chknums))

# Read the checkpoint
t, packets, delpackets, sources = readchk(osp.join(args.dir,
                                                   chkname+chknum))

# Convert packet positions to cylindrical coordinates
r = (packets['x'][:,0]**2 + packets['x'][:,1]**2)**0.5
phi = np.arctan(packets['x'][:,1] / packets['x'][:,0])
idx = np.logical_and(packets['x'][:,0] < 0, packets['x'][:,1] < 0)
phi[idx] = phi[idx] - np.pi*u.rad
idx = np.logical_and(packets['x'][:,0] < 0, packets['x'][:,1] > 0)
phi[idx] = phi[idx] + np.pi*u.rad
z = packets['x'][:,2]

# Compute the energy per radian along the ring
U = []
phi_edge = np.linspace(-np.pi, np.pi, 51)
phi_c = 0.5 * (phi_edge[1:] + phi_edge[:-1])
dphi = phi_edge[1] - phi_edge[0]
for Ts in Tsrc:
    idx = np.abs((packets['T'] - Ts) / Ts) < 1.0e-6
    w = packets['w'][idx][0]
    T = packets['T'][idx][0]
    count, e = np.histogram(phi[idx], bins=phi_edge)
    Tsum = count * w * T.to(u.erg)
    U.append( Tsum / (phi_edge[1]-phi_edge[0]) )

# Define a function that returns the analytic solution for a given L,
# r, and kappa, averaged over bins with specified edges
def Uexact(kappa, r, L, t, phi_edge, nterm=100):
    k = kappa / r**2
    dphi = phi_edge[1:] - phi_edge[:-1]
    n = np.arange(nterm)+1
    f = (np.exp(-n**2 * k * t) - 1) / n**3 * \
        (np.sin(np.outer(phi_edge[1:], n)) -
         np.sin(np.outer(phi_edge[:-1], n)))
    U = L / np.pi * (t/2 - 1/k * np.sum(f, axis=1) / dphi)
    return U

# Get analytic solution for each source; note that the analytic
# solution is the analytic integral of the mean energy per radian in
# each bin
Uth = []
for L, T in zip(Lsrc, Tsrc):
    idx = np.abs((packets['T'] - T) / T) < 1.0e-6
    p = packets['p'][idx][0]    # Get momentum correspond to this CR energy
    kappa = kPar0 * (p / (const.m_p * const.c))**kParIdx
    Uth.append(Uexact(kappa, rsrc, L, t, phi_edge))

# Compute L1 norm of error on energy
l1err = []
relerr = []
for L, U_, Uth_ in zip(Lsrc, U, Uth):
    l1err.append( np.sum(np.abs(U_ - Uth_))*dphi )
    relerr.append( l1err[-1] / (L*t) )

# Compute maximum error in packet position
poserr = np.amax( ((r - rsrc)**2 + z**2)**0.5 )
relposerr = poserr / rsrc

# Report result
if np.amax(relerr) < 1e-2 and relposerr < 1.0e-3:
    print("PASS")
else:
    print("FAIL")

# Make plot if requested
if not args.testonly:
    plt.figure(1)
    plt.clf()
    for i in range(len(Tsrc)):
        plt.plot(phi_c, Uth[i].to(u.erg),
                 'C{:d}'.format(i),
                 label='{:d} GeV'.format(
                     int(np.round(Tsrc[i].to(u.GeV) / u.GeV))))
        plt.plot(phi_c, U[i].to(u.erg), marker='o',
                 mfc='C{:d}'.format(i),
                 mec='k')
    plt.legend()
    plt.xlabel(r'$\phi$')
    plt.ylabel(r'$dE_\mathrm{CR}/d\phi$ [erg rad$^{-1}$]')
    plt.xlim([-np.pi,np.pi])
    plt.xticks(np.pi * np.array([-1, -0.5, 0, 0.5, 1]))
    plt.gca().set_xticklabels([r'$-\pi$', r'$-\pi/2$', r'$0$',
                               r'$\pi/2$', r'$\pi$'])
    plt.savefig(osp.join(args.dir, "result1.pdf"))
                 
