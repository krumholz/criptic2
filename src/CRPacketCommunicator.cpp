#ifdef ENABLE_MPI

#include "CRPacketCommunicator.H"
#include "CRTree.H"
#include <algorithm>

using namespace std;
using namespace criptic;
using namespace MPIUtil;

// Constructor and destructor
CRPacketCommunicator::
CRPacketCommunicator(const controls::params& par_) : par(par_) {

  // Initialize the "all done" array
  rankDone.resize(nRank);
  rankDoneHandle.resize(nRank);

  // Initialize outgoing request and response queues to have one entry
  // for each MPI rank
  outgoingRequests.resize(nRank);
  outgoingResponses.resize(nRank);
  outgoingRequestHandles.resize(nRank);
  outgoingResponseHandles.resize(nRank);
  outgoingRequestSeq.resize(nRank);
  outgoingResponseSeq.resize(nRank);

  // Initialize the first blocks in the request and response queue
  for (int i=0; i<nRank; i++) {
    vector<ngbSearchRequest> req;
    outgoingRequests[i].push(req);
    outgoingRequests[i].back().reserve(controls::numNgb);
    vector<ngbSearchResponse> resp;
    outgoingResponses[i].push(resp);
    outgoingResponses[i].back().reserve(controls::numNgb);
  }
  
  // Mark that our rank is not done
  rankDone[myRank] = 0;

  // Initialize openMP locks
#ifdef _OPENMP
  omp_init_lock(&deferredPacketLock);
  omp_init_lock(&readyPacketLock);
  outgoingRequestLocks.resize(nRank);
  for (int i=0; i<nRank; i++) {
    if (i == myRank) continue;
    omp_init_lock(&outgoingRequestLocks[i]);
    omp_init_lock(&outgoingResponseLocks[i]);
  }
#endif
}

CRPacketCommunicator::~CRPacketCommunicator() {
#ifdef _OPENMP
  omp_destroy_lock(&deferredPacketLock);
  omp_destroy_lock(&readyPacketLock);
  for (int i=0; i<nRank; i++) {
    if (i == myRank) continue;
    omp_destroy_lock(&outgoingRequestLocks[i]);
    omp_destroy_lock(&outgoingResponseLocks[i]);
  }
#endif
}


// Method to check if all work queues are empty; this requires
// locking, since more than one thread can modify the deferredPackets
// and readyPackets queues. However, we only need to test the lock,
// since, if the lock is not available, we know that the queue is not
// empty.
bool CRPacketCommunicator::allWorkQueuesEmpty() {

  // First check the incoming request queue, which does not require
  // locking
  if (!incomingRequests.empty()) return false;

  // Now check the deferred packet queue
#ifdef _OPENMP
  if (!omp_test_lock(&deferredPacketLock)) return false;
#endif
  bool empty = deferredPackets.empty();
#ifdef _OPENMP
  omp_unset_lock(&deferredPacketLock);
#endif
  if (!empty) return false;

  // Finally check the ready packet queue
#ifdef _OPENMP
  if (!omp_test_lock(&readyPacketLock)) return false;
#endif
  empty = readyPackets.empty();
#ifdef _OPENMP
  omp_unset_lock(&readyPacketLock);
#endif
  if (!empty) return false;

  // If we have gotten here, all queues are empty
  return true;
}
  

// This method grabs an incoming request off the queue for processing;
// if none are available, it returns an empty request with the rank of
// the originating processor set to -1 as a flag. Note that we do not
// need to lock here, because only the master thread ever modifies
// this queue.
ngbSearchReq CRPacketCommunicator::getIncomingRequest() {
  ngbSearchReq req;
  if (incomingRequests.empty()) req.rank = -1;
  else {
    req = incomingRequests.front();
    incomingRequests.pop();
  }
  return req;
}

// This method grabs a packet off the ready queue for processing by
// local threads
deferredPacketData *CRPacketCommunicator::getReadyPacket() {

  // Try to acquire lock on queue; if we fail, return a null pointer
#ifdef _OPENMP
  if (!omp_test_lock(&readyPacketLock)) return nullptr;
#endif

  // We now have the lock, so we can safely pull a request off the
  // queue
  deferredPacketData *p = nullptr;
  if (!readyPackets.empty()) {
    p = readyPackets.front();
    readyPackets.pop();
  }

  // Release the lock
#ifdef _OPENMP
  omp_unset_lock(&readyPacketLock);
#endif

  // Return the packet we just pulled
  return p;
}

// This method queues a response to a search request from another rank
void
CRPacketCommunicator::queueResponse(ngbSearchResp& resp) {

  // Lock queue for the rank we are adding to
  int rank = resp.rank;
#ifdef _OPENMP
  omp_set_lock(&outgoingResponseLocks[rank]);
#endif
  
  // Add to queue
  outgoingResponses[rank].back().push_back(resp.resp);

  // Release lock
#ifdef _OPENMP
  omp_unset_lock(&outgoingResponseLocks[rank]);
#endif
}


// Method to indicate to other ranks that this rank is done
void CRPacketCommunicator::markDone() {
  // If we are changing state from "not done" to "done", record that,
  // and broadcast information to all other ranks
  if (!rankDone[myRank]) {
    rankDone[myRank] = 1;
    for (int i=0; i<nRank; i++) {
      if (i == myRank) continue;
      MPI_Isend(rankDone.data() + myRank, 1, MPI_INT, i,
		tagCycleComplete, MPI_COMM_WORLD,
		rankDoneHandle.data() + i);
      if (par.verbosity > 2) {
	cout << "CRPacketCommunicator: rank " << myRank
	     << ": sending completion signal to rank "
	     << i << endl;
      }
    }
  }
}


// This method registers requests for searches on other MPI ranks
void
CRPacketCommunicator::requestSearch(const vector<int>& ranks,
				    const vector<int>& numNgb,
				    const IdxType target,
				    const vector<IdxType> &ngb,
				    const vector<Real> &dist,
				    const Real searchCtr[3],
				    const Real eMin) {

  // First we need to add this packet to the deferred packet list
  deferredPacketData *data = new deferredPacketData;
  data->target = target;
  data->ngb = ngb;
  data->dist = dist;
  data->nReq = ranks.size();
#ifdef _OPENMP
  omp_set_lock(&deferredPacketLock);
#endif
  deferredPackets.push_back(data);
#ifdef _OPENMP
  omp_unset_lock(&deferredPacketLock);
#endif

  // Next we need to create a search request for each rank
  vector<ngbSearchRequest> req(ranks.size());
  for (IdxType i=0; i<req.size(); i++) {
    req[i].target = target;
    req[i].numNgb = numNgb[i];
    for (int j=0; j<3; j++) req[i].searchCtr[j] = searchCtr[j];
    req[i].distMax = dist.back();
    req[i].eMin = eMin;
  }

  // Add to request queue for appropriate rank
  for (IdxType i=0; i<req.size(); i++) {
#ifdef _OPENMP
    omp_set_lock(&outgoingRequestLocks[i]);
#endif
    outgoingRequests[ranks[i]].back().push_back(req[i]);
#ifdef _OPENMP
    omp_unset_lock(&outgoingRequestLocks[i]);
#endif
  }
}


// Method to update deferred packets using the responses provided by
// neighbors
void CRPacketCommunicator::
updateDeferredPackets(const vector<ngbSearchResponse>& responses) {

  // Step 1: grab pointers to the deferred packets we need to update
  vector<list<deferredPacketData *>::iterator>
    dpToUpdate(responses.size());
#ifdef _OPENMP
  omp_set_lock(&deferredPacketLock);
#endif    
  for (IdxType i=0; i<responses.size(); i++) {
    // Find the packet this response is for in the deferredPacket
    // list; note that we don't do anything fancy with this search
    // and just start at the beginning of the list, because much of
    // the time the responses are going to come back in the same
    // order that packets were placed in the deferred packet queue
    // in the first place
    list<deferredPacketData *>::iterator it;
    for (it=deferredPackets.begin();
	 it != deferredPackets.end();
	 ++it) {
      if (responses[i].target == (*it)->target) {
	dpToUpdate[i] = it;
	break;
      }
    }
    assert(it != deferredPackets.end()); // Safety check
  }
#ifdef _OPENMP
  omp_unset_lock(&deferredPacketLock);
#endif

  // Step 2: go through the deferred packets we have found and
  // update their properties using the received responses; no need
  // to lock while doing so, since no other thread touches the data in
  // deferred packets
  queue<list<deferredPacketData *>::iterator> dpReady;
  for (IdxType i=0; i<responses.size(); i++) {

    // Reference to object we're working on
    deferredPacketData *dp = *(dpToUpdate[i]);
    
    // Decrease the number of requests this packet is waiting on; if
    // this reduces the number to zero, save a pointer to the data
    // in the queue that we will move onto the readyPacket list
    dp->nReq--;
    if (dp->nReq == 0) dpReady.push(dpToUpdate[i]);

    // Create merged list of neighbors and distances
    vector<IdxType> ngb;
    vector<Real> dist;
    ngb.reserve(controls::numNgb);
    dist.reserve(controls::numNgb);
    IdxType locPtr = 0, extPtr = 0;
    while (ngb.size() < controls::numNgb) {

      // Get distances to next packet in local and external lists
      Real locDist = maxReal;
      if (locPtr < dp->ngb.size()) locDist = dp->dist[locPtr];
      Real extDist = responses[i].dist[extPtr];
	    
      // If both distances are maxreal, we're out of packets, so stop
      if (locDist == maxReal && extDist == maxReal) break;

      // Compare local and external distance
      if (locDist < extDist) {
	// Local packet is closer, so just copy to merged list
	ngb.push_back(dp->ngb[locPtr]);
	dist.push_back(dp->dist[locPtr]);
	locPtr++;
      } else {
	// Remote packet is closer, so save remote packet in
	// the imported packet list for this deferred packet, and
	// point to it
	IdxType n = dp->pd.size();
	dp->x.resize(3*(n+1));
	for (int j=0; j<3; j++)
	  dp->x[3*n+j] = responses[i].x[3*extPtr + j];
	dp->pd.push_back(responses[i].packets[extPtr]);
	ngb.push_back(n | extNodeFlag);
	dist.push_back(responses[i].dist[extPtr]);
	extPtr++;
      }
    }

    // Store the final neighbor and distance lists
    dp->ngb = ngb;
    dp->dist = dist;
  }

  // Step 3: go through the list of packets that are now ready; for
  // each of them, delete it from the deferredPacket list, and move
  // it onto the readyPacket queue. This requires locking both.
  if (!dpReady.empty()) {
#ifdef _OPENMP
    omp_set_lock(&deferredPacketLock);
    omp_set_lock(&readyPacketLock);
#endif
    while (!dpReady.empty()) {
      readyPackets.push( *(dpReady.front()) );
      deferredPackets.erase(dpReady.front());
      dpReady.pop();
    }
  }
#ifdef _OPENMP
  omp_unset_lock(&deferredPacketLock);
  omp_unset_lock(&readyPacketLock);
#endif
  
}


// This is the main communication method; it carries the following
// steps in order: (1) probe for incoming requests from other ranks,
// and, if any are found, receives them and places them into the
// incomingRequests queue; (2) probe for incoming responses from other
// ranks, and, if any are found, uses them to update the information
// in the deferredPackets list and moves them to the readyPackets
// queue; (3) check the outgoingRequests queue and see if there are
// requests that should go to other MPI ranks; if there are, send
// them; (4) check the outgoingResponses queue and see if there are
// responses that should be send to other MPI ranks; if there are send
// them
void CRPacketCommunicator::communicate(bool flush) {

  // Step 1: probe for messages from other ranks; these can be either
  // requests for a neighbor search, responses to our requests, or the
  // "all done" signal that indicates that a given rank has processed
  // all of its packets and will be sending any further requests
  while (true) {

    // Probe for incoming messages
    int msgFound;
    MPI_Status stat;
    MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD,
	       &msgFound, &stat);

    // If no messages found, go to next step
    if (!msgFound) break;

    // We have found a message. The next step is to determine if it is
    // a request for a search, or response to our own request; we
    // decide between these two based on the message tag
    if (stat.MPI_TAG >= tagNgbSearchReq) {

      // This is a request for a neighbor search; see how many
      // requests are incoming, and allocate memory
      int nreq;
      MPI_Get_count(&stat, ngbSearchReqType, &nreq);
      vector<ngbSearchRequest> req(nreq);

      // Print if very verbose
      if (par.verbosity > 2) {
	int msgNum = stat.MPI_TAG - tagNgbSearchReq;
	cout << "CRPacketCommunicator: rank " << myRank
	     << ": receiving request message " << msgNum
	     << " from rank " << stat.MPI_SOURCE
	     << " with tag " << stat.MPI_TAG
	     << "; message contains " << nreq
	     << " requests"
	     << endl;
      }

      // Receive the data
      MPI_Recv(req.data(), nreq, ngbSearchReqType,
	       stat.MPI_SOURCE, stat.MPI_TAG,
	       MPI_COMM_WORLD, MPI_STATUS_IGNORE);

      // Push requests onto incoming request queue
      for (int i=0; i<nreq; i++) {
	ngbSearchReq r;
	r.req = req[i];
	r.rank = stat.MPI_SOURCE;
	incomingRequests.push(r);
      }
      
    } else if (stat.MPI_TAG >= tagNgbSearchResp) {

      // This is a response to our neighbor search request; see
      // how many responses are incoming, and allocate memory
      int nresp;
      MPI_Get_count(&stat, ngbSearchRespType, &nresp);
      vector<ngbSearchResponse> resp(nresp);

      // Print if very verbose
      if (par.verbosity > 2) {
	int msgNum = stat.MPI_TAG - tagNgbSearchResp;
	cout << "CRPacketCommunicator: rank " << myRank
	     << ": receiving response message " << msgNum
	     << " from rank " << stat.MPI_SOURCE
	     << " with tag " << stat.MPI_TAG
	     << "; message contains " << nresp
	     << " responses"
	     << endl;
      }
      
      // Receive the data
      MPI_Recv(resp.data(), nresp, ngbSearchRespType,
	       stat.MPI_SOURCE, stat.MPI_TAG,
	       MPI_COMM_WORLD, MPI_STATUS_IGNORE);

      // Use the data to update deferred packets
      updateDeferredPackets(resp);
      
    } else {

      // This response is the "all done" message; receive it to
      // complete the communcation cycle
      MPI_Recv(rankDone.data() + stat.MPI_SOURCE, 1, MPI_INT,
	       stat.MPI_SOURCE, stat.MPI_TAG, MPI_COMM_WORLD,
	       MPI_STATUS_IGNORE);
      
    }
      
  }


  // Step 2: dispatch outgoing requests for neighor searches
  for (int i=0; i<nRank; i++) {

    // Point to the current block of requests
    vector<ngbSearchRequest>& req = outgoingRequests[i].back();

    // If no new requests, do nothing
    if (req.size() == 0) continue;

    // If number of requests is smaller than the block size and we
    // have not been told to flush, do nothing
    if (req.size() < controls::MPIBlockSize && !flush) continue;

    // Make tag and send data
    MPI_Request r;
    int tag = outgoingRequestSeq[i] + tagNgbSearchReq;
    MPI_Isend(req.data(), req.size(), ngbSearchReqType, i, tag,
	      MPI_COMM_WORLD, &r);

    // Print if very verbose
    if (par.verbosity > 2) {
      cout << "CRPacketCommunicator: rank " << myRank
	   << ": sent request message "
	   << outgoingRequestSeq[i]
	   << " with tag " << tag
	   << " to rank " << i
	   << "; message contains " << req.size()
	   << " requests"
	   << endl;
    }
    
    // Save request handle
    outgoingRequestHandles[i].push(r);

    // Allocate a new block of memory to hold the next set of
    // requests
    vector<ngbSearchRequest> v;
    outgoingRequests[i].push(v);
    outgoingRequests[i].back().reserve(2*controls::MPIBlockSize);

    // Increment counter
    outgoingRequestSeq[i]++;
  }
  

  // Step 3: dispatch outgoing responses
  for (int i=0; i<nRank; i++) {

    // Point to the current block of responses
    vector<ngbSearchResponse>& resp = outgoingResponses[i].back();

    // If no new responses, do nothing
    if (resp.size() == 0) continue;

    // If number of responses is smaller than the message block size,
    // and we have not been told to flush, do nothing
    if (resp.size() < controls::MPIBlockSize && !flush) continue;

    // Make tag and send data
    MPI_Request r;
    int tag = outgoingResponseSeq[i] + tagNgbSearchResp;
    MPI_Isend(resp.data(), resp.size(), ngbSearchRespType, i, tag,
	      MPI_COMM_WORLD, &r);

    // Print if very verbose
    if (par.verbosity > 2) {
      cout << "CRPacketCommunicator: rank " << myRank
	   << ": sent response message "
	   << outgoingResponseSeq[i]
	   << " with tag " << tag
	   << " to rank " << i
	   << "; message contains " << resp.size()
	   << " responses"
	   << endl;
    }

    // Save handle
    outgoingResponseHandles[i].push(r);

    // Allocate a new block of memory to hold the next set of
    // responses
    vector<ngbSearchResponse> v;
    outgoingResponses[i].push(v);
    outgoingResponses[i].back().reserve(2*controls::MPIBlockSize);

    // Increment counter
    outgoingResponseSeq[i]++;
  }

  
  // Step 4: test for completion of outgoing sends, and free memory
  // for those that have completed
  for (int i=0; i<nRank; i++) {

    // Check request handles
    while (outgoingRequestHandles[i].size() > 0) {
      MPI_Request &r = outgoingRequestHandles[i].front();
      int completed;
      MPI_Test(&r, &completed, MPI_STATUS_IGNORE);
      if (!completed) break;
      outgoingRequests[i].pop();
      outgoingRequestHandles[i].pop();
    }

    // Check response handles
    while (outgoingResponseHandles[i].size() > 0) {
      MPI_Request &r = outgoingResponseHandles[i].front();
      int completed;
      MPI_Test(&r, &completed, MPI_STATUS_IGNORE);
      if (!completed) break;
      outgoingResponses[i].pop();
      outgoingResponseHandles[i].pop();
    }
  }
}


// This method makes sure that the buffers for all outgoing requests
// and responses are cleared, all counters reset to zero, all flags
// set to "not done", in preparation for a new communications cycle
void CRPacketCommunicator::finalizeCycle() {
  for (int i=0; i<nRank; i++) {
    while (outgoingRequests[i].size() > 1)
      outgoingRequests[i].pop();
    outgoingRequests[i].back().resize(0);
    while (outgoingResponses[i].size() > 1)
      outgoingResponses[i].pop();
    outgoingResponses[i].back().resize(0);
    while (outgoingRequestHandles[i].size() > 0)
      outgoingRequestHandles[i].pop();
    while (outgoingResponseHandles[i].size() > 0)
      outgoingResponseHandles[i].pop();
    outgoingRequestSeq[i] = 0;
    outgoingResponseSeq[i] = 0;
    rankDone[i] = 0;
  }
}


#endif
// ENABLE_MPI
