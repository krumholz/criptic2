#include "CRSource.H"
#include <cmath>

using namespace criptic;
using namespace std;


// Function to return a CR source that corresponds to a particular
// luminosity and particular energy limits, expressed in
// human-readable units
CRSource CRSrc::makeSource(const partTypes::pType type,
			   const criptic::Real& lum_metric,
			   const criptic::Real& T0_GeV,
			   const criptic::Real& T1_GeV,
			   const criptic::Real& q) {
  CRSource s;
  s.type = type;
  s.p0 = sr::p_from_T(type, T0_GeV * units::GeV / constants::mp_c2);
  s.p1 = sr::p_from_T(type, T1_GeV * units::GeV / constants::mp_c2);
  s.q = q;
  s.coef = (lum_metric / constants::mp_c2) / TFac(s);
  return s;
}

// Method to draw packets
vector<CRPacket> CRSrc::drawPackets(const CRSource& s,
				    const IdxType& n,
				    const Real& tStart,
				    const Real& tStop,
				    const Real& qSamp,
				    const RngVec& rng) {

  // Allocate space to hold new packets
  vector<CRPacket> p(n);

  // Loop over packets to be drawn
  Real w = 0.0;
#ifdef _OPENMP
#pragma omp parallel for reduction(+:w)
#endif
  for (IdxType i=0; i<n; i++) {

    // Set CR packet properties
    p[i].type = s.type;
    p[i].src = s.uniqueID;
    p[i].tInj = rng.uniform(tStart, tStop);
    p[i].gr = 0.0;

    // Set momentum; be careful to handle special case where p1 = p0
    if (s.p1 > s.p0) {
      Real y = rng.uniform();
      if (qSamp == -1.0) {
	p[i].p = s.p0 * exp(y * log(s.p1/s.p0));
      } else {
	Real p0q = pow(s.p0, qSamp+1);
	Real p1q = pow(s.p1, qSamp+1);
	p[i].p = pow(y*p0q + (1-y)*p1q, 1/(qSamp+1));
      }
    } else {
      p[i].p = s.p0;
    }

    // Set weight scaling
    p[i].w = pow(p[i].p, s.q-qSamp);
    w += p[i].w;

  }

  // Rescale weights to get correct total
  Real scale = nLum(s) * (tStop - tStart) / w;
#ifdef _OPENMP
#pragma omp parallel for
#endif
  for (IdxType i=0; i<n; i++) {
    p[i].w *= scale;
    p[i].wInj = p[i].w;
  }

  // Return
  return p;
}
