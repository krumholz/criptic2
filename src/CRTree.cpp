#include "CRTree.H"
#include "CRPacketCommunicator.H"
#include "globalControls.H"
#include "ngbKernel.H"
#include "PropKernel.H"
#include <algorithm>
#include <queue>
#include <utility>

using namespace std;
using namespace criptic;
using namespace Peano;
namespace fQ = fieldQuantities;

/////////////////////////////////////////////////////////////////////
// Functions to apply masks and flags to node indices; just defined
// here to increase code readability
/////////////////////////////////////////////////////////////////////
typedef enum {
  nodeNull,
  nodePacket,
  nodeLocal,
  nodeExt
} nodeType;

inline nodeType nType(const IdxType n) {
  if (n == nullNode) return nodeNull;
  else if (n & localNodeFlag) return nodeLocal;
  else if (n & extNodeFlag) return nodeExt;
  else return nodePacket;
}
inline bool isNullNode(const IdxType n) {
  return n == nullNode;
}
inline bool isPacketNode(const IdxType n) {
  return nType(n) == nodePacket;
}
inline bool isLocalNode(const IdxType n) {
  return nType(n) == nodeLocal;
}
inline bool isExtNode(const IdxType n) {
  return nType(n) == nodeExt;
}
inline IdxType addLocalFlag(const IdxType n) {
  return n | localNodeFlag;
}
inline IdxType removeLocalFlag(const IdxType n) {
  return n & localNodeMask;
}
inline IdxType addExtFlag(const IdxType n) {
  return n | extNodeFlag;
}
inline IdxType removeExtFlag(const IdxType n) {
  return n & extNodeMask;
}


/////////////////////////////////////////////////////////////////////
// Trivial little utility functions
/////////////////////////////////////////////////////////////////////

// Set the Peano key for all packets and sources
inline void CRTree::setPeanoKeys() {
  peanoKeys.resize(pd.size());
  srcPeanoKeys.resize(src.size());
#ifdef _OPENMP
#pragma omp parallel for
#endif
  for (IdxType i=0; i<peanoKeys.size(); i++) {
    int ijk[3];
    for (int n = 0; n < 3; n++)
      ijk[n] = (int) ((x[3*i+n] - domLo[n]) * domFac);
    peanoKeys[i] = PeanoKey(ijk[0], ijk[1], ijk[2]);
  }
#ifdef _OPENMP
#pragma omp parallel for
#endif
  for (IdxType i=0; i<srcPeanoKeys.size(); i++) {
    int ijk[3];
    for (int n = 0; n < 3; n++)
      ijk[n] = (int) ((xSrc[3*i+n] - domLo[n]) * domFac);
    srcPeanoKeys[i] = PeanoKey(ijk[0], ijk[1], ijk[2]);
  }
}

// Set the Morton key for all packets
inline void CRTree::setMortonKeys() {
  mortonKeys.resize(pd.size());
#ifdef _OPENMP
#pragma omp parallel for
#endif
  for (IdxType i=0; i<mortonKeys.size(); i++) {
    int ijk[3];
    for (int n = 0; n < 3; n++)
      ijk[n] = (int) ((x[3*i+n] - domLo[n]) * domFac);
    mortonKeys[i] = MortonKey(ijk[0], ijk[1], ijk[2]);
  }
}

// Compute the leaf number for a packet or source in the topTree
inline IdxType CRTree::getTopTreeLeaf(const IdxType i) const {
  IdxType no = rootNode;
  while (topNodes[no].child != nullNode) {
    no = topNodes[no].child + (peanoKeys[i] - topNodes[no].start) /
      (topNodes[no].size >> 3);
  }
  return no;
}
inline IdxType CRTree::getTopTreeLeafSrc(const IdxType i) const {
  IdxType no = rootNode;
  while (topNodes[no].child != nullNode) {
    no = topNodes[no].child + (srcPeanoKeys[i] - topNodes[no].start) /
      (topNodes[no].size >> 3);
  }
  return no;
}


/////////////////////////////////////////////////////////////////////
// Constructors
/////////////////////////////////////////////////////////////////////

// Version to build an empty tree containing no packets
CRTree::CRTree(const Geometry& geom_,
	       const PropModel& prop_,
	       const GasModel& gas_,
	       const RngVec& rng_,
	       const controls::params& par_) :
  geom(geom_), prop(prop_), gas(gas_), rng(rng_), par(par_)
#ifdef ENABLE_MPI
  , comm(par_)
#endif
{
  // Initialize packet data
  uniqueID = 0;
  totPacket = 0;
  totCost = 0.0;
}

// Constructor version that takes an initial data set
CRTree::CRTree(const Geometry& geom_,
	       const PropModel& prop_,
	       const GasModel& gas_,
	       const RngVec& rng_,
	       const controls::params& par_,
	       const vector<Real> &x_,
	       const vector<CRPacket> &pd_,
	       const vector<Real> &xSrc_,
	       const vector<CRSource> &src_) :
  geom(geom_), prop(prop_), gas(gas_), rng(rng_), par(par_)
#ifdef ENABLE_MPI
  , comm(par_)
#endif
{
  // Initialize packet data
  uniqueID = 0;
  totPacket = 0;
  totCost = 0.0;

  // Build tree from initial data  
  addPacketsAndSources(x_, pd_, xSrc_, src_);
  rebuild();
}


/////////////////////////////////////////////////////////////////////
// Routines to add packets and sources to the tree. These routines
// also automatically update the uniqueID pointers, so that unique IDs
// remain distinct on different MPI ranks
/////////////////////////////////////////////////////////////////////

// Add packets and sources to incoming buffers
void CRTree::addPacketsAndSources(const vector<Real> &x_,
				  const vector<CRPacket> &pd_,
				  const vector<Real> &xSrc_,
				  const vector<CRSource> &src_) {

  // Add to packets buffer
  IdxType packetPtr = pdbuf.size();
  copy(x_.begin(), x_.end(), back_inserter(xbuf));
  copy(pd_.begin(), pd_.end(), back_inserter(pdbuf));

  // Resize field quantity and smoothing length arrays to contain
  // holders for these packets
  if (fQ::nFieldQty > 0) {
    qtybuf.resize( qtybuf.size() + fQ::nFieldQty * pd_.size() );
    hbuf.resize( hbuf.size() + fQ::nFieldQty * pd_.size() );
  }

  // Initialize cost for these packets to mean cost of all other
  // packets
  criptic::Real meanCost = (totPacket > 0) ? totCost / totPacket : 1.0;
  costbuf.insert(costbuf.end(), pd_.size(), meanCost);

  // Add sources to buffer
  IdxType srcPtr = srcbuf.size();
  copy(xSrc_.begin(), xSrc_.end(), back_inserter(xSrcbuf));
  copy(src_.begin(), src_.end(), back_inserter(srcbuf));

  // Synchronize uniqueID pointer across ranks; the way this works is
  // that every rank starts with the same unique ID pointer, and then
  // we do an AllToAll where every rank tells all the other ranks how
  // many packets and sources it is adding, then all ranks offset
  // their uniqueIDs by the number being added on lower ranks, e.g.,
  // if rank 0 is adding 50 packets, rank 1 is adding 100 packets, and
  // rank 2 is adding 200 packets, then rank 0 will increase its
  // uniqueID by 0, rank 1 will increase its uniqueID by 50, and rank
  // 2 will increase its uniqueID by 150.
#ifdef ENABLE_MPI
  vector<IdxType> nAdd(MPIUtil::nRank);
  nAdd[MPIUtil::myRank] = pd_.size() + src_.size();
  MPI_Alltoall(MPI_IN_PLACE, 1, MPI_SZ, nAdd.data(), 1, MPI_SZ,
	       MPI_COMM_WORLD);
  for (IdxType i=0; i<MPIUtil::myRank; i++) uniqueID += nAdd[i];
#endif

  // Assign uniqueID's to the packets and sources we're adding
  for (IdxType i=packetPtr; i<pdbuf.size(); i++) {
    pdbuf[i].uniqueID = uniqueID;
    uniqueID++;
  }
  for (IdxType i=srcPtr; i<srcbuf.size(); i++) {
    srcbuf[i].uniqueID = uniqueID;
    uniqueID++;
  }
}

// Add packet and add source routines; these just invoke the general
// add routine with either zero sources or zero packets
void CRTree::addPackets(const vector<Real> &x_,
			const vector<CRPacket> &pd_) {
  vector<Real> xSrc_;
  vector<CRSource> src_;
  addPacketsAndSources(x_, pd_, xSrc_, src_);
}
void CRTree::addSources(const vector<Real> &xSrc_,
			const vector<CRSource> &src_) {
  vector<Real> x_;
  vector<CRPacket> pd_;
  addPacketsAndSources(x_, pd_, xSrc_, src_);
}
  

/////////////////////////////////////////////////////////////////////
// Routine to determine the global bounding box of all packets,
// along with the total packet count
/////////////////////////////////////////////////////////////////////
void CRTree::setupDomain() {

  // Initialize limits on domain domain
  Real &domLoX = domLo[0];
  Real &domLoY = domLo[1];
  Real &domLoZ = domLo[2];
  Real &domHiX = domHi[0];
  Real &domHiY = domHi[1];
  Real &domHiZ = domHi[2];
  domLoX = domLoY = domLoZ = maxReal;
  domHiX = domHiY = domHiZ = -maxReal;

  // First go through the local data; note that we traverse all
  // packets here rather than using the tree, because the goal is to
  // rebuild the tree after packets move, or are added or deleted
#ifdef _OPENMP
#pragma omp parallel for reduction(min:domLoX,domLoY,domLoZ) reduction(max:domHiX,domHiY,domHiZ)
#endif
  for (vector<Real>::size_type i=0; i<pd.size(); i++) {
    if (domLoX > x[3*i]) domLoX = x[3*i];
    if (domLoY > x[3*i+1]) domLoY = x[3*i+1];
    if (domLoZ > x[3*i+2]) domLoZ = x[3*i+2];
    if (domHiX < x[3*i]) domHiX = x[3*i];
    if (domHiY < x[3*i+1]) domHiY = x[3*i+1];
    if (domHiZ < x[3*i+2]) domHiZ = x[3*i+2];
  }

  // Now do sources
#ifdef _OPENMP
#pragma omp parallel for reduction(min:domLoX,domLoY,domLoZ) reduction(max:domHiX,domHiY,domHiZ)
#endif
  for (vector<Real>::size_type i=0; i<src.size(); i++) {
    if (domLoX > xSrc[3*i]) domLoX = xSrc[3*i];
    if (domLoY > xSrc[3*i+1]) domLoY = xSrc[3*i+1];
    if (domLoZ > xSrc[3*i+2]) domLoZ = xSrc[3*i+2];
    if (domHiX < xSrc[3*i]) domHiX = xSrc[3*i];
    if (domHiY < xSrc[3*i+1]) domHiY = xSrc[3*i+1];
    if (domHiZ < xSrc[3*i+2]) domHiZ = xSrc[3*i+2];
  }

  // In an MPI calculation, synchronize this information across all
  // ranks
#ifdef ENABLE_MPI
  MPI_Allreduce(MPI_IN_PLACE, domLo, 3, MPI_RL, MPI_MIN,
		MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE, domHi, 3, MPI_RL, MPI_MAX,
		MPI_COMM_WORLD);
#endif

  // Compute domain length and factor
  domLen = 0.0;
  for (int j=0; j<3; j++) {
    if (domLen < domHi[j] - domLo[j]) domLen = domHi[j] - domLo[j];
  }
  domLen *= 1.0001;  // Safety margin
  domFac = 1.0 / domLen * (((Peano::PKey) 1) << Peano::bitsPerDim);

  // Finally, synchronize total number of packets and sources, the
  // number of packets and sources in the buffer, total cost of all
  // packets, and total statistical weight of all sources
  totPacket = pd.size();
  totDeletedPacket = pddel.size();
  totSrc = src.size();
  localCost = 0.0;
  totSrcWgt = 0.0;
#ifdef _OPENMP
#pragma omp parallel for reduction(+:localCost)
#endif
  for (IdxType i = 0; i<pd.size(); i++) localCost += cost[i];
#ifdef _OPENMP
#pragma omp parallel for reduction(+:totSrcWgt)
#endif
  for (IdxType i = 0; i<src.size(); i++)
    totSrcWgt += CRSrc::srcWgt(src[i], par.qSamp);
#ifdef ENABLE_MPI
  MPI_Allreduce(MPI_IN_PLACE, &totPacket, 1, MPI_SZ, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE, &totDeletedPacket, 1, MPI_SZ, MPI_SUM,
		MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE, &totSrc, 1, MPI_SZ, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(&localCost, &totCost, 1, MPI_RL, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(&totSrcWgt, &totSrcWgt, 1, MPI_RL, MPI_SUM, MPI_COMM_WORLD);
#else
  totCost = localCost;
#endif

  // Recompute count and cost limits
  countLimit = totPacket / (MPIUtil::nRank * controls::minDivPerRank);
  costLimit = totCost / (MPIUtil::nRank * controls::minDivPerRank);
}

/////////////////////////////////////////////////////////////////////
// Routine to rebuild the tree
/////////////////////////////////////////////////////////////////////
void CRTree::rebuild() {

  // Step 1: merge the buffers of incoming packets and sources into
  // the main data holders, number the newly-created objects, and clear the
  // incoming buffers.
  copy(xbuf.begin(), xbuf.end(), back_inserter(x));
  copy(pdbuf.begin(), pdbuf.end(), back_inserter(pd));
  copy(costbuf.begin(), costbuf.end(), back_inserter(cost));
  if (fQ::nFieldQty > 0) {
    // Here we re-size rather than copying, because the data will need
    // to be recomputed after tree rebuild anyway
    qty.resize(qty.size() + qtybuf.size());
    h.resize(h.size() + hbuf.size());
  }
  copy(xSrcbuf.begin(), xSrcbuf.end(), back_inserter(xSrc));
  copy(srcbuf.begin(), srcbuf.end(), back_inserter(src));
  xbuf.clear();
  pdbuf.clear();
  costbuf.clear();
  qtybuf.clear();
  hbuf.clear();
  xSrcbuf.clear();
  srcbuf.clear();

  // Step 2: get the size of the bounding box on all packets and
  // sources across all MPI ranks
  setupDomain();

  // Step 3: compute Peano keys for all packets
  setPeanoKeys();

  // Step 4: sort the packets by their Peano keys
  sortByPKey(peanoKeys, x, pd, cost);
  
  // Step 5: initialize the top-level (global) tree to just a single
  // root node
  topNodes.resize(1);
  topNodes[0].size = PeanoCells;
  topNodes[0].start = 0;
  topNodes[0].count = peanoKeys.size();
  topNodes[0].cost = localCost;
  topNodes[0].child = -1;
  topNodes[0].parent = -1;
  topNodes[0].first = 0;

  // Step 6: recursively build the remainder of the top-level tree on
  // the local MPI rank; this top level tree will contain only
  // packets that are stored locally on this MPI rank for now
  buildTopTreeLocal(0);

  // Step 7: synchronize the top tree across all MPI ranks, so that
  // all ranks share the same set of topNodes
#ifdef ENABLE_MPI
  syncTopTrees();
#endif
  
  // Step 8: now that trees are synchronized, count the number of
  // leaves and assign them a sequential number
  numberTopTreeLeaves(rootNode);

  // Step 9: record the cost and packet count in each leaf, and
  // synchronize this information across ranks
  sumCostTopTreeLeaves();

#ifdef ENABLE_MPI
  // Step 10: calculate the load balance
  findLoadBalance();

  // Step 11: exchange packets and sources across ranks following the
  // calculated load balance
  exchangePacketsAndSources();
#endif

  // Step 12: now rebuild the local tree on each rank
  rebuildLocal();
}


/////////////////////////////////////////////////////////////////////
// Routine to build the top-level tree based on local information
/////////////////////////////////////////////////////////////////////
void CRTree::buildTopTreeLocal(const IdxType i) {

  // First check if we need to divide this node because it does not
  // adequately subdivide its parent node
  bool flag = false;
  if (topNodes[i].parent != nullNode) {
    IdxType par = topNodes[i].parent;
    if (topNodes[i].count > controls::maxOpenFac * topNodes[par].count ||
	topNodes[i].cost > controls::maxOpenFac * topNodes[par].cost)
      flag = true;
  }

  // Check if we need to subdivide this node because it has too many
  // packets, or because it has already been flagged; however, we
  // cannot subdivide a node with fewer than 8 sub-cells in it
  if ((topNodes[i].count > countLimit ||
       topNodes[i].cost > costLimit ||
       flag) &&
      topNodes[i].size >= 8) {
    
    // Expand the topNodes list to accommodate the new children we are
    // adding, and point to first of them
    topNodes[i].child = topNodes.size();
    topNodes.resize(topNodes.size() + 8);

    // Initialize child nodes
    for (int n=0; n<8; n++) {
      IdxType c = topNodes[i].child + n;
      topNodes[c].parent = i;
      topNodes[c].child = nullNode;
      topNodes[c].size = topNodes[i].size >> 3;
      topNodes[c].start = topNodes[i].start + n * topNodes[c].size;
      topNodes[c].first = topNodes[i].first;
      topNodes[c].count = 0;
      topNodes[c].cost = 0.0;
    }

    // Now sort packets onto child nodes
    IdxType c = topNodes[i].child;
    {
      int n = 0;
      for (IdxType p = topNodes[i].first;
	   p < topNodes[i].first + topNodes[i].count;
	   p++) {

	// Check if we are done with this octant, and should move to the
	// next
	if (n < 7) {
	  while (peanoKeys[p] >= topNodes[c+1].start) {
	    n++;
	    c++;
	    topNodes[c].first = p;
	    if (n >= 7) break;
	  }
	}
      
	// Increment running tallies for this node
	topNodes[c].count++;
	topNodes[c].cost += cost[p];
      }
    }
    
    // Last step: call this routine recursively on all our children
    for (int n=0; n<8; n++) {
      IdxType c = topNodes[i].child + n;
      buildTopTreeLocal(c);
    }
  }

  // Print status if sufficiently verbose
  if (i == rootNode && par.verbosity > 1) {
    for (int j=0; j<MPIUtil::nRank; j++) {
      if (j == MPIUtil::myRank) {
	cout << "CRTree::rebuild: "
	     << "rank " << j << " completed local topTree construction: "
	     << pd.size() << " local packets, "
	     << topNodes.size() << " local nodes"
	     << endl;
      }
      MPIUtil::barrier();
    }
  }
}

/////////////////////////////////////////////////////////////////////
// Routine to merge one tree of topNodes into another
/////////////////////////////////////////////////////////////////////
void CRTree::insertTopNodeTree(const vector<topNode> &insertTree,
				     const IdxType no,
				     const IdxType noInsert) {
  
  // Procedure depends on size of tree to be inserted compared to
  // local tree size
  if (insertTree[noInsert].size < topNodes[no].size) {

    // The tree we are inserting contains fewer Peano-Hilbert cells
    // than the current level of the tree into which we are inserting;
    // we therefore need to descend the tree into which we are inserting

    // If this node has no children, create some into which we can
    // descend
    if (topNodes[no].child == nullNode) {

      // Recompute counts for insertion
      IdxType count = topNodes[no].count -
	insertTree[insertTree[noInsert].parent].count;
      IdxType countInsert = count / 8;
      IdxType countLoc = count - 7 * countInsert;

      // Recompute costs
      Real cost = topNodes[no].cost -
	insertTree[insertTree[noInsert].parent].cost;
      Real costInsert = cost / 8;
      Real costLoc = cost - 7 * costInsert;

      // Add pointer to new child node
      topNodes[no].child = topNodes.size();

      // Make room for the new nodes
      topNodes.resize(topNodes.size() + 8);

      // Add new nodes
      for (int j=0; j<8; j++) {
	if (j == 0) {
	  count = countLoc;
	  cost = costLoc;
	} else {
	  count = countInsert;
	  cost = costInsert;
	}
	IdxType n = topNodes[no].child + j;
	topNodes[n].size = (topNodes[no].size >> 3);
	topNodes[n].count = count;
	topNodes[n].cost = cost;
	topNodes[n].child = nullNode;
	topNodes[n].parent = no;
	topNodes[n].start = topNodes[no].start + j * topNodes[n].size;
      }
    }

    // Recursively call this routine to insert into one of the child
    // nodes
    IdxType ch = topNodes[no].child +
      (insertTree[noInsert].start - topNodes[no].start) /
      (topNodes[no].size >> 3);
    insertTopNodeTree(insertTree, ch, noInsert);
    
  } else {

    // If we are here, then we have found the right level in the local
    // topNode tree into which to insert the tree we're adding

    // Add packet count and cost to local node
    topNodes[no].count += insertTree[noInsert].count;
    topNodes[no].cost += insertTree[noInsert].cost;

    // Check if the tree we are inserting has children at this node
    if (insertTree[noInsert].child != nullNode) {
      
      // If the tree we are inserting has children, we now need to call
      // this routine again to insert the children recursively
      for (int j=0; j<8; j++) {
	IdxType ch = insertTree[noInsert].child + j;
	insertTopNodeTree(insertTree, no, ch);
      }
      
    } else {

      // If the tree we are inserting does not have children, but the
      // local node does, we have to update the packet count and
      // cost for those children
      if (topNodes[no].child != nullNode)
	updateChildCountsCosts(no, insertTree[noInsert].count,
			       insertTree[noInsert].cost);
    }
  }
}

/////////////////////////////////////////////////////////////////////
// Method to update the packet counts and costs of child nodes
// during tree insertion
/////////////////////////////////////////////////////////////////////
void CRTree::updateChildCountsCosts(const IdxType no,
					  const IdxType count,
					  const Real cost) {
  
  // Compute updated counts and costs
  IdxType countInsert = count / 8;
  IdxType countLoc = count - 7 * countInsert;
  Real newCost = cost / 8;

  // Update children
  for (int j=0; j<8; j++) {
    IdxType n = topNodes[no].child + j;
    IdxType newCount;
    if (j == 0)
      newCount = countLoc;
    else
      newCount = countInsert;
    topNodes[n].count += newCount;
    topNodes[n].cost += newCost;

    // Recursively call on children
    if (topNodes[n].child != nullNode)
      updateChildCountsCosts(n, newCount, newCost);
  }
}

  
/////////////////////////////////////////////////////////////////////
// Routine to synchronize top-level tree information across all MPI
// ranks
/////////////////////////////////////////////////////////////////////
#ifdef ENABLE_MPI

void CRTree::syncTopTrees() {

  // Algorithm if number of ranks is a power of 2
  if (MPIUtil::nRank == (1 << MPIUtil::log2nRank)) {

    // Loop over permutations of tasks, doing pairwise exchanges
    for (int nGroup = 1; nGroup < (1 << MPIUtil::log2nRank); nGroup <<= 1) {

      // Get send and receive rank for this group
      int sender = MPIUtil::myRank;
      int receiver = sender ^ nGroup;  // Receiver = xor of sender and
				       // group #

      // Sender and receiver exchange information on the number of
      // nodes in their local trees
      vector<topNode>::size_type nNodeLocal = topNodes.size();
      vector<topNode>::size_type nNodeRemote;
      MPI_Status status;
      MPI_Sendrecv(&nNodeLocal, 1, MPI_SZ, receiver,
		   MPIUtil::tagPeanoTopNodeCount,
		   &nNodeRemote, 1, MPI_SZ, receiver,
		   MPIUtil::tagPeanoTopNodeCount,
		   MPI_COMM_WORLD, &status);
      
      // Now exchange the nodes themselves
      vector<topNode> remoteTopNodes(nNodeRemote);
      MPI_Sendrecv(topNodes.data(), nNodeLocal, MPIUtil::topNodeType,
		   receiver, MPIUtil::tagPeanoTopNodeExchange,
		   remoteTopNodes.data(), nNodeRemote, MPIUtil::topNodeType,
		   receiver, MPIUtil::tagPeanoTopNodeExchange,
		   MPI_COMM_WORLD, &status);

      // If the sender's rank is > receiver's rank, exchange the local
      // topNode list with the remote one; this is to ensure that,
      // when the lists are merged, we get the same result on every
      // rank, since the merging operation is not commutative
      if (sender > receiver) swap(topNodes, remoteTopNodes);

      // Merge the remote top nodes into the local tree
      insertTopNodeTree(remoteTopNodes, rootNode, rootNode);
    }

  } else {

    // If the number of MPI ranks is a not a power of 2, we use a
    // different algorithm to construct the merged trees
    recursiveSyncTopTrees(0, MPIUtil::nRank);

  }

  // Now that trees have been synchronized across all MPI ranks, check
  // whether we need to further subdivide any nodes
  for (IdxType i=0; i<topNodes.size(); i++) {

    // Skip nodes that already have children
    if (topNodes[i].child != nullNode) continue;

    // If this node has no children, check if it is above count or
    // cost limit, and has size > 1; if so subdivide it
    if ((topNodes[i].count > countLimit ||
	 topNodes[i].cost > costLimit) &&
	topNodes[i].size > 1) {

      // Point to new child
      topNodes[i].child = topNodes.size();
	
      // Make room for new nodes
      topNodes.resize(topNodes.size() + 8);

      // Initialize new nodes
      for (int j=0; j<8; j++) {
	IdxType ch = topNodes[i].child + j;
	topNodes[ch].size = (topNodes[i].size >> 3);
	topNodes[ch].count = topNodes[i].count / 8;
	topNodes[ch].cost = topNodes[i].cost / 8;
	topNodes[ch].child = nullNode;
	topNodes[ch].parent = i;
	topNodes[ch].start = topNodes[i].start + j * topNodes[ch].size;
      }
    }
  }

  // Print status if verbose enough
  if (MPIUtil::IOProc && par.verbosity > 1) {
    cout << "CRTree::rebuild: "
	 << "synchronized topTree across MPI ranks: "
	 << totPacket << " packets, "
	 << topNodes.size() << " nodes"
	 << endl;
  }
}

#endif

/////////////////////////////////////////////////////////////////////
// Routine to recursively synchronize top-level tree information
// across all MPI ranks; used by syncTopTrees when the number of ranks
// is not a power of 2
/////////////////////////////////////////////////////////////////////
#ifdef ENABLE_MPI
void CRTree::recursiveSyncTopTrees(int startRank, int n) {

  // Partition this work group
  int nLeft = n / 2;
  int nRight = n - nLeft;

  // Call recursively if there are > 2 ranks in this group
  if (n > 2) {
    recursiveSyncTopTrees(startRank, nLeft);
    recursiveSyncTopTrees(startRank + nLeft, nRight);
  }

  // If there are two or more ranks in this call, exchange data
  // between the left and right ones
  if (n >= 2) {

    // Storage
    vector<topNode> remoteTopNodes;
    vector<topNode>::size_type nNodeRemote;

    // First startRank and the middle rank exchange information
    if (MPIUtil::myRank == startRank ||
	MPIUtil::myRank == startRank + nLeft) {

      // Figure out who is receiving
      int receiver;
      if (MPIUtil::myRank == startRank) {
	receiver = startRank + nLeft;
      } else {
	receiver = startRank;
      }

      // Sender and receiver exchange information on the number of
      // nodes in their local trees
      vector<topNode>::size_type nNodeLocal = topNodes.size();
      MPI_Status status;
      MPI_Sendrecv(&nNodeLocal, 1, MPI_SZ, receiver,
		   MPIUtil::tagPeanoTopNodeCount,
		   &nNodeRemote, 1, MPI_SZ, receiver,
		   MPIUtil::tagPeanoTopNodeCount,
		   MPI_COMM_WORLD, &status);

      // Now exchange the nodes themselves
      remoteTopNodes.resize(nNodeRemote);
      MPI_Sendrecv(topNodes.data(), nNodeLocal, MPIUtil::topNodeType,
		   receiver, MPIUtil::tagPeanoTopNodeExchange,
		   remoteTopNodes.data(), nNodeRemote, MPIUtil::topNodeType,
		   receiver, MPIUtil::tagPeanoTopNodeExchange,
		   MPI_COMM_WORLD, &status);
		   
    }

    // Now startRank sends the tree it received to other ranks within
    // the left half of the group
    if (MPIUtil::myRank == startRank) {
      for (int receiver = startRank + 1; receiver < startRank + nLeft;
	   receiver++) {
	// Send number of nodes
	MPI_Send(&nNodeRemote, 1, MPI_SZ, receiver,
		 MPIUtil::tagPeanoTopNodeCount, MPI_COMM_WORLD);
	// Send node data
	MPI_Send(topNodes.data(), nNodeRemote, MPIUtil::topNodeType,
		 receiver, MPIUtil::tagPeanoTopNodeExchange,
		 MPI_COMM_WORLD);
      }
    }

    // Middle rank sends the data it imported to all other ranks within
    // the right half of the group
    if (MPIUtil::myRank == startRank + nLeft) {
      for (int receiver = startRank + nLeft + 1;
	   receiver < startRank + nLeft + nRight;
	   receiver++) {
	// Send number of nodes
	MPI_Send(&nNodeRemote, 1, MPI_SZ, receiver,
		 MPIUtil::tagPeanoTopNodeCount, MPI_COMM_WORLD);
	// Send node data
	MPI_Send(topNodes.data(), nNodeRemote, MPIUtil::topNodeType,
		 receiver, MPIUtil::tagPeanoTopNodeExchange,
		 MPI_COMM_WORLD);
      }
    }

    // Ranks in the left half of the group, but not the leftmost,
    // receive
    if (MPIUtil::myRank > startRank &&
	MPIUtil::myRank < startRank + nLeft) {
      // Receive number of nodes
      MPI_Recv(&nNodeRemote, 1, MPI_SZ, startRank,
	       MPIUtil::tagPeanoTopNodeCount, MPI_COMM_WORLD,
	       MPI_STATUS_IGNORE);
      // Receive node data
      remoteTopNodes.resize(nNodeRemote);
      MPI_Recv(remoteTopNodes.data(), nNodeRemote, MPIUtil::topNodeType,
	       startRank, MPIUtil::tagPeanoTopNodeExchange,
	       MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }

    // Ranks in the right half of the group, but not the middle rank,
    // receive
    if (MPIUtil::myRank > startRank + nLeft &&
	MPIUtil::myRank < startRank + nLeft + nRight) {
      // Receive number of nodes
      MPI_Recv(&nNodeRemote, 1, MPI_SZ, startRank + nLeft,
	       MPIUtil::tagPeanoTopNodeCount, MPI_COMM_WORLD,
	       MPI_STATUS_IGNORE);
      // Receive node data
      remoteTopNodes.resize(nNodeRemote);
      MPI_Recv(remoteTopNodes.data(), nNodeRemote, MPIUtil::topNodeType,
	       startRank + nLeft, MPIUtil::tagPeanoTopNodeExchange,
	       MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }

    // Swap local and remote trees so that they are ordered with lower
    // rank as remote and higher rank as local on all ranks -- this
    // ensures that, when the trees are merged, the results are
    // identical on all ranks
    if (MPIUtil::myRank >= startRank &&
	MPIUtil::myRank < startRank + nLeft)
      swap(topNodes, remoteTopNodes);

    // Insert imported nodes into local tree
    if (MPIUtil::myRank >= startRank &&
	MPIUtil::myRank < startRank + n) {
      insertTopNodeTree(remoteTopNodes, rootNode, rootNode);
    }
  }
}

#endif

/////////////////////////////////////////////////////////////////////
// Routine to count the number of leaves in the topNodes tree, and to
// assign each leave a sequential number
/////////////////////////////////////////////////////////////////////
void CRTree::numberTopTreeLeaves(const IdxType no) {

  // If starting from root, zero out the leaf count
  if (no == rootNode) nTopLeaves = 0;

  // Check if this node is a leaf
  if (topNodes[no].child == nullNode) {

    // Yes, this is a leaf; assign it a number, then increment count
    topNodes[no].leaf = nTopLeaves;
    nTopLeaves++;

  } else {

    // No, not a leaf, so check this nodes children
    for (int i=0; i<8; i++)
      numberTopTreeLeaves(topNodes[no].child + i);

  }

  // Print status if sufficiently verbose
  if (MPIUtil::IOProc && no == rootNode && par.verbosity > 1) {
    cout << "CRTree::rebuild: "
	 << "completed topTree construction: "
	 << totPacket << " packets, "
	 << topNodes.size() << " nodes, "
	 << nTopLeaves << " leaves"
	 << endl;
  }
}

/////////////////////////////////////////////////////////////////////
// Routine to sum the costs of topTree leaves
/////////////////////////////////////////////////////////////////////
void CRTree::sumCostTopTreeLeaves() {

#ifdef _OPENMP
#pragma omp parallel
#endif
  {
#ifdef _OPENMP
    // Get thread information
    const int nthread = omp_get_num_threads();
    const IdxType off = nTopLeaves * omp_get_thread_num();
#else
    const int nthread = 1;
    const IdxType off = 0;
#endif

    // Initialize cost and count holders; in openMP mode, we need
    // nthread times the storage, so we can sum in parallel and then
    // reduce across threads
#ifdef _OPENMP
#pragma omp single
#endif
    {
      topLeafCosts.assign(nTopLeaves * nthread, 0.0);
      topLeafCounts.assign(nTopLeaves * nthread, 0);
    }

    // Loop over packets on the local process in parallel
#ifdef _OPENMP
#pragma omp for
#endif
    for (IdxType i=0; i<pd.size(); i++) {

      // Increment the leaf cost and count on the correct thread
      IdxType no = getTopTreeLeaf(i);      
      topLeafCosts[off + topNodes[no].leaf] += cost[i];
      topLeafCounts[off + topNodes[no].leaf]++;
    }

#ifdef _OPENMP
#pragma omp for
    // Now reduce across threads
    for (IdxType n=0; n<nTopLeaves; n++) {
      for (int i=1; i<nthread; i++) {
	topLeafCosts[n] += topLeafCosts[n + i*nTopLeaves];
	topLeafCounts[n] += topLeafCounts[n + i*nTopLeaves];
      }
    }

    // Delete extra storage from the arrays
#pragma omp single
    {
      topLeafCosts.resize(nTopLeaves);
      topLeafCounts.resize(nTopLeaves);
    }
#endif
  }

#ifdef ENABLE_MPI
  // Now reduce across MPI ranks
  MPI_Allreduce(MPI_IN_PLACE, topLeafCosts.data(), nTopLeaves,
		MPI_RL, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE, topLeafCounts.data(), nTopLeaves,
		MPI_SZ, MPI_SUM, MPI_COMM_WORLD);
#endif
}


/////////////////////////////////////////////////////////////////////
// Routine to do load balancing across MPI ranks. Note that this
// routine doesn't actually pass the data; it just computes how the
// tree should be broken up across ranks to achieve load balance.
/////////////////////////////////////////////////////////////////////
#ifdef ENABLE_MPI
void CRTree::findLoadBalance() {

  // Initialize array describing the load balance
  rankStartLeaf.assign(MPIUtil::nRank+1, 0);
  leafAssignments.assign(nTopLeaves, 0);

  // Loop over ranks
  IdxType ptr = 1;
  Real cumCost = topLeafCosts[0];
  for (int i=0; i<MPIUtil::nRank-1; i++) {

    // Target cumulative cost for this rank
    Real targetCost = (i+1) * totCost / MPIUtil::nRank;

    // Add leaves to this rank until we are straddling the target cost
    while (cumCost + topLeafCosts[ptr] < targetCost &&
	   nTopLeaves - ptr > (IdxType) (MPIUtil::nRank - i)) {
      leafAssignments[ptr] = i;
      cumCost += topLeafCosts[ptr];
      ptr++;
    }

    // Now add or retain the last leaf based on which brings us closer
    // to the target
    if (cumCost + topLeafCosts[ptr] - targetCost <
	targetCost - cumCost) {
      leafAssignments[ptr] = i;
      cumCost += topLeafCosts[ptr];
      ptr++;
    }

    // Record the start of the next leaf
    ptr++;
    cumCost += topLeafCosts[ptr];
    rankStartLeaf[i+1] = ptr;
    leafAssignments[ptr] = i+1;
  }
  rankStartLeaf[MPIUtil::nRank] = nTopLeaves;
  for (IdxType n=ptr+1; n<nTopLeaves; n++)
    leafAssignments[n] = MPIUtil::nRank-1;

  // Print status if verbose
  if (MPIUtil::IOProc && par.verbosity > 1) {
    for (int i=0; i<MPIUtil::nRank; i++) {
      cout << "CRTree::rebuild: computed load balance "
	   << "rank " << i << ": "
	 << rankStartLeaf[i+1] - rankStartLeaf[i] << " leaves"
	 << endl;
    }
  }
}
#endif

/////////////////////////////////////////////////////////////////////
// Routine to exchange packets and sources across MPI ranks
/////////////////////////////////////////////////////////////////////
#ifdef ENABLE_MPI
void CRTree::exchangePacketsAndSources() {

  // The overall strategy here is as follows:
  // (1) Each MPI rank goes through its topTree and figures out which
  //     of the packets and sources that it owns live in leaves
  //     belonging to other ranks, and packs those into a send buffer.
  // (2) Ranks exchange the sizes of the buffers they have to send to
  //     each other with an all-to-all.
  // (3) Each rank allocates a buffer to hold incoming data.
  // (4) Data are exchanged between processors using non-blocking
  //     sends and receives.

  // Step 1a: each rank moves the packets and sources it is exporting
  // into buffers; note that this is done in three passes; the first
  // just counts and figures out where the packets and soruces should
  // go, the second moves the data into the export buffers, and the
  // third deletes the data off the local list
  vector<IdxType> exportCount;
  vector<int> destination(peanoKeys.size()),
    srcDestination(srcPeanoKeys.size());
#ifdef _OPENMP
#pragma omp parallel
#endif
  {
#ifdef _OPENMP
    const int nthread = omp_get_num_threads();
    const IdxType off = 2 * MPIUtil::nRank * omp_get_thread_num();
#else
    const int nthread = 1;
    const IdxType off = 0;
#endif
#ifdef _OPENMP
#pragma omp single
#endif
    {
      exportCount.assign(2 * MPIUtil::nRank * nthread, 0);
    }

    // Loop over packets; for each packet, figure out which rank
    // it goes on, and increment the counter if necessary
#ifdef _OPENMP
#pragma omp for
#endif
    for (IdxType n=0; n<peanoKeys.size(); n++) {
      IdxType no = getTopTreeLeaf(n);
      IdxType leafNum = topNodes[no].leaf;
      destination[n] = leafAssignments[leafNum];
      if (destination[n] != MPIUtil::myRank)
	exportCount[off + destination[n]]++;
    }

    // Repeat for sources
#ifdef _OPENMP
#pragma omp for
#endif
    for (IdxType n=0; n<srcPeanoKeys.size(); n++) {
      IdxType no = getTopTreeLeafSrc(n);
      IdxType leafNum = topNodes[no].leaf;
      srcDestination[n] = leafAssignments[leafNum];
      if (srcDestination[n] != MPIUtil::myRank)
	exportCount[off + srcDestination[n] + 1]++;
    }

#ifdef _OPENMP
    // Reduce across threads
#pragma omp for
    for (int i=0; i<MPIUtil::nRank; i++) {
      for (int j=0; j<nthread; j++) {
	exportCount[2*i] += exportCount[2*i + j*nthread];
	exportCount[2*i+1] += exportCount[2*i+1 + j*nthread];
      }
    }
#pragma omp single
    exportCount.resize(2*MPIUtil::nRank);
#endif
  }

  // Step 1b: now that we have the correct count; now actually move
  // the data into export buffers; note that we need to pass three
  // distinct pieces of data for each packet -- its position,
  // the CRPacket data, and its Peano key -- and two for each
  // source -- its position and CRSource data. We do not bother to
  // pass the cost, because that will be recomputed anyway, and we do
  // not pass the Peano keys for the sources, since they will not be
  // placed in the tree and thus their keys will not be used. To
  // maximize efficiency we pack all this data into a single buffer
  // and send it in a single message.
  vector<vector<unsigned char> > exportBuffers(MPIUtil::nRank);
  IdxType pElementSize = 3*sizeof(Real) + sizeof(CRPacket) + sizeof(PKey);
  IdxType sElementSize = 3*sizeof(Real) + sizeof(CRSource);
#ifdef _OPENMP
#pragma omp parallel for
#endif
  for (int i=0; i<MPIUtil::nRank; i++) {

    // Skip own rank
    if (i == MPIUtil::myRank) continue;

    // Resize the buffer to the correct size; and pointers to the
    // starts of the blocks for each type of data
    exportBuffers[i].resize( exportCount[2*i] * pElementSize +
			     exportCount[2*i+1] * sElementSize );
    unsigned char* xBuf = exportBuffers[2*i].data();
    unsigned char* pdBuf = xBuf + exportCount[2*i] * 3 * sizeof(Real);
    unsigned char* peanoKeyBuf = pdBuf + exportCount[2*i] * sizeof(CRPacket);
    unsigned char* xSrcBuf = peanoKeyBuf + exportCount[2*i] * sizeof(PKey);
    unsigned char* srcBuf = xSrcBuf + exportCount[2*i+1] * 3 * sizeof(Real);
    
    // Move data into buffer
    for (IdxType n=0; n<pd.size(); n++) {
      if (destination[n] == i) {
	memcpy(xBuf, &x[3*n], 3*sizeof(Real));
	xBuf += 3*sizeof(Real);
	memcpy(pdBuf, &pd[n], sizeof(CRPacket));
	pdBuf += sizeof(CRPacket);
	memcpy(peanoKeyBuf, &peanoKeys[n], sizeof(PKey));
	peanoKeyBuf += sizeof(PKey);
      }
    }
    for (IdxType n=0; n<src.size(); n++) {
      if (srcDestination[n] == i) {    
	memcpy(xSrcBuf, &xSrc[3*n], 3*sizeof(Real));
	xSrcBuf += 3*sizeof(Real);
	memcpy(srcBuf, &src[n], sizeof(CRSource));
	srcBuf += sizeof(CRSource);
      }
    }
  }

  // Step 1c: delete the packets and sources that are being exported;
  // note that this is always down serially, to avoid race conditions
  IdxType ptrNew = 0, ptrOld = 0;
  while (ptrOld < pd.size()) {
    while (destination[ptrOld] == MPIUtil::myRank &&
	   ptrOld < pd.size()) {
      for (int i=0; i<3; i++) x[3*ptrNew+i] = x[3*ptrOld+i];
      pd[ptrNew] = pd[ptrOld];
      peanoKeys[ptrNew] = peanoKeys[ptrOld];
      ptrNew++;
      ptrOld++;
    }
    while (destination[ptrOld] != MPIUtil::myRank &&
	   ptrOld < pd.size()) ptrOld++;
  }
  x.resize(3*ptrNew);
  pd.resize(ptrNew);
  if (fQ::nFieldQty > 0) h.resize(ptrNew)
  peanoKeys.resize(ptrNew);
  IdxType nExp = ptrOld - ptrNew;
  ptrNew = 0;
  ptrOld = 0;
  while (ptrOld < src.size()) {
    while (srcDestination[ptrOld] == MPIUtil::myRank &&
	   ptrOld < src.size()) {
      for (int i=0; i<3; i++) xSrc[3*ptrNew+i] = xSrc[3*ptrOld+i];
      src[ptrNew] = src[ptrOld];
      ptrNew++;
      ptrOld++;
    }
    while (srcDestination[ptrOld] != MPIUtil::myRank &&
	   ptrOld < src.size()) ptrOld++;
  }
  xSrc.resize(3*ptrNew);
  src.resize(ptrNew);

  // Print status if very verbose
  if (par.verbosity > 1) {
    for (int i=0; i<MPIUtil::nRank; i++) {
      if (i == MPIUtil::myRank) {
	cout << "CRTree::rebuild: rank " << i
	     << ": deleted " << nExp
	     << " packets and "
	     << ptrOld - ptrNew
	     << " sources being exported; export counts are: ";
	for (int j=0; j<MPIUtil::nRank; j++) {
	  cout << "rank " << j << ": " << exportCount[2*j]
	       << " packets, "
	       << exportCount[2*j+1]
	       << " sources; ";
	}
	cout << endl;
      }
      MPIUtil::barrier();
    }
  }

  // Step 2: use an all-to-all so that every processor tells every
  // other processor how many packets and sources it is sending
  vector<IdxType> importCount(2*MPIUtil::nRank);
  MPI_Alltoall(exportCount.data(), 2, MPI_SZ,
	       importCount.data(), 2, MPI_SZ, MPI_COMM_WORLD);

  
  // Step 3: allocate space in the data array to hold the packets we
  // are going to get, and figure out where in each of those arrays
  // the data should be placed

  // Make space in local data arrays for new packets we're getting
  IdxType nPacketLoc = pd.size();
  IdxType nSrcLoc = src.size();
  IdxType totImportCount = 0, totSrcImportCount = 0;
  for (int i=0; i<MPIUtil::nRank; i++) {
    totImportCount += importCount[2*i];
    totSrcImportCount += importCount[2*i+1];
  }
  x.resize(3*nPacketLoc + 3*totImportCount);
  pd.resize(nPacketLoc + totImportCount);
  if (fQ::nFieldQty > 0) h.resize(nPacketLoc + totImportCount);
  peanoKeys.resize(nPacketLoc + totImportCount);
  xSrc.resize(3*nSrcLoc + 3*totSrcImportCount);
  src.resize(nSrcLoc + totSrcImportCount);

  // For each MPI rank from which we will be receiving, make a pointer
  // to the location in our own data arrays into which the data from
  // that rank should be placed. We put data from the first rank we're
  // receiving from at the end of the current data, data from the next
  // rank we're receiving from after that, and so on. Note that we do
  // not transfer field quantities or smoothing lengths, because those
  // will need to be recomputed after the tree rebuild anyway.
  vector<unsigned char *> xRecvPtr(MPIUtil::nRank);
  vector<unsigned char *> pdRecvPtr(MPIUtil::nRank);
  vector<unsigned char *> peanoKeyRecvPtr(MPIUtil::nRank);
  vector<unsigned char *> xSrcRecvPtr(MPIUtil::nRank);
  vector<unsigned char *> srcRecvPtr(MPIUtil::nRank);
  xRecvPtr[0] = reinterpret_cast<unsigned char *>(x.data()) +
    3 * nPacketLoc * sizeof(Real);
  pdRecvPtr[0] = reinterpret_cast<unsigned char *>(pd.data()) +
    nPacketLoc * sizeof(CRPacket);
  peanoKeyRecvPtr[0] = reinterpret_cast<unsigned char *>(peanoKeys.data()) +
    nPacketLoc * sizeof(PKey);
  xSrcRecvPtr[0] = reinterpret_cast<unsigned char *>(xSrc.data()) +
    3 * nSrcLoc * sizeof(Real);
  srcRecvPtr[0] = reinterpret_cast<unsigned char *>(src.data()) +
    nSrcLoc * sizeof(CRSource);
  for (int i=1; i<MPIUtil::nRank; i++) {
    xRecvPtr[i] = xRecvPtr[i-1] + 3 * importCount[2*(i-1)] * sizeof(Real);
    pdRecvPtr[i] = pdRecvPtr[i-1] + importCount[2*(i-1)] * sizeof(CRPacket);
    peanoKeyRecvPtr[i] = peanoKeyRecvPtr[i-1] + importCount[2*(i-1)]
      * sizeof(PKey);
    xSrcRecvPtr[i] = xSrcRecvPtr[i-1] + 3 * importCount[2*(i-1)+1] *
      sizeof(Real);
    srcRecvPtr[i] = srcRecvPtr[i-1] + importCount[2*(i-1)+1] *
      sizeof(CRSource);
  }

  // Step 4a: post send requests for all data types to all other
  // MPI ranks
  vector<MPI_Request> requests;
  for (int i=0; i<MPIUtil::nRank; i++) {
    unsigned char *xBuf = exportBuffers[2*i].data();
    unsigned char *pdBuf = xBuf + exportCount[2*i] * 3 * sizeof(Real);
    unsigned char *peanoKeyBuf = pdBuf + exportCount[2*i] * sizeof(CRPacket);
    unsigned char *xSrcBuf = peanoKeyBuf + exportCount[2*i] * sizeof(PKey);
    unsigned char *srcBuf = xSrcBuf + exportCount[2*i+1] * 3 * sizeof(Real);
    if (exportCount[2*i] > 0) {   // Skip if no data to send
      MPI_Request req;
      MPI_Isend(xBuf, 3*exportCount[2*i]*sizeof(Real), MPI_BYTE, i,
		MPIUtil::tagCRPacketPos, MPI_COMM_WORLD, &req);
      requests.push_back(req);
      MPI_Isend(pdBuf, exportCount[2*i]*sizeof(CRPacket), MPI_BYTE, i,
		MPIUtil::tagCRPacketData, MPI_COMM_WORLD, &req);
      requests.push_back(req);
      MPI_Isend(peanoKeyBuf, exportCount[2*i]*sizeof(PKey), MPI_BYTE, i,
		MPIUtil::tagCRPacketKeys, MPI_COMM_WORLD, &req);
      requests.push_back(req);
    }
    if (exportCount[2*i+1] > 0) {   // Skip if no data to send
      MPI_Request req;
      MPI_Isend(xSrcBuf, 3*exportCount[2*i+1]*sizeof(Real), MPI_BYTE, i,
		MPIUtil::tagCRSrcPos, MPI_COMM_WORLD, &req);
      requests.push_back(req);
      MPI_Isend(srcBuf, exportCount[2*i+1]*sizeof(CRSource), MPI_BYTE, i,
		MPIUtil::tagCRSrcData, MPI_COMM_WORLD, &req);
      requests.push_back(req);
    }
  }

  // Step 4b: post receive requests for all data types from all other
  // MPI ranks
  for (int i=0; i<MPIUtil::nRank; i++) {
    if (importCount[2*i] > 0) {   // Skip if no data to receive
      MPI_Request req;
      MPI_Irecv(xRecvPtr[i], 3*importCount[2*i]*sizeof(Real), MPI_BYTE, i,
		MPIUtil::tagCRPacketPos, MPI_COMM_WORLD, &req);
      requests.push_back(req);
      MPI_Irecv(pdRecvPtr[i], importCount[2*i]*sizeof(CRPacket), MPI_BYTE, i,
		MPIUtil::tagCRPacketData, MPI_COMM_WORLD, &req);
      requests.push_back(req);
      MPI_Irecv(peanoKeyRecvPtr[i], importCount[2*i]*sizeof(PKey), MPI_BYTE, i,
		MPIUtil::tagCRPacketKeys, MPI_COMM_WORLD, &req);
      requests.push_back(req);
    }
    if (importCount[2*i+1] > 0) {   // Skip if no data to receive
      MPI_Request req;
      MPI_Irecv(xSrcRecvPtr[i], 3*importCount[2*i+1]*sizeof(Real), MPI_BYTE, i,
		MPIUtil::tagCRSrcPos, MPI_COMM_WORLD, &req);
      requests.push_back(req);
      MPI_Irecv(srcRecvPtr[i], importCount[2*i+1]*sizeof(CRSource), MPI_BYTE, i,
		MPIUtil::tagCRSrcData, MPI_COMM_WORLD, &req);
      requests.push_back(req);
    }
  }

  // Step 4c: wait for all requests to complete
  MPI_Waitall(requests.size(), requests.data(), MPI_STATUSES_IGNORE);  

  // Print status if very verbose
  if (par.verbosity > 1) {
    for (int i=0; i<MPIUtil::nRank; i++) {
      if (i == MPIUtil::myRank) {
	cout << "CRTree::rebuild: exchange done, rank " << i
	     << " now holds " << pd.size()
	     << " packets and " << src.size()
	     << " sources"
	     << endl;
      }
      MPIUtil::barrier();
    }
  }
}

#endif


/////////////////////////////////////////////////////////////////////
// Routine to rebuild the local tree on a given MPI rank
/////////////////////////////////////////////////////////////////////
void CRTree::rebuildLocal() {

  // Step 1: clone the top tree into our local tree structure
  nodes.reserve(0.7 * (pd.size() + pdbuf.size() + src.size()));
  nodes.resize(1);
  topToLocalMap.resize(nTopLeaves);
  nodes[rootNode].len = domLen;
  for (int i=0; i<3; i++)
    nodes[rootNode].center[i] = 0.5 * (domLo[i] + domHi[i]);
  for (int i=0; i<8; i++)
    nodes[rootNode].children[i] = nullNode;
  nodes[rootNode].parent = nullNode;
  int treeLev = 1;
  int xi = 0, yi = 0, zi = 0;
  cloneTopTreeToLocalTree(addLocalFlag(rootNode), rootNode, treeLev,
			  xi, yi, zi);

  // Step 2: now compute Morton keys for all packets; within the
  // local tree, packets will be loaded in Morton order rather than
  // Peano order, in order to facilitate neighbor finding
  setMortonKeys();

  // Step 3: now add packets to local tree
  IdxType parent = nullNode;
  for (IdxType k=0; k<pd.size(); k++) {

    // First figure out which leaf of the topTree this packet goes
    // in; this will tell us which node of the local tree to start at
    int shift = 3 * (bitsPerDim - 1);
    IdxType no = rootNode;
    while (topNodes[no].child != nullNode) {
      no = topNodes[no].child + (peanoKeys[k] - topNodes[no].start) /
	(topNodes[no].size / 8);
      shift -= 3;
    }

    // Now point to the correct node in the local tree
    no = topToLocalMap[topNodes[no].leaf];

    // Now loop through local tree until we find a place to attach
    // this packet
    int subnode = -1;
    while (true) {

      // Is the node we are looking at an internal node, or are we
      // pointing at a packet?
      if (isLocalNode(no)) {

	// We're in the node tree interior, not at a single packet;
	// get index of this node in the array
	IdxType noIdx = removeLocalFlag(no);
	
	// Now determine if we're at the bottom of the tree yet
	if (shift >= 0) {
	  // Not at the bottom yet, so figure out which of the eight
	  // children to descend to
	  subnode = ((mortonKeys[k] >> shift) & 7);
	} else {
	  // We are at bottom of tree, so decide which subnode to to
	  // into based on position
	  subnode = 0;
	  if (x[3*k] > nodes[noIdx].center[0]) subnode += 1;
	  if (x[3*k+1] > nodes[noIdx].center[1]) subnode += 2;
	  if (x[3*k+2] > nodes[noIdx].center[2]) subnode += 4;
	}

	// Set subnode to random value if we have gotten to a node
	// length smaller than our tolerance -- this is to prevent
	// infinite recursion if we wind up with packets very
	// close to one another
	if (nodes[noIdx].len < 1e-4 * par.soften) {
	  subnode = (int) (8.0 * rng.uniform());
	  if (subnode >= 8) subnode = 7;
	}

	// Get pointer to next node we should descend to
	IdxType nextNode = nodes[noIdx].children[subnode];
	shift -= 3;

	// Check if the child slot we are descending to already exists
	if (nextNode != nullNode) {
	  // Yes, child slot is occupied, so point to that child node
	  // and continue loop; note that we set parent to the index
	  // in memory of this node, rather than the index plus flag
	  // value, because we will be using it as an index below. 
	  parent = noIdx;
	  no = nextNode;
	} else {
	  // Child slot is not occupied, so attach the packet here
	  nodes[noIdx].children[subnode] = k;
	  break;
	}
	
      } else {

	// If we're here, the current node we're looking at is a leaf
	// that contains a single packet. However, we just found
	// another packet that wants to be in this slot. We therefore
	// have to create a new node in the tree at this point and
	// attach the new packet to this node, before resuming trying
	// to attach this packet.

	// Make parent point to the new node we're building
	nodes[parent].children[subnode] = nodes.size() | localNodeFlag;

	// Add the new node
	nodes.resize(nodes.size() + 1);
	node &newNode = nodes.back();

	// Set geometric properties of new node
	newNode.len = 0.5 * nodes[parent].len;
	Real lenHalf = 0.25 * nodes[parent].len;
	if (subnode & 1)
	  newNode.center[0] = nodes[parent].center[0] + lenHalf;
	else
	  newNode.center[0] = nodes[parent].center[0] - lenHalf;
	if (subnode & 2)
	  newNode.center[1] = nodes[parent].center[1] + lenHalf;
	else
	  newNode.center[1] = nodes[parent].center[1] - lenHalf;
	if (subnode & 4)
	  newNode.center[2] = nodes[parent].center[2] + lenHalf;
	else
	  newNode.center[2] = nodes[parent].center[2] - lenHalf;

	// Set new node to have no children
	for (int n=0; n<8; n++) newNode.children[n] = nullNode;

	// Now we need to figure out where to put the packet that
	// was at this point in the tree; we do this based on its
	// Morton key or position, exactly we did the first time we
	// located this packet
	if (shift >= 0) {
	  PKey nodeMortonKey = mortonKeys[no];
	  subnode = ((nodeMortonKey >> shift) & 7);
	} else {
	  subnode = 0;
	  if (x[3*no] > newNode.center[0]) subnode += 1;
	  if (x[3*no+1] > newNode.center[1]) subnode += 2;
	  if (x[3*no+2] > newNode.center[2]) subnode += 4;
	}

	// Have the appropriate subnode point to the packet we have
	// displaced
	newNode.children[subnode] = no;

	// Now resume trying to attach the current packet, starting
	// from the newly-created node
	no = addLocalFlag(nodes.size()) - 1;
      }
    }
  }

  // Step 4: go through the local tree and flag nodes that represent
  // topLevel nodes whose children live on other MPI ranks; we flag
  // these by placing the value extNodeFlag | rank in their first child
  // pointer, where rank is the rank that owns that node
#ifdef ENABLE_MPI
  for (IdxType i=0; i<nTopLeaves; i++) {
    if (leafAssignments[i] != MPIUtil::myRank) {
      IdxType idx = removeLocalFlag(topToLocalMap[i]);
      nodes[idx].children[0] = addExtFlag(leafAssignments[i]);
    }
  }
#endif

  // Step 5: for each node in the tree, figure out the minimum
  // injection time and maximum energy of each of the CR packets it
  // holds; in the process we will record the sequence of nodes in the
  // tree, which will allow us to do future tree walks without needing
  // recursion, thereby speeding things up
  IdxType nNext = pd.size();
#ifdef ENABLE_MPI
  nNext += nTopLeaves;
#endif
  nextNode.resize(nNext);
  IdxType sibling = nullNode;
  IdxType lastNode = nullNode;
  setNodeTimeEnergyLimits(addLocalFlag(rootNode), sibling, lastNode);
  // Store last bit of traversal history
  if (isLocalNode(lastNode)) {
    nodes[removeLocalFlag(lastNode)].next = nullNode;
  } else {
    nextNode[lastNode] = nullNode;
  }
  
  // Print status if very verbose
  if (par.verbosity > 1) {
    for (int i=0; i<MPIUtil::nRank; i++) {
      if (i == MPIUtil::myRank) {
	cout << "CRTree::rebuildLocal: rank "
	     << MPIUtil::myRank << " built local tree of "
	     << nodes.size() << " nodes, holding "
	     << pd.size() << " packets"
	     << endl;
      }
    }
    MPIUtil::barrier();
  }
}

/////////////////////////////////////////////////////////////////////
// Routine to initialize the local tree using information from the
// topTree
/////////////////////////////////////////////////////////////////////
void CRTree::cloneTopTreeToLocalTree(const IdxType localNo,
					   const IdxType topNo,
					   const int treeLev,
					   const int x,
					   const int y,
					   const int z) {

  // If this node in the topTree has no children, nothing to do here
  if (topNodes[topNo].child == nullNode) return;

  // Remove flag big
  const IdxType localIdx = removeLocalFlag(localNo);
  
  // Loop over children of this top node, and clone their information
  // into the local tree
  for (int i=0; i<2; i++) {
    for (int j=0; j<2; j++) {
      for (int k=0; k<2; k++) {

	// Get index for location of this child in the topTree
	IdxType sub = 7 & PeanoKey((x << 1) + i,
				   (y << 1) + j,
				   (z << 1) + k,
				   treeLev);

	// Get half the size of new node to be added
	Real lenHalf = 0.25 * nodes[localIdx].len;

	// Store pointers to this node in both the local tree and the
	// mapping from the topTree
	nodes[localIdx].children[i + 2*j + 4*k] =
	  addLocalFlag(nodes.size());
	if (topNodes[topNodes[topNo].child + sub].child == nullNode)
	  topToLocalMap[topNodes[topNodes[topNo].child + sub].leaf]
	    = addLocalFlag(nodes.size());
	
	// Set properties of the new node
	nodes.resize(nodes.size()+1);
	node &no = nodes.back();
	no.len = 0.5 * nodes[localIdx].len;
	no.center[0] = nodes[localIdx].center[0] + (2*i - 1) * lenHalf;
	no.center[1] = nodes[localIdx].center[1] + (2*j - 1) * lenHalf;
	no.center[2] = nodes[localIdx].center[2] + (2*k - 1) * lenHalf;

	// Initialize the new nodes's parent and child pointers
	no.parent = localNo;
	for (int n=0; n<8; n++) no.children[n] = nullNode;

	// Now call recursively on children
	cloneTopTreeToLocalTree(addLocalFlag(nodes.size()) - 1,
				topNodes[topNo].child + sub,
				treeLev + 1,
				2*x + i,
				2*y + j,
				2*z + k);
      }
    }
  }
}

/////////////////////////////////////////////////////////////////////
// Routine to set the minimum time and maximum energy over nodes;
// this will be used later when finding neighbors. In the process, we
// will also fill in the nextNode array, which will make it easier to
// walk the tree in the future without the need for recursion.
/////////////////////////////////////////////////////////////////////
void
CRTree::setNodeTimeEnergyLimits(const IdxType no,
				      IdxType sibling,
				      IdxType &lastNode) {

  // Initialize to flag values
  IdxType noIdx = removeLocalFlag(no);   // Remove flag
  nodes[noIdx].maxEnergy = 0.0;

  // Record information about the order in which we are traversing the
  // tree; the variable lastNode contains the index of the last node
  // of the tree we visited, which could be either a local node or a
  // single packet
  if (lastNode != nullNode) {
    if (isLocalNode(lastNode)) {
      nodes[removeLocalFlag(lastNode)].next = no;
    } else {
      nextNode[lastNode] = no;
    }
  }

  // Record the identity of our sibling
  nodes[noIdx].sibling = sibling;

  // Update our pointer to the last visited node
  lastNode = no;

  // If this is an external node, meaning that data it holds lives on
  // another MPI rank (indicated by the extNodeFlag in its first
  // child), we cannot to anything more at this point because we do
  // not have access to the data; we will fill that information in
  // below. For now, just return.
#ifdef ENABLE_MPI
  if (isExtNode(nodes[noIdx].children[0])) return;
#endif

  // Loop over children
  for (int i=0; i<8; i++) {

    // Grab pointer to child, and skip to next if child is null
    const IdxType ch = nodes[noIdx].children[i];
    if (ch == nullNode) continue;

    // Get pointer to sibling of this child; this is either the next
    // non-null child, or the sibling of the current node if there are
    // no more non-null siblings
    IdxType nextSib = sibling;
    for (int j=i+1; j<8; j++) {
      if (nodes[noIdx].children[j] != nullNode) {
	nextSib = nodes[noIdx].children[j];
	break;
      }
    }

    // Next step depends on whether this child is an internal node
    // or a single packet
    if (isLocalNode(ch)) {

      // This is child is another local node, so call this routine
      // recursively on it, then use the resulting values to update
      // our own limits.
      setNodeTimeEnergyLimits(ch, nextSib, lastNode);
      IdxType chIdx = removeLocalFlag(ch);
      if (nodes[noIdx].maxEnergy < nodes[chIdx].maxEnergy)
	nodes[noIdx].maxEnergy = nodes[chIdx].maxEnergy;

    } else {
      
      // This child is a single packet, so just update our values
      // using that packet's values
      Real en = sr::EPacket(pd[ch]);
      if (nodes[noIdx].maxEnergy < en)
	nodes[noIdx].maxEnergy = en;

      // Store information about the sequence in which we process
      // nodes during a recursive tree walk; this goes in the next
      // field for internal nodes, and in the nextNode array for
      // individual packets
      if (lastNode != nullNode) {
	if (isLocalNode(lastNode)) {
	  nodes[removeLocalFlag(lastNode)].next = ch;
	} else {
	  nextNode[lastNode] = ch;
	}
      }

      // Update our last visited pointer
      lastNode = ch;

    }

  }

  // If we are calling this as the root node, we have now recursively
  // updated the entire tree using all our local information. The
  // final step is that we need to exchange information between MPI
  // ranks for those nodes whose children are not stored locally on
  // this MPI rank. We do this just by packing the minimum energy and
  // maximum energy for each of the nodes that is also in the
  // topNode tree into an arrays, and doing global MPI reductions on
  // them. We then copy the data back into the local nodes.
#ifdef ENABLE_MPI
  if (noIdx == rootNode) {

    // Copy data from local nodes to array in preparation for reduction
    vector<Real> maxEnergy(topNodes.size());
    for (IdxType i=0; i<topNodes.size(); i++)
      maxEnergy[i] = nodes[i].maxEnergy;

    // Call global reduction
    MPI_Allreduce(MPI_IN_PLACE, maxEnergy.data(), topNodes.size(),
		  MPI_RL, MPI_MAX, MPI_COMM_WORLD);

    // Now copy data back from buffers into node structure
    for (IdxType i=0; i<topNodes.size(); i++)
      nodes[i].maxEnergy = maxEnergy[i];
    
  }
#endif
}

/////////////////////////////////////////////////////////////////////
// Method to inject new packets from sources
/////////////////////////////////////////////////////////////////////
IdxType CRTree::injectPackets(const Real tStart,
			      const Real tStop) {

  // Loop over sources; note that this loop is not threaded, because
  // the drawing procedure within each source is threaded
  IdxType nInj = 0;
  for (IdxType i=0; i<src.size(); i++) {

    // Figure out number of packets to draw
    IdxType n = lround( (tStop - tStart) * par.packetRate *
			CRSrc::srcWgt(src[i], par.qSamp) / totSrcWgt);
    nInj += n;
    
    // Draw packet properties
    vector<CRPacket> pdNew =
      CRSrc::drawPackets(src[i], n, tStart, tStop,
			 par.qSamp, rng);

    // Set positions; rather than place them all exactly at the source
    // position, we dither the positions of the injected packets by a
    // tiny amount over a spherical region, distributing the packets
    // with a centrally-peaked density distribution, in order to
    // ensure that there is a non-zero radial gradient even in the
    // pathological case where the packets being injected are the only
    // ones in the domain. Note that this case also requires setting a
    // minimum rDither, since if there is only one source and no
    // packets, then we will have domLen = 0, at least during the
    // first step.
    vector<Real> xNew(3*n);
    Real rDither = totPacket > 1000000 ? domLen/totPacket : domLen/1000000;
    if (rDither == 0.0) rDither = 1.0e-6;
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for (IdxType j=0; j<n; j++) {
      Real r = rng.uniform(0, rDither); // p(r) ~ const --> centrally peaked
      Real mu = rng.uniform(-1.0, 1.0);
      Real phi = rng.uniform(0.0, 2.0*M_PI);
      xNew[3*j] = xSrc[3*i] + r * sqrt(1.0 - mu*mu) * cos(phi);
      xNew[3*j+1] = xSrc[3*i+1] + r * sqrt(1.0 - mu*mu) * sin(phi);
      xNew[3*j+2] = xSrc[3*i+2] + r * mu;
    }
    
    // Add packets to structure
    addPackets(xNew, pdNew);
  }

  // Return
  return nInj;
}

/////////////////////////////////////////////////////////////////////
// The routine to fill the field quantities for each packet. The
// structure is somewhat complex, due to the need to overlap
// computation and communication within an MPI + openMP framework. The
// openMP part uses a task-based model.
/////////////////////////////////////////////////////////////////////
void CRTree::fillFieldQuantities(const bool buf) {

  // If not quantities to compute, just return
  if (fQ::nFieldQty == 0) return;

  // Start openMP parallel region
#ifdef _OPENMP
#pragma omp parallel
#endif
  {

    // Set pointers; if we are working on the main packet list,
    // packetPtr starts at 0 and ends at the number of packets in the
    // tree; if we are working on the buffer, it starts at the number
    // of packets in the main tree, and ends at that plus the number
    // in the buffer.
    IdxType packetPtr = buf ? pd.size() : 0;
    IdxType packetPtrStart = packetPtr;
    IdxType packetPtrStop = buf ? pd.size() + pdbuf.size() : pd.size();

    // Loop until all packets have been processed and all work is done
    // on all ranks
    bool allDone = false;
    IdxType loopCount = 0;
#ifdef _OPENMP
#pragma omp master
#endif
    while (!allDone) {

      // If not all packets have been processed yet, launch task to
      // process next packet
      if (packetPtr < packetPtrStop) {
#ifdef _OPENMP
#pragma omp task
#endif
	fillFieldPacket(packetPtr);
	packetPtr++;
      }

      // If there are deferred packets that are now ready, launch task
      // to process next one
#ifdef ENABLE_MPI
      MPIUtil::deferredPacketData *def = comm.getReadyPacket();
      if (def) {
#ifdef _OPENMP
#pragma omp task
#endif
	fillFieldDeferredPacket(def);
      }
#endif
      
      // If there are request for neighbor search from other ranks
      // pending, launch a task to process the next one
#ifdef ENABLE_MPI
      MPIUtil::ngbSearchReq req = comm.getIncomingRequest();
      if (req.rank != -1) {
#ifdef _OPENMP
#pragma omp task
#endif
	  processIncomingRequest(req);
      }
#endif

      // Check if all local work queues are empty; if so, we should flush
      // the communicator and send mark that we are done
      bool localWorkDone = packetPtr == packetPtrStop;
#ifdef ENABLE_MPI
      localWorkDone = localWorkDone & comm.allWorkQueuesEmpty();
      comm.communicate(localWorkDone);
      if (localWorkDone) comm.markDone();
#endif

      // See if all work is done
#ifdef ENABLE_MPI
      allDone = localWorkDone && comm.allRanksDone();
#else
      allDone = localWorkDone;
#endif

      // Print if verbose
      if (par.verbosity > 2) {
	loopCount++;
	IdxType nProc = packetPtr - packetPtrStart;
#ifdef ENABLE_MPI
	IdxType nDeferred = comm.nDeferred() + comm.nReady();
	nProc -= nDeferred;
#endif
	if (nProc % 1024 == 0 && nProc > 0) {
	  cout << "CRTree:fillFieldQuantities: iteration "
	       << loopCount << ", processed " << nProc
	       << " / " << packetPtrStop - packetPtrStart
#ifdef ENABLE_MPI
	       << ", deferred = " << nDeferred
#endif
	       << endl;
	}
      }

    } // end while (!allDone)
    
  } // end openMP parallel region

  // De-allocate all communication buffers and reset all flags
#ifdef ENABLE_MPI
  comm.finalizeCycle();
#endif
}


/////////////////////////////////////////////////////////////////////
// This is the main packet advancement routine
/////////////////////////////////////////////////////////////////////
Real CRTree::advancePackets(const Real tStart, const Real tStop) {

  // Save advance time and time step mean
  vector<Real> t(pd.size() + pdbuf.size());
  
  // Loop over packets
  Real dtMin = maxReal;
  Real dtMean = 0.0;
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic) reduction(min:dtMin) reduction(+:dtMean)
#endif
  for (IdxType i=0; i<pd.size() + pdbuf.size(); i++) {

    // Grab pointer to data for this packet
    Real *x_, *q_;
    CRPacket *pd_;
    Real ht = maxReal; // h defaults to infinity if no field
		       // quantities are used
    if (i < pd.size()) {
      x_ = x.data() + 3*i;
      pd_ = pd.data() + i;
      q_ = qty.data() + i * fQ::nFieldQty;
      if (fQ::nFieldQty > 0) ht = h[i];
    } else {
      x_ = xbuf.data() + 3 * (i - pd.size());
      pd_ = pdbuf.data() + i - pd.size();
      q_ = qtybuf.data() + (i - pd.size()) * fQ::nFieldQty;
      if (fQ::nFieldQty > 0) ht = hbuf[i - pd.size()];
    }

    // Advance the packet
    Real dt;
    t[i] = advanceSinglePacket(tStart, tStop, par,
			       geom, prop, gas, rng,
			       ht, x_, *pd_, q_, dt);

    // Update global time step data
    if (dt < dtMin) dtMin = dt;
    dtMean += log(dt);
    
  }
  dtMean = exp(dtMean / (pd.size() + pdbuf.size()));

  // Set new time global step estimate. Note that this is only a
  // useful thing to do if we have field quantities; otherwise we just
  // use the fixed initial time step, since only individual particle
  // time steps matter.
  Real dtNext;
  if (fQ::nFieldQty == 0) dtNext = par.dtInit;
  else dtNext = pow(dtMin, par.cMinWgt) *
	 pow(dtMean, 1.0 - par.cMinWgt);

  // Move packets from packet list that have been marked for deletion
  // to the deleted list
  IdxType ptr = 0;
  while (ptr < pd.size()) {
    if (pd[ptr].w > 0.0) ptr++;
    else {
      // Positions
      xdel.push_back(x[3*ptr]);
      xdel.push_back(x[3*ptr+1]);
      xdel.push_back(x[3*ptr+2]);
      x.erase(x.begin() + 3*ptr, x.begin() + 3*ptr + 2);

      // Packet data
      pddel.push_back(pd[ptr]);
      pd.erase(pd.begin() + ptr);

      // Field quantities -- note that we do not save these for
      // deleted packets, since we do not use these in time step
      // estimation
      if (fQ::nFieldQty > 0) {
	qty.erase(qty.begin() + fQ::nFieldQty*ptr,
		  qty.begin() + fQ::nFieldQty*(ptr+1) - 1);
	h.erase(h.begin() + fQ::nFieldQty*ptr,
		h.begin() + fQ::nFieldQty*ptr + 1);
      }

      // Time -- here we just save the deletion time
      tdel.push_back(t[ptr]);
    }
  }

  // Repeat for packets in the buffer
  ptr = 0;
  while (ptr < pdbuf.size()) {
    if (pdbuf[ptr].w > 0.0) ptr++;
    else {
      // Positions
      xdel.push_back(xbuf[3*ptr]);
      xdel.push_back(xbuf[3*ptr+1]);
      xdel.push_back(xbuf[3*ptr+2]);
      x.erase(xbuf.begin() + 3*ptr, xbuf.begin() + 3*ptr + 2);

      // Packet data
      pddel.push_back(pdbuf[ptr]);
      pdbuf.erase(pdbuf.begin() + ptr);

      // Field quantities
      if (fQ::nFieldQty > 0) {
	qtybuf.erase(qtybuf.begin() + fQ::nFieldQty*ptr,
		     qtybuf.begin() + fQ::nFieldQty*(ptr+1) - 1);
	hbuf.erase(hbuf.begin() + ptr,
		   hbuf.begin() + ptr + 1);
      }

      // Deletion time
      tdel.push_back(t[ptr + pd.size()]);
    }
  }

  // Return estimate of next global time step
  return dtNext;
}


/////////////////////////////////////////////////////////////////////
// Routine to find neighbors
/////////////////////////////////////////////////////////////////////

bool CRTree::getNeighbors(const Real *searchCtr,
			  const Real eMin,
			  const IdxType target,
			  const int numNgb,
			  vector<IdxType> &ngb,
			  vector<Real> &dist,
			  const Real distMax) {

  // Step 1: initialize data holders
  ngb.assign(numNgb, nullNode);
  dist.assign(numNgb, maxReal);
#ifdef ENABLE_MPI
  vector<int> extRank;
  vector<Real> extDist;
#endif

  // Step 2: walk the tree
  IdxType no = addLocalFlag(rootNode);
  while (no != nullNode) {

    // Check if the current node is an internal node or a single packet
    if (isLocalNode(no)) {

      // This is an internal node; get pointer to node we're working on
      const node& nd = nodes[removeLocalFlag(no)];

      // First check if this node is ruled out because it does not
      // contain any packets high enough in energy to be relevant. If
      // so, we can discard this node and go to its sibling.
      if (nd.maxEnergy < eMin) {
	no = nd.sibling;
	continue;
      }

      // Next get the minimum distance between the search center and
      // the cube defined by this node; if distance exceeds maximum
      // distance of neighbors found so far, or the maximum allowed
      // distance in the search, we can again discard this node and go
      // to its sibling.
      Real d = geom.minDist(searchCtr, nd.center, nd.len);
      if (d > dist.back() || d > distMax) {
	no = nd.sibling;
	continue;
      }

      // Check if this is an external node, whose children live on
      // another MPI rank. If so, and if we own the target packet, we
      // need to add it to the list of MPI ranks on which we may need
      // to request searches. We only do this for packets we own,
      // since for packets owned by other ranks, those ranks will take
      // care of requesting remote neighbor searches.
#ifdef ENABLE_MPI
      if (isExtNode(nd.children[0]) && target != nullNode) {

	// Get rank on which to search
	int rank = removeExtFlag(nd.children[0]);

	// See if we already have this rank in our external search
	// list. If we do, keep whichever instance has the smaller
	// distance; if not, push this rank and its corresponding
	// distance onto search list.
	IdxType rankPtr;
	for (rankPtr = 0; rankPtr < extRank.size(); rankPtr++)
	  if (extRank[rankPtr] == rank) break;
	if (rankPtr == extRank.size()) {
	  // We do not already have this rank in our list
	  extRank.push_back(rank);
	  extDist.push_back(d);
	} else {
	  // We do have this rank in our list
	  if (d < extDist[rankPtr]) extDist[rankPtr] = d;
	}

      }
#endif
      
      // Point to next node in the tree walk
      no = nd.next;

    } else {
    
      // This node represents a single packet

      // We do not include the target particle as its own neighbor
      if (no == target) {
	no = nextNode[no];
	continue;
      }

      // Check if the energy of this packet is above the minimum
      Real en = sr::EPacket(pd[no]);
      if (en < eMin) {
	no = nextNode[no];
	continue;
      }

      // Compute distance from this packet to the search point
      Real d = geom.dist(searchCtr, x.data() + 3*no);

      // If this distance is larger than the largest distance in our
      // current neighbor list, or larger than the allowed maximum
      // distance, again move on
      if (d > dist.back() || d > distMax) {
	no = nextNode[no];
	continue;
      }

      // If we're here, we have found a new neighbor. We will now
      // insert it into our neighbor list, keeping the list ordered by
      // distance. As a first step, we do a binary search to figure
      // out where to do the insertion.
      int r;
      if (d < dist.front()) r = 0;
      else {
	int l = 0;
	r = ngb.size()-1;
	while (r - l > 1) {
	  int c = (l + r) / 2;
	  if (d > dist[c]) l = c; else r = c;
	}
      }

      // Now move the right part of the list to make room for the
      // new packet
      for (int i = ngb.size()-1; i > r; i--) {
	ngb[i] = ngb[i-1];
	dist[i] = dist[i-1];
      }

      // Insert new packet into space we have created for it
      ngb[r] = no;
      dist[r] = d;

      // Point to next node
      no = nextNode[no];

    }
  }

  // Step 3: we have now finished traversing the tree; now sanitize
  // the neighor list by removing empty entries, which are possible if
  // we found < numNgb neighbors on the local rank.
  while (ngb.back() == nullNode) {
    ngb.pop_back();
    dist.pop_back();
    if (ngb.size() == 0) break;
  }

#ifdef ENABLE_MPI
  // Step 4: handle requests to other ranks
  if (extRank.size() > 0) {

    // Step 4a: for each request to another rank, figure out how many
    // possible neighbors to request from that rank based on where the
    // minimum distance to packets on that rank falls in the list of
    // neighbors found on this rank. Note that the number of neighbors
    // to request could be zero, because it could be that, after
    // finding all the neighbors on this rank, the minimum distance to
    // the external rank is larger than the maximum distance to
    // neighbors on this rank.
    vector<int> extNumNgb(extRank.size());
    for (IdxType i=0; i<extRank.size(); i++) {
      // Locate minimum distance to this rank in list of local
      // neighbor distances
      int r;
      if (extDist[i] <= dist.front()) r = 0;
      else if (extDist[i] >= dist.back()) r = dist.size();
      else {
	int l = 0;
	r = ngb.size()-1;
	while (r - l > 1) {
	  int c = (l + r) / 2;
	  if (extDist[i] > dist[c]) l = c; else r = c;
	}
      }
      // Store number of neighbors to look for on remote rank
      extNumNgb[i] = numNgb - r;
    }
    
    // Step 4b: delete requests for searches for zero neighbors
    IdxType j = 0;
    for (IdxType i=0; i<extRank.size(); i++) {
      if (extNumNgb[i] > 0) {
	if (j < i) {
	  extRank[j] = extRank[i];
	  extNumNgb[j] = extNumNgb[i];
	}
	j++;
      }
    }
    extRank.resize(j);
    extNumNgb.resize(j);
    
    // Step 4c: if we need to make requests of external ranks, register
    // them with the communicator, then return false to indicate that
    // the search is not complete because we need to wait for other
    // ranks
    if (extRank.size() > 0) {
      comm.requestSearch(extRank, extNumNgb, target, ngb, dist,
			 searchCtr, eMin);
      return false;
    }
  }
#endif

  // Step 5: if we're here, then we do not need to do any searching on
  // other MPI ranks, so we are done can can return true
  return true;
    
}


/////////////////////////////////////////////////////////////////////
// Methods to compute field quantities
/////////////////////////////////////////////////////////////////////

void CRTree::
computeFieldQuantities(const IdxType target,
		       const vector<IdxType>& ngb,
		       vector<Real>& dist,
		       const vector<Real>& xImp,
		       const vector<CRPacket>& pdImp) {

  // If no field quantities are used, this routine is a no-op  
#if defined(COMPUTE_W) || defined(COMPUTE_DW)
  
  // Point to this packet in position and qty arrays
  Real *xt, *q, *htmp;
  CRPacket *pdt;
  if (target < pd.size()) {
    xt = x.data() + 3 * target;
    pdt = pd.data() + target;
    q = qty.data() + fQ::nFieldQty * target;
    htmp = h.data() + target;
  } else {
    IdxType t = target - pd.size();
    xt = xbuf.data() + 3 * t;
    pdt = pdbuf.data() + t;
    q = qtybuf.data() + fQ::nFieldQty * t;
    htmp = hbuf.data() + t;
  }
  Real &ht = *htmp;

  // Check for pathological case of no neighbors, in which case we
  // just set all quantities to zero
  if (dist.size() == 0) {
    for (IdxType i=0; i<fQ::nFieldQty; i++) q[i] = 0.0;
    return;
  }

  // Comptue smoothing kernel h, being careful to handle pathological
  // case where we do not have enough neighbors; in this case, we just
  // set the smoothing kernel to the distance to the most distant
  // neighbor we do have.
  struct ngbKernel::nrParams p;
  p.dist = dist.data();
  p.n = dist.size();
  Real hMax = dist.back() / ngbKernel::kernelCutoff;
  if (ngbKernel::hResid(hMax, &p) < 0.0) ht = hMax;
  else {

#if 0
    // Set initial guess if this is the first time for this packet
    if (ht == 0.0) {
      if (dist.size() < ngbKernel::nNgbAvg)
	ht = dist[ngbKernel::nNgbAvg];
      else
	ht = hMax;
    } else if (dist[0]/ht >= ngbKernel::kernelCutoff) {
      // Avoid situation where initial guess is so far off that the
      // derivative evaluates to zero
      ht = 0.5 * (dist.front() + dist.back());
    }

    // Set up GSL solver
    gsl_root_fdfsolver *s =
      gsl_root_fdfsolver_alloc(gsl_root_fdfsolver_newton);
    gsl_function_fdf fdf;
    fdf.f = &ngbKernel::hResid;
    fdf.df = &ngbKernel::hDeriv;
    fdf.fdf = & ngbKernel::hResidAndDeriv;
    fdf.params = &p;
    gsl_root_fdfsolver_set(s, &fdf, ht);
#endif

    // Set up GSL solver
    gsl_root_fsolver *s =
      gsl_root_fsolver_alloc(gsl_root_fsolver_brent);
    gsl_function f;
    f.function = &ngbKernel::hResid;
    f.params = &p;
    gsl_root_fsolver_set(s, &f, 0,
			 ngbKernel::kernelCutoff * dist.back());

    // Solve for h
    int iter = 0, status = GSL_CONTINUE;
    do {
      iter++;
      //status = gsl_root_fdfsolver_iterate(s);
      status = gsl_root_fsolver_iterate(s);
      Real h0 = ht;
      //ht = gsl_root_fdfsolver_root(s);
      ht = gsl_root_fsolver_root(s);
      status = gsl_root_test_delta(ht, h0, 0, 1.0e-2);
    } while (status == GSL_CONTINUE && iter < 100);

    // Free solver
    //gsl_root_fdfsolver_free(s);
    gsl_root_fsolver_free(s);
  
  }    

  // Cache inverse of h
#ifdef COMPUTE_W
  Real hinv3 = 1.0 / (ht * ht * ht);
#endif
#ifdef COMPUTE_DW
  Real hinv4 = hinv3 / ht;
#endif

  // Initialize quantities to zero
  for (IdxType i=0; i<fQ::nFieldQty; i++) q[i] = 0.0;

  // Loop over neighbors
  for (IdxType i=0; i<ngb.size(); i++) {

    // Point to neighbor data and position; this may be either local
    // or in the import buffer that we were passed
    const Real *x_;
    const CRPacket *pd_;
    if (isExtNode(ngb[i])) {
      IdxType n = removeExtFlag(ngb[i]);
      x_ = &xImp[3*n];
      pd_ = &pdImp[n];
    } else {
      x_ = &x[3*ngb[i]];
      pd_ = &pd[ngb[i]];
    }

    // Compute kernel weight for this packet
    Real dnorm = dist[i] / ht;
    if (dnorm >= ngbKernel::kernelCutoff) break;
#if defined(COMPUTE_W) && defined(COMPUTE_DW)
    Real w, dw;
    ngbKernel::WdW(dnorm, w, dw);
    w *= hinv3;
    dw *= hinv4;
#elif defined(COMPUTE_W)
    Real w = ngbKernel::W(dnorm) * hinv3;
    Real dw = 0.0;
#elif defined(COMPUTE_DW)
    Real w = 0.0;
    Real dw = ngbKernel::dW(dnorm) * hinv4;
#endif
    
    // Compute field quantities
    fQ::addToFieldQty(xt, x_, dist[i], pd_, w, dw, q);
  }
    
  // Add contribution from target particle itself
#if defined(COMPUTE_W)
  Real w = ngbKernel::W(0) * hinv3;
  Real dw = 0.0;
  fQ::addToFieldQty(xt, xt, 0.0, pdt, w, dw, q);
#endif

#endif
  // end COMPUTE_W || COMPUTE_DW
}


/////////////////////////////////////////////////////////////////////
// Methods to get neighbors and compute field quantities for a single
// packet
/////////////////////////////////////////////////////////////////////
void CRTree::fillFieldPacket(const IdxType p) {
  
  // Grab data for this packet
  Real *x_;
  Real en;
  if (p < pd.size()) {
    x_ = x.data() + 3*p;
    en = sr::EPacket(pd[p]);
  } else {
    IdxType p1 = p - pd.size();
    x_ = xbuf.data() + 3*p1;
    en = sr::EPacket(pdbuf[p1]);
  }
    
  // Find neighbors; if this returns false, that means we need to wait
  // for a neighbor search on other MPI ranks, so we skip computing
  // field quantities for this packet now; it has been placed in the
  // deferred queue
  vector<IdxType> ngb;
  vector<Real> dist;
  if (!getNeighbors(x_, en, p, controls::numNgb, ngb, dist)) return;

  // Compute local quantities (pressure, pressure gradient, etc.) at
  // each packet from neighbor list
  computeFieldQuantities(p, ngb, dist);
}


#ifdef ENABLE_MPI
/////////////////////////////////////////////////////////////////////
// Method to fill field quantities for a packet that was deferred to
// await data from another MPI rank
/////////////////////////////////////////////////////////////////////

void CRTree::
fillFieldDeferredPacket(MPIUtil::deferredPacketData *d) {

  // Compute field quantities from the neighbor list
  computeFieldQuantities(d->target, d->ngb, d->dist, d->x, d->pd);

  // Delete the deferred packet data now that we're done
  delete d;
}
#endif

/////////////////////////////////////////////////////////////////////
// Method to carry out neighbor searches at the request of other MPI
// ranks
/////////////////////////////////////////////////////////////////////

#ifdef ENABLE_MPI
void CRTree::
processIncomingRequest(const MPIUtil::ngbSearchReq &req) {

  // Prepare holder for response
  MPIUtil::ngbSearchResp resp;

  // Copy rank and target information
  resp.rank = req.rank;
  resp.resp.target = req.req.target;

  // Find neighbors
  vector<IdxType> ngb;
  vector<Real> dist;
  getNeighbors(req.req.searchCtr, req.req.eMin, nullNode,
	       req.req.numNgb, ngb, dist, req.req.distMax);

  // Copy neighbor list into response
  for (IdxType j=0; j<ngb.size(); j++) {
    for (int k=0; k<3; k++) resp.resp.x[3*j + k] = x[3*ngb[j]+k];
    resp.resp.dist[j] = dist[j];
    resp.resp.packets[j] = pd[ngb[j]];
  }

  // Put flag values into reminder of distance array
  for (IdxType j=ngb.size(); j<controls::numNgb; j++)
    resp.resp.dist[j] = maxReal;

  // Queue the response for transmission to other rank
  comm.queueResponse(resp);
}
#endif

