
#include "CheckpointManager.H"
#include "utils/constants.H"
#include "utils/MPIUtil.H"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cmath>
#include <type_traits>
#include <arpa/inet.h>
#if __cplusplus >= 201703L
#   include <filesystem>
#elif defined(USE_BOOST)
#   include <boost/filesystem.hpp>
using namespace boost;
#endif

using namespace std;
using namespace criptic;
namespace fQ = fieldQuantities;

CheckpointManager::CheckpointManager(ParmParser &pp,
				     const controls::params &par_) :
  par(par_) {

  // Get checkpoint name and format
  if (!pp.query("output.chkname", baseChkName))  
    baseChkName = "criptic_";
  if (!pp.query("output.ascii", writeAscii))
    writeAscii = 0;
  if (!pp.query("output.rng", saveRngState))
    saveRngState = 1;

  // Get checkpoint intervals in time and step number
  if (!pp.query("output.chk_int", chkInt)) chkInt = 0;
  if (!pp.query("output.chk_dt", chkDt)) chkDt = 0.0;

  // Handle restarts
  if (!pp.isRestart()) {

    // This is not a restart
    restartNum = -1;
    chkNum = 0;
    if (chkInt > 0) nextChkStep = chkInt; else nextChkStep = maxInt;
    if (chkDt > 0) nextChkTime = chkDt; else nextChkTime = maxReal;
    
#if 0
  } else {
    
    // This is a restart, so construct the restart file name based on
    // the command line options
    restartNum = -1;
    if (pp.restartFile().length() != 0) {
      
      // We were explicitly given a restart file name, so we will use
      // that. This name should be in the format basenameNNNNN.chk or
      // basenameNNNNN_rankMMMMM.chk; we need to extract the basename
      // part and the NNNNN part
      restartName = pp.restartFile();
      string errstr("CheckpointManager: checkpoint file names "
		    "must be in the format basenameNNNNN.chk or "
		    "basenameNNNNN_rankMMMMM.chk");

      // Chop off the .chk
      if (restartName.length() < 4) MPIUtil::haltRun(errstr, 1);
      if (restartName.compare(restartName.length() - 4, 4, ".chk") != 0)
	MPIUtil::haltRun(errstr, 1);
      restartName.erase(restartName.length() - 4);

      // Chop off the _rankMMMMM if present
      if (restartName.length() >= 5 + controls::chkDigits) {
	string sub = restartName.substr(restartName.length()
					- 5 - controls::chkDigits, 5);
	if (sub.compare("_rank") == 0) {
	  restartName.erase(restartName.length() - 5 - controls::chkDigits);
	}
      }

      // Extract the checkpoint number
      if (restartName.length() < controls::chkDigits)
	MPIUtil::haltRun(errstr, 1);
      string numstr = restartName.substr(restartName.length(),
					 controls::chkDigits);
      try {
	restartNum = stoi(numstr);
	chkNum = restartNum + 1;
      } catch (const std::invalid_argument& ia) {
	MPIUtil::haltRun(errstr, 1);
      }

      // Remove checkpoint number from restart file base name
      restartName.erase(restartName.length() - controls::chkDigits);
      
    } else {
      
      // No explicit file name specified, so use the base name in the
      // input file, and find the highest-numbered checkpoint file
      // with that base name
      restartName = baseChkName;
      string ext(".chk");
      string fname;
      for (auto &p : std::filesystem::directory_iterator(".")) {
	if (p.path().extension() != ext) continue;
	string stem = p.path().stem().string();
	if (stem.rfind(restartName, 0) == 0) {
	  string numstr = stem.substr(restartName.length(),
				      controls::chkDigits);
	  try {
	    int n = stoi(numstr);
	    if (n > restartNum) {
	      restartNum = n;
	      chkNum = restartNum + 1;
	      fname = stem;
	    }
	  } catch (const std::invalid_argument& ia) { }
	}
      }

      // Make sure we found a valid restart file
      if (fname.length() == 0) {
	string errstr = "CheckpointManager: unable to find restart "
	  "file with base name " + baseChkName;
	MPIUtil::haltRun(errstr, 1);
      } else if (par.verbosity > 0 && MPIUtil::IOProc) {
	cout << "CheckpointManager: restarting from checkpoint "
	     << fname << endl;
      }
    }
#endif
  }
}

void
CheckpointManager::writeCheckpoint(const int step,
				   const criptic::Real t,
				   const CRTree& tree,
				   const RngVec& rng) {
  
  // Construct the output filename
  stringstream ss;
  ss << baseChkName << setfill('0') << setw(controls::chkDigits) << chkNum;

  // Print status
  if (par.verbosity > 0 && MPIUtil::IOProc)
    cout << "CheckpointManager: writing checkpoint "
	 << ss.str() << " at step = " << step+1
	 << ", t = " << t << endl;

  // Write metadata file
  if (MPIUtil::IOProc) {
    stringstream ssMeta;
    ssMeta << ss.str() << ".txt";
    ofstream metaData;
    string fname = ssMeta.str();
    metaData.open(fname);
    metaData << "CRIPTIC checkpoint" << endl;
    metaData << "Units = "
#ifdef CRIPTIC_UNITS_MKS
	     << "MKS"
#else
	     << "CGS"
#endif
	     << endl;
    metaData << "t = " << t << endl;
    metaData << "step = " << step << endl;
    if (writeAscii)
      metaData << "format = ASCII" << endl;
    else
      metaData << "format = binary" << endl;
    metaData << "nPacket = " << tree.nPacket() << endl;
    metaData << "nDeletedPacket = " << tree.nDeletedPacket() << endl;
    metaData << "nSrc = " << tree.nSrc() << endl;
    metaData << "nRank = " << MPIUtil::nRank << endl;
#ifdef _OPENMP
    metaData << "nThread = " << omp_get_num_threads() << endl;
#else
    metaData << "nThread = " << 1 << endl;
#endif    
    metaData.close();
  }
  
  // Add MPI rank extension  
#ifdef ENABLE_MPI
  ss << "_rank" << setfill('0') << setw(controls::chkDigits) << MPIUtil::myRank;
#endif

  // Write out packets and sources
  stringstream ssChk;
  ssChk << ss.str() << ".chk";
  string fname = ssChk.str();
  ofstream outfile;
  if (writeAscii) 
    outfile.open(fname);
  else
    outfile.open(fname, ios::binary);
  outfile << "nPacket = " << tree.nPacketLoc() << endl;
  outfile << "nDeletedPacket = " << tree.nDeletedPacketLoc() << endl;
  outfile << "nSrc = " << tree.nSrcLoc() << endl;

  // ASCII vs binary output
  if (writeAscii) {

    // For ASCII, first dump a header line  
    int fWidth = 15;
    outfile << setw(fWidth) << "x"
	    << setw(fWidth) << "y" 
	    << setw(fWidth) << "z"
	    << setw(fWidth) << "type"
	    << setw(fWidth) << "source"
	    << setw(fWidth) << "uniqueID"
	    << setw(fWidth) << "t_inj"
	    << setw(fWidth) << "wgt_inj"
	    << setw(fWidth) << "momentum"
	    << setw(fWidth) << "wgt"
	    << setw(fWidth) << "grammage";
    for (IdxType i=0; i<fQ::nFieldQty; i++)
      outfile << setw(fWidth) << fQ::qtyName[i];
    outfile << endl;

    // Next dump packets; note that for ASCII output we convert internal
    // CR units (momentum in m_p * c, energy in m_p * c^2) to GeV/c
    // for momentum, and GeV for energy; for local quantities,
    // pressures are output as P/k_B, energies as eV / volume (cm^3
    // or m^3 depending on unit system), and number densities as
    // number / volume
    for (IdxType i=0; i<tree.nPacketLoc(); i++) {
      const Real *x = tree.packetPos(i);
      const CRPacket *pd = tree.packetData(i);
      const Real *qty = tree.packetQty(i);
      outfile << setw(fWidth) << x[0]
	      << setw(fWidth) << x[1]
	      << setw(fWidth) << x[2]
	      << setw(fWidth) << (int) pd->type
	      << setw(fWidth) << (int) pd->src
	      << setw(fWidth) << (int) pd->uniqueID
	      << setw(fWidth) << pd->tInj
	      << setw(fWidth) << pd->wInj
	      << setw(fWidth) << pd->p
	* constants::mp_c / (units::GeV / constants::c)
	      << setw(fWidth) << pd->w
	      << setw(fWidth) << pd->gr;
      for (IdxType n=0; n<fQ::nFieldQty; n++)
	outfile << setw(fWidth) << qty[n] * fQ::qtyUnit[n];
      outfile << endl;
    }

    // Write header line for deleted packets
    outfile << setw(fWidth) << "x"
	    << setw(fWidth) << "y" 
	    << setw(fWidth) << "z"
	    << setw(fWidth) << "type"
	    << setw(fWidth) << "source"
	    << setw(fWidth) << "uniqueID"
	    << setw(fWidth) << "t_inj"
	    << setw(fWidth) << "wgt_inj"
	    << setw(fWidth) << "momentum"
	    << setw(fWidth) << "wgt"
	    << setw(fWidth) << "grammage"
	    << setw(fWidth) << "t_del"
	    << endl;

    // Write deleted packets
    for (IdxType i=0; i<tree.nDeletedPacketLoc(); i++) {
      const Real *x = tree.deletedPacketPos(i);
      const CRPacket *pd = tree.deletedPacketData(i);
      const Real *tdel = tree.deletedPacketTime(i);
      outfile << setw(fWidth) << x[0]
	      << setw(fWidth) << x[1]
	      << setw(fWidth) << x[2]
	      << setw(fWidth) << (int) pd->type
	      << setw(fWidth) << (int) pd->src
	      << setw(fWidth) << (int) pd->uniqueID
	      << setw(fWidth) << pd->tInj
	      << setw(fWidth) << pd->wInj
	      << setw(fWidth) << pd->p
	* constants::mp_c / (units::GeV / constants::c)
	      << setw(fWidth) << pd->w
	      << setw(fWidth) << pd->gr
	      << setw(fWidth) << *tdel
	      << endl;
    }

    // Dump header line for sources
    outfile << endl
	    << setw(fWidth) << "x"
	    << setw(fWidth) << "y" 
	    << setw(fWidth) << "z"
	    << setw(fWidth) << "type"
	    << setw(fWidth) << "uniqueID"
	    << setw(fWidth) << "p0"
	    << setw(fWidth) << "p1"
	    << setw(fWidth) << "q"
	    << setw(fWidth) << "coef"
	    << endl;

    // Now write sources
    for (IdxType i=0; i<tree.nSrcLoc(); i++) {
      const Real *x = tree.srcPos(i);
      const CRSource *src = tree.srcData(i);
      outfile << setw(fWidth) << x[0]
	      << setw(fWidth) << x[1]
	      << setw(fWidth) << x[2]
	      << setw(fWidth) << src->type
	      << setw(fWidth) << (int) src->uniqueID
	      << setw(fWidth) << src->p0
	* constants::mp_c / (units::GeV / constants::c)
	      << setw(fWidth) << src->p1
	* constants::mp_c / (units::GeV / constants::c)
	      << setw(fWidth) << src->q
	      << setw(fWidth) << src->coef
	* (units::GeV / constants::c) / constants::mp_c
	      << endl;
    }

  } else {

    // Binary output

    // First dump out list of field quantities as ASCII
    outfile << fQ::qtyName[0];
    for (IdxType i=1; i<fQ::nFieldQty; i++)
      outfile << " " << fQ::qtyName[i];
    outfile << endl;

    // Next write out a line indicating if the binary data are in
    // big-endian or little-endian order
    if (htonl(10) == 10) {
      // Big endian, because network order = machine order
      outfile << "1" << endl;
    } else {
      // Little endian
      outfile << "0" << endl;
    }

    // Now write out lines containing the size of a Real, the size of
    // an int, the size of an IdxType, the size of a CRPacket, the
    // size of a CRSource, and the alignment of both
    outfile << sizeof(Real) << " " << sizeof(int) << " "
	    << sizeof(IdxType) << " " << sizeof(CRPacket) << " "
	    << sizeof(CRSource) << " "
#ifdef USE_BOOST
	    << boost::alignment_of<CRPacket>::value << " "
	    << boost::alignment_of<CRSource>::value
#else
	    << alignment_of<CRPacket>::value << " "
	    << alignment_of<CRSource>::value
#endif
	    << endl;

    // Write out a line giving the offsets of different elements of a
    // CR packet
    outfile << offsetof(CRPacket, type) << " "
	    << offsetof(CRPacket, src)  << " "
	    << offsetof(CRPacket, uniqueID) << " "
	    << offsetof(CRPacket, tInj) << " "
	    << offsetof(CRPacket, wInj) << " "
	    << offsetof(CRPacket, p) << " "
	    << offsetof(CRPacket, w) << " "
	    << offsetof(CRPacket, gr) << endl;

    // Write out a line giving the offsets of different elements of a
    // CR source
    outfile << offsetof(CRSource, type) << " "
	    << offsetof(CRSource, uniqueID)  << " "
	    << offsetof(CRSource, p0) << " "
	    << offsetof(CRSource, p1) << " "
	    << offsetof(CRSource, q) << " "
	    << offsetof(CRSource, coef) << endl;

    // Now dump remainder of data as binary
    if (tree.nPacketLoc() > 0) {
      // Live packets
      const Real *x = tree.packetPos(0);
      const CRPacket *pd = tree.packetData(0);
      const Real *qty = tree.packetQty(0);
      outfile.write((const char *) x, 3*tree.nPacketLoc()*sizeof(Real));
      outfile.write((const char *) pd, tree.nPacketLoc()*sizeof(CRPacket));
      outfile.write((const char *) qty,
		    fieldQuantities::nFieldQty *
		    tree.nPacketLoc()*sizeof(Real));
    }
    if (tree.nDeletedPacketLoc() > 0) {
      // Deleted packets
      const Real *xdel = tree.deletedPacketPos(0);
      const CRPacket *pddel = tree.deletedPacketData(0);
      const Real *tdel = tree.deletedPacketTime(0);
      outfile.write((const char *) xdel,
		    3*tree.nDeletedPacketLoc()*sizeof(Real));
      outfile.write((const char *) pddel,
		    tree.nDeletedPacketLoc()*sizeof(CRPacket));
      outfile.write((const char *) tdel,
		    tree.nDeletedPacketLoc()*sizeof(Real));
    }
    if (tree.nSrcLoc() > 0) {
      // Sources
      const Real *xSrc = tree.srcPos(0);
      const CRSource *src = tree.srcData(0);
      outfile.write((const char *) xSrc, 3*tree.nSrcLoc()*sizeof(Real));
      outfile.write((const char *) src, tree.nSrcLoc()*sizeof(CRSource));
    }
  }
  outfile.close();

  // Save the rng state
  if (saveRngState) {
    stringstream ssRng;
    ssRng << ss.str() << ".rng";
    string fname = ssRng.str();
    ofstream rngfile;
    rngfile.open(fname, ios::binary);
    rngfile << rng;
    rngfile.close();
  }

  // Increment counter
  chkNum++;

  // Save time and step number of this checkpoint
  lastChkStep = step;
  lastChkTime = t;

  // Record time and step number of this checkpoint, and compute time
  // and step number when next checkpoint should be written
  if (chkInt > 0)
    nextChkStep = chkInt * (step / chkInt + 1);
  if (chkDt > 0.0)
    nextChkTime = chkDt * (round(t / chkDt) + 1.0);
}
  
