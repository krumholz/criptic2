// Implementation of the geometry class

#include "Geometry.H"

using namespace std;
using namespace criptic;

Geometry::Geometry(ParmParser &pp, controls::params& pars) {

  // First get BC type on low side, from the boundary.lo keyword
  vector<string> bcLoStr;
  bool hasLoBC = pp.query("boundary.lo_type", bcLoStr);
  bool allLoOpen = true;
  if (!hasLoBC) {
    bcLo.assign(3, openBC);
  } else {
    bcLo.resize(3);
    if (bcLoStr.size() != 3)
      MPIUtil::haltRun("Geometry: expected three values for "
		       "boundary.lo_type", errBadInputFile);
    for (int i=0; i<3; i++) {
      if (bcLoStr[i] == "open")
	bcLo[i] = openBC;
      else if (bcLoStr[i] == "absorbing")
	bcLo[i] = absorbingBC;
      else if (bcLoStr[i] == "reflecting")
	bcLo[i] = reflectingBC;
      else if (bcLoStr[i] == "periodic")
	bcLo[i] = periodicBC;
      else {
	string errMsg = "Geometry: unknown boundary condition type: " +
	  bcLoStr[i];
	MPIUtil::haltRun(errMsg, errBadInputFile);
      }
      if (bcLo[i] != openBC) allLoOpen = false;
    }
  }
  
  // Repeat on high side
  vector<string> bcHiStr;
  bool hasHiBC = pp.query("boundary.hi_type", bcHiStr);
  bool allHiOpen = true;
  if (!hasHiBC) {
    bcHi.resize(3);
    for (int i=0; i<3; i++)
      if (bcLo[i] == periodicBC) bcHi[i] = periodicBC;
      else bcHi[i] = openBC;
  } else {
    bcHi.resize(3);
    if (bcHiStr.size() != 3)
      MPIUtil::haltRun("Geometry: expected three values for "
		       "boundary.hi_type", errBadInputFile);
    for (int i=0; i<3; i++) {
      if (bcHiStr[i] == "open")
	bcHi[i] = openBC;
      else if (bcHiStr[i] == "absorbing")
	bcHi[i] = absorbingBC;
      else if (bcHiStr[i] == "reflecting")
	bcHi[i] = reflectingBC;
      else if (bcHiStr[i] == "periodic")
	bcHi[i] = periodicBC;
      else {
	string errMsg = "Geometry: unknown boundary condition type: " +
	  bcHiStr[i];
	MPIUtil::haltRun(errMsg, errBadInputFile);
      }
      if (bcHi[i] != openBC) allHiOpen = false;
    }
  }
  
  // Error check: make sure that if the low boundary condition says
  // periodic, the high one does too, and vice-versa
  for (int i=0; i<3; i++) {
    if ((bcLo[i] == periodicBC && bcHi[i] != periodicBC) ||
	(bcLo[i] != periodicBC && bcHi[i] == periodicBC)) {
      MPIUtil::haltRun("Geometry: if one of boundary.lo_type and "
		       "boundary.hi_type is periodic, coresponding "
		       "value on other face must be periodic as well",
		       errBadInputFile);
    }
  }

  // Set periodicity flag
  periodicity.resize(3);
  for (int i=0; i<3; i++) periodicity[i] = (bcLo[i] == periodicBC);

  // Next check for size of domain; this is mandatory if boundary
  // condition is not open, and is allowed but ignored if boundary is
  // open;
  bool hasProbLo = pp.query("boundary.prob_lo", probLo);
  bool hasProbHi = pp.query("boundary.prob_hi", probHi);
  if (!hasProbLo && !allLoOpen)
    MPIUtil::haltRun("Geometry: did not find required boundary.prob_lo "
		     "entry in input file", errBadInputFile);
  if (!hasProbHi && !allHiOpen)
    MPIUtil::haltRun("Geometry: did not find required boundary.prob_hi "
		     "entry in input file", errBadInputFile);

  // Safety checks
  if (hasProbLo && probLo.size() != 3)
    MPIUtil::haltRun("Geometry: exected three values for boundary.prob_lo",
		     errBadInputFile);
  if (hasProbHi && probHi.size() != 3)
    MPIUtil::haltRun("Geometry: exected three values for boundary.prob_hi",
		     errBadInputFile);

  // If domain is infinite or semi-infinite, fill in flag values
  probLo.resize(3);
  probHi.resize(3);
  for (int i=0; i<3; i++) {
    if (bcLo[i] == openBC) probLo[i] = -maxReal;
    if (bcHi[i] == openBC) probHi[i] = maxReal;
  }

  // Set domain size
  probSize.resize(3);
  for (int i=0; i<3; i++) {
    if (bcLo[i] != openBC && bcHi[i] != openBC)
      probSize[i] = probHi[i] - probLo[i];
    else
      probSize[i] = maxReal;
    if (probSize[i] <= 0.0)
      MPIUtil::haltRun("Geometry: boundary.prob_hi values must be larger "
		       "than boundary.prob_lo values", errBadInputFile);
  }

  // Ensure we have a finite softening length; if one is not set, and
  // we have a finite problem size, then set the softening length to a
  // default value of 1e-6 of problem size
  if (pars.soften == 0.0) {
    Real minProbSize = maxReal;
    for (int i=0; i<3; i++)
      if (minProbSize > probSize[i]) minProbSize = probSize[i];
    if (minProbSize == maxReal)
      MPIUtil::haltRun("Geometry: for infinite or semi-infinite domain "
		       "softening length must be set in input file",
		       errBadInputFile);
    else
      pars.soften = 1e-6 * minProbSize;
  }
}

