// Implementation of the parmParser class

#include "ParmParser.H"

#include <iostream>
#include <sstream>
#include <fstream>
#include <stdexcept>
#include <typeinfo>
#include "utils/MPIUtil.H"
#include "utils/constants.H"
#include "globalControls.H"

using namespace std;
using namespace criptic;

#ifdef USE_BOOST
// Boost is available as an alternative to the c++17 standard
#include <boost/variant/get.hpp>
using namespace boost;

// Define our own version of holds_alternative
template <typename T, typename... Ts>
bool holds_alternative(const boost::variant<Ts...>& v) noexcept
{
    return boost::get<T>(&v) != nullptr;
}

// Use #define's to specify which versions of GET and INDEX
#define GET         boost::get
#define VARIDX(X)   ((X).which())

#else

#define GET         std::get
#define VARIDX(X)   ((X).index())

#endif

////////////////////////////////////////////////////////////////////////
// The constructor
////////////////////////////////////////////////////////////////////////

ParmParser::ParmParser(int argc, char **argv)
{
  // Loop over arguments
  string paramFileName;
  int argptr = 1;
  restart = false;
  while (argptr < argc) {

    // Read next argument and see if it begins with -, indicating a
    // command flag, or not, indicating that this is a potential
    // argument
    string arg(argv[argptr++]);
    if (arg.find("-") == 0) {

      // Flag string, so decide what to do next based on which flag it
      // is
      if (!arg.compare("-h") || !arg.compare("--help")) {
	
	// Help flag; just print usage information and exit
	printUsage();
	MPIUtil::shutdown();
	exit(0);
	
      } else if (!arg.compare("-rng") || !arg.compare("--rngfile")) {

	// Flag to specify a file from which to read the random number
	// generator state; the next argument gives the file name
	if (argptr >= argc) {
	  printUsage();
	  MPIUtil::haltRun("expected filename after --rngfile option",
			   errBadCommandLine);
	}
	rngFileName = argv[argptr++];

      } else if (!arg.compare("-r") || !arg.compare("--restart")) {

	// Flag to specify to restart from a checkpoint
	restart = true;

	// If this is a restart, and the argument after it is a file
	// name, read that
	if (argptr < argc) {
	  string nextarg(argv[argptr+1]);
	  if (nextarg.find("-") == 0) {
	    restartFileName = string(argv[argptr++]);
	  }
	}

      } else {

	// Unknown flag; bail out
	printUsage();
	stringstream ss;
	ss << "unknown option: " << arg;
	MPIUtil::haltRun(ss.str(), errBadCommandLine);

      }

    } else {

      // No flag, so this must be the name of our input file
      if (paramFileName.length() != 0) {
	printUsage();
	MPIUtil::haltRun("found unexpected argument after input file name",
			 errBadCommandLine);
      }
      paramFileName = arg;

    }
  }

  // Make sure we got an input file name, and bail out if not
  if (paramFileName.length() == 0) {
    printUsage();
    MPIUtil::haltRun("missing input file name", errBadCommandLine);
  }

  // Open parameter file
  ifstream paramFile;
  paramFile.open(paramFileName.c_str(), ios::in);
  if (!paramFile.is_open()) {
    string errMsg = "unable to open file " + paramFileName;
    MPIUtil::haltRun(errMsg, errBadInputFile);
  }

  // Parse parameter file
  parseFile(paramFile);

  // Close the file
  paramFile.close();
}

////////////////////////////////////////////////////////////////////////
// Methods to get the value of a keyword
////////////////////////////////////////////////////////////////////////

void ParmParser::get(const string& name, int& val) const {
  if (keywords.find(name) == keywords.end()) missingKeyError(name);
  const keyword& k = keywords.at(name);
  if (holds_alternative<int>(k)) {
    val = GET<int>(k);
  } else if (VARIDX(k) == 1) {
    // Real --> int
    val = static_cast<int>(GET<Real>(k));
  } else {
    // If we're here, we don't know how to do this type conversion, so
    // throw an error
    typeError(name);
  }
}

void ParmParser::get(const string& name, Real& val) const {
  if (keywords.find(name) == keywords.end()) missingKeyError(name);
  const keyword& k = keywords.at(name);
  if (holds_alternative<Real>(k)) {
    val = GET<Real>(k);
  } else if (VARIDX(k) == 0) {
    // int --> Real
    val = static_cast<Real>(GET<Real>(k));
  } else {
    // If we're here, we don't know how to do this type conversion, so
    // throw an error
    typeError(name);
  }
}

void ParmParser::get(const string& name, string& val) const {
  if (keywords.find(name) == keywords.end()) missingKeyError(name);
  const keyword& k = keywords.at(name);
  if (holds_alternative<string>(k)) {
    val = GET<string>(k);
  } else if (VARIDX(k) == 0) {
    // int --> string
    val = to_string(GET<int>(k));
  } else if (VARIDX(k) == 1) {
    // Real --> string
    val = to_string(GET<Real>(k));
  } else {
    // If we're here, we don't know how to do this type conversion, so
    // throw an error
    typeError(name);
  }
}

void ParmParser::get(const string& name, vector<int>& val) const {
  if (keywords.find(name) == keywords.end()) missingKeyError(name);
  const keyword& k = keywords.at(name);
  if (holds_alternative<vector<int> >(k)) {
    val = GET<vector<int> >(k);
  } else if (VARIDX(k) == 0) {
    // int --> vector<int>
    val.clear();
    val.push_back(GET<int>(k));
  } else if (VARIDX(k) == 1) {
    // Real --> vector<int>
    val.clear();
    val.push_back(static_cast<int>(GET<Real>(k)));
  } else if (VARIDX(k) == 4) {
    // vector<Real> --> vector<int>
    val.clear();
    vector<Real> v = GET<vector<Real> >(k);
    for (vector<Real>::size_type i=0; i<v.size(); i++)
      val.push_back(static_cast<int>(v[i]));
  } else {
    // If we're here, we don't know how to do this type conversion, so
    // throw an error
    typeError(name);
  }
}

void ParmParser::get(const string& name, vector<Real>& val) const {
  if (keywords.find(name) == keywords.end()) missingKeyError(name);
  const keyword& k = keywords.at(name);
  if (holds_alternative<vector<Real> >(k)) {
    val = GET<vector<Real> >(k);
  } else if (VARIDX(k) == 0) {
    // int --> vector<Real>
    val.clear();
    val.push_back(GET<int>(k));
  } else if (VARIDX(k) == 1) {
    // Real --> vector<Real>
    val.clear();
    val.push_back(GET<Real>(k));
  } else if (VARIDX(k) == 3) {
    // vector<int> --> vector<int>
    val.clear();
    vector<Real> v = GET<vector<Real> >(k);
    for (vector<Real>::size_type i=0; i<v.size(); i++)
      val.push_back(static_cast<int>(v[i]));
  } else {
    // If we're here, we don't know how to do this type conversion, so
    // throw an error
    typeError(name);
  }
}

void ParmParser::get(const string& name, vector<string>& val) const {
  if (keywords.find(name) == keywords.end()) missingKeyError(name);
  const keyword& k = keywords.at(name);
  if (holds_alternative<vector<string> >(k)) {
    val = GET<vector<string> >(k);
  } else if (VARIDX(k) == 0) {
    // int --> vector<string>
    val.clear();
    val.push_back(to_string(GET<int>(k)));
  } else if (VARIDX(k) == 1) {
    // Real --> vector<string>
    val.clear();
    val.push_back(to_string(GET<Real>(k)));
  } else if (VARIDX(k) == 2) {
    // string --> vector<string>
    val.clear();
    val.push_back(GET<string>(k));
  } else if (VARIDX(k) == 3) {
    // vector<int> --> vector<string>
    val.clear();
    vector<int> v = GET<vector<int> >(k);
    for (vector<int>::size_type i=0; i<v.size(); i++)
      val.push_back(to_string(v[i]));
  } else if (VARIDX(k) == 4) {
    // vector<Real> --> vector<string>
    val.clear();
    vector<Real> v = GET<vector<Real> >(k);
    for (vector<Real>::size_type i=0; i<v.size(); i++)
      val.push_back(to_string(v[i]));
  }
}

template <class T> bool
ParmParser::query(const string &name, T& val) const {
  if (keywords.find(name) == keywords.end()) return false;
  else {
    this->get(name, val);
    return true;
  }
}

// Instatiate various versions of the query member function
template bool ParmParser::query(const string&, int&) const;
template bool ParmParser::query(const string&, Real&) const;
template bool ParmParser::query(const string&, string&) const;
template bool ParmParser::query(const string&, vector<int>&) const;
template bool ParmParser::query(const string&, vector<Real>&) const;
template bool ParmParser::query(const string&, vector<string>&) const;


////////////////////////////////////////////////////////////////////////
// Method to extract the global control parameters
////////////////////////////////////////////////////////////////////////

controls::params ParmParser::getGlobalPars() const {

  // Initialize output holder
  controls::params pars;

  // Set default values first
  pars.cStep = 0.5;
  pars.cMinWgt = 0.5;
  pars.errTol = 1.0e-4;
  pars.pMin = (units::MeV / constants::c) / constants::mp_c;
  pars.wFracMin = 0.01;
  pars.tStop = maxReal;
  pars.dtMin = eps;
  pars.dtIncFac = 1.5;
  pars.qSamp = -1.0;
  pars.verbosity = 1;
  pars.maxStep = maxInt;
  pars.soften = 0.0;

  // Read data
  query("c_step", pars.cStep);
  query("c_min_wgt", pars.cMinWgt);
  query("err_tol", pars.errTol);
  if (query("p_min", pars.pMin)) {
    // Convert user input in GeV/c to internal units
    pars.pMin *= (units::GeV/constants::c) / constants::mp_c;
  }
  query("w_frac_min", pars.wFracMin);
  query("t_stop", pars.tStop);
  get("dt_init", pars.dtInit); // This parameter is mandatory
  get("packet_rate", pars.packetRate); // This parameter is mandatory
  query("dt_min", pars.dtMin);
  query("q_samp", pars.qSamp);
  query("dt_inc_fac", pars.dtIncFac);
  query("verbosity", pars.verbosity);
  query("max_step", pars.maxStep);
  query("soften", pars.soften);

  // Safety checks
  if (pars.cMinWgt < 0.0 || pars.cMinWgt > 1.0)
    MPIUtil::haltRun("c_min_wgt must be from 0 - 1", errBadInputFile);
  if (pars.wFracMin >= 1.0)
    MPIUtil::haltRun("w_frac_min must be < 1", errBadInputFile);
  if (pars.dtIncFac < 1.0)
    MPIUtil::haltRun("dt_inc_fac must be >= 1", errBadInputFile);

  // Return
  return pars;
}


////////////////////////////////////////////////////////////////////////
// Method to write out all parameter values
////////////////////////////////////////////////////////////////////////

void ParmParser::dumpParams(ostream &outFile) {

  if (MPIUtil::IOProc) {
    // Loop over all key, value pairs
    for (auto it = keywords.begin(); it != keywords.end(); ++it) {
      string k = it->first;
      vector<string> v;
      this->get(k, v);
      outFile << k << "  ";
      for (vector<string>::size_type i=0; i<v.size(); i++)
	outFile << "  " << v[i];
      outFile << endl;
    }
  }
}

////////////////////////////////////////////////////////////////////////
// Method to print a usage message
////////////////////////////////////////////////////////////////////////

void ParmParser::printUsage() {
  if (MPIUtil::IOProc) {
    cout << "Usage: criptic criptic.in" << endl;
    cout << "       criptic [-r or --restart [RESTARTFILE]] "
	 << "[-rn N or --restartnum N] "
	 << "[-rng RNGFILE or --rngfile RNGFILE] criptic.in"
	 << endl;
    cout << "       criptic [-h or --help]" << endl;
    cout << "       Options: " << endl;
    cout << "       --restart : restart from a checkpoint; can "
	 << "optionally be followed by the name"
	 << "                   of a checkpoint file" << endl;
    cout << "       --rngfile : read random number generator state "
	 << "from indicated file" << endl;
    cout << "       --help : print this message and exit" << endl;
  }
}

////////////////////////////////////////////////////////////////////////
// Method to parse an open parameter file
////////////////////////////////////////////////////////////////////////

void ParmParser::parseFile(ifstream &paramFile) {
  string line;
  while (!(paramFile.eof())) {

    // Read a line, and save a copy of the original
    getline(paramFile, line);
    string origLine(line);

    // Trim leading and trailing whitespace
    const std::string wschars = "\t\n\v\f\r ";
    line.erase(0, line.find_first_not_of(wschars));
    line.erase(line.find_last_not_of(wschars)+1);

    // Trim anything after a #
    while (true) {
      size_t commentPos = line.find_last_of("#");
      if (commentPos == string::npos) break;
      line.erase(commentPos);
    }

    // If line is empty, continue
    if (line.length() == 0) continue;

    // Break line up into whitespace-separated tokens
    vector<string> tokens;
    istringstream iss(line);
    copy(istream_iterator<string>(iss),
	 istream_iterator<string>(),
	 back_inserter(tokens));

    // Proceed based on number of tokens
    if (tokens.size() < 2) {
      
      // Need at least two tokens, so this is an error
      parseError(origLine);

    } else if (tokens.size() == 2) {

      // Exactly two tokens, so try to parse this first as an integer,
      // then if that fails as a double, then if that fails as a
      // string
      size_t pos;
      bool success = false;

      // Try parsing as an integer first
      int ival;
      try {
	ival = stoi(tokens[1], &pos);
	if (pos == tokens[1].length()) success = true;
      } catch (...) { }
      if (success) {
	keywords[tokens[0]] = ival;
      } else {
	// Try parsing as a real number
	Real rval;
	try {
	  rval = stod(tokens[1], &pos);
	  if (pos == tokens[1].length()) success = true;
	} catch (...) { }
	if (success) {
	  keywords[tokens[0]] = rval;
	} else {
	  // If we're here, this didn't parse as in int or a real, so
	  // just leave as a string
	  keywords[tokens[0]] = tokens[1];
	}
      }

    } else {
    
      // For two or more arguments, this is going to be a vector of
      // something; proceed to determine the type of that something
      // exactly as in the single argument case
      size_t pos;
      bool success = false;

      // Try parsing as integers
      vector<int> ival;
      try {
	for (vector<string>::size_type i=1; i<tokens.size(); i++) {
	  ival.push_back(stoi(tokens[i], &pos));
	  if (pos != tokens[i].length()) break;
	}
	if (ival.size() == tokens.size()-1) success = true;
      } catch (...) { }
      if (success) {
	keywords[tokens[0]] = ival;
      } else {
	// Try parsing as real numbers
	vector<Real> rval;
	try {
	  for (vector<string>::size_type i=1; i<tokens.size(); i++) {
	    rval.push_back(stod(tokens[i], &pos));
	    if (pos != tokens[i].length()) break;
	  }
	  if (rval.size() == tokens.size()-1) success = true;
	} catch (...) { }
	if (success) {
	  keywords[tokens[0]] = rval;
	} else {
	  // If we're here, this didn't parse as ints or reals, so
	  // just make it a vector of strings
	  vector<string> sval;
	  for (vector<string>::size_type i=1; i<tokens.size(); i++)
	    sval.push_back(tokens[i]);
	  keywords[tokens[0]] = sval;
	}
      }
    }
  }
}
    
////////////////////////////////////////////////////////////////////////
// Methods to throw error and exit
////////////////////////////////////////////////////////////////////////

void ParmParser::parseError(const string& line) const {
  string err = "unable to parse input file line: " + line;
  MPIUtil::haltRun(err, errBadInputFile);
}

void ParmParser::typeError(const string& name) const {
  string err = "for keyword " + name + ", unable to interpret value";
  vector<string> kOut;
  get(name, kOut);
  for (auto k : kOut) err += "  " + k;
  MPIUtil::haltRun(err, errBadInputFile);
}

void ParmParser::missingKeyError(const string& name) const {
  string err = "keyword " + name + " not found in input file";
  MPIUtil::haltRun(err, errBadInputFile);
}
