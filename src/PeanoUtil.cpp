#include "PeanoUtil.H"
#include "CRPacket.H"
#include <algorithm>
#include <numeric>

using namespace std;
using namespace criptic;
using namespace Peano;

// Implementation of the parallel sort by key
template<class T>
void Peano::sortByPKey(vector<PKey> &key,
		       vector<Real> &x,
		       vector<T> &pd,
		       vector<Real> &cost) {

  // Make aray of indices
  vector<IdxType> idx(key.size());
  iota(idx.begin(), idx.end(), 0);

  // Sort indices by key
  sort(idx.begin(), idx.end(), [&key](IdxType a, IdxType b) { return key[a] < key[b]; } );

  // Now re-arrange the data
  vector<bool> sorted(key.size());
  for (IdxType i=0; i<key.size(); i++) {
    if (sorted[i]) continue;
    sorted[i] = true;
    IdxType prev_j = i;
    IdxType j = idx[i];
    while (i != j) {
      for (int n=0; n<3; n++) swap(x[3*prev_j+n], x[3*j+n]);
      swap(key[prev_j], key[j]);
      swap(pd[prev_j], pd[j]);
      swap(cost[prev_j], cost[j]);
      sorted[j] = true;
      prev_j = j;
      j = idx[j];
    }
  }
}

// Explicit instantiation of templates
template void
Peano::sortByPKey<CRPacket>(std::vector<PKey> &key,
			    std::vector<criptic::Real> &x,
			    std::vector<CRPacket> &pd,
			    std::vector<criptic::Real> &cost);
