#include "GasModelCartesian.H"

using namespace std;
using namespace criptic;

//////////////////////////////////////////////////////////////////////
// Implementation of the GasModelCartesian class
//////////////////////////////////////////////////////////////////////

// Constructor
GasModelCartesian::GasModelCartesian(const ParmParser& pp,
				     const Geometry& geom_) :
  geom(geom_) {

  // Read size of Cartesian grid from the parser
  vector<int> n;
  pp.get("grid.n", n);
  if (n.size() != 3)
    MPIUtil::haltRun("GasModelCartesian: expected three values for "
		     "grid.n", errBadGridSpecification);

  // Safety check: make sure geometry doesn't have open boundary conditions
  for (int i=0; i<3; i++) {
    if (geom.bLo(i) == Geometry::openBC ||
	geom.bHi(i) == Geometry::openBC) {
      MPIUtil::haltRun("GasModelCartesian: can't use open boundary "
		       "conditions with a Cartesian gas model",
		       errBadGridSpecification);
    }
  }

  // Compute grid properties
  dxMin = maxReal;
  for (int i=0; i<3; i++) {
    if (n[i] < 1)
      MPIUtil::haltRun("GasModelCartesian: values of grid.n must "
		       "be positive integers", errBadGridSpecification);
    nGrid[i] = n[i];
    dx[i] = geom.xSize(i) / n[i];
    if (dx[i] < dxMin) dxMin = dx[i];
  }

  // Allocate memory to hold gas data
  gasData.resize( (nGrid[0] + 2) *
		  (nGrid[1] + 2) *
		  (nGrid[2] + 2) *
		  nf );
}


// Method to apply boundary conditions to compute ghost zone
// properties. The basic rule is:
// absorbing boundaries --> all quantities linearly extrapolated
//                          across buondary
// reflecting boundaries --> all quantities except the component of
//                           vectors normal to surface linearly
//                           extrapolated; vector component normal to
//                           surface is reflected
// periodic boundaries --> just to periodic fill
void GasModelCartesian::applyBC() {

  // Only one MPI rank does this
  if (!MPIUtil::IOProc) return;

  // Safety check
  if (nGrid[0] < 2 || nGrid[1] < 2 || nGrid[2] < 2)
    MPIUtil::haltRun("GasModelCartesian: applyBC requires grid "
		     "size >= 2 cells in every dimension",
		     errBadGridSpecification);

  // Lo x face
  for (IdxType j=0; j<nGrid[1]; j++) {
    for (IdxType k=0; k<nGrid[2]; k++) {
      for (IdxType n=0; n<nf; n++) {
	switch (geom.bLo(0)) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(-1,j,k,n) = 2 * (*this)(0,j,k,n) -
	      (*this)(1,j,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
	  {
	    if (n == vxIdx || n == BxIdx)
	      (*this)(-1,j,k,n) = -(*this)(0,j,k,n);
	    else
	      (*this)(-1,j,k,n) = 2 * (*this)(0,j,k,n) -
		(*this)(1,j,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(-1,j,k,n) = (*this)(nGrid[0]-1,j,k,n);
	    break;
	  }
	}
      }
    }
  }
      
  // Hi x face
  for (IdxType j=0; j<nGrid[1]; j++) {
    for (IdxType k=0; k<nGrid[2]; k++) {
      for (IdxType n=0; n<nf; n++) {
	switch (geom.bHi(0)) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(nGrid[0],j,k,n) = 2 * (*this)(nGrid[0]-1,j,k,n) -
	      (*this)(nGrid[0]-2,j,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
	  {
	    if (n == vxIdx || n == BxIdx)
	      (*this)(nGrid[0],j,k,n) = -(*this)(nGrid[0]-1,j,k,n);
	    else
	      (*this)(nGrid[0],j,k,n) = 2 * (*this)(nGrid[0]-1,j,k,n) -
		(*this)(nGrid[0]-2,j,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(nGrid[0],j,k,n) = (*this)(0,j,k,n);
	    break;
	  }
	}
      }
    }
  }

  // Lo y face
  for (IdxType i=0; i<nGrid[0]; i++) {
    for (IdxType k=0; k<nGrid[2]; k++) {
      for (IdxType n=0; n<nf; n++) {
	switch (geom.bLo(1)) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(i,-1,k,n) = 2 * (*this)(i,0,k,n) -
	      (*this)(i,1,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
	  {
	    if (n == vyIdx || n == ByIdx)
	      (*this)(i,-1,k,n) = -(*this)(i,0,k,n);
	    else
	      (*this)(i,-1,k,n) = 2 * (*this)(i,0,k,n) -
		(*this)(i,1,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(i,-1,k,n) = (*this)(i,nGrid[1]-1,k,n);
	    break;
	  }
	}
      }
    }
  }
      
  // Hi y face
  for (IdxType i=0; i<nGrid[0]; i++) {
    for (IdxType k=0; k<nGrid[2]; k++) {
      for (IdxType n=0; n<nf; n++) {
	switch (geom.bHi(1)) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(i,nGrid[1],k,n) = 2 * (*this)(i,nGrid[1]-1,k,n) -
	      (*this)(i,nGrid[1]-2,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
	  {
	    if (n == vyIdx || n == ByIdx)
	      (*this)(i,nGrid[1],k,n) = -(*this)(i,nGrid[1]-1,k,n);
	    else
	      (*this)(i,nGrid[1],k,n) = 2 * (*this)(i,nGrid[1]-1,k,n) -
		(*this)(i,nGrid[1]-2,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(i,nGrid[1],k,n) = (*this)(i,0,k,n);
	    break;
	  }
	}
      }
    }
  }

  // Lo z face
  for (IdxType i=0; i<nGrid[0]; i++) {
    for (IdxType j=0; j<nGrid[1]; j++) {
      for (IdxType n=0; n<nf; n++) {
	switch (geom.bLo(2)) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(i,j,-1,n) = 2 * (*this)(i,j,0,n) -
	      (*this)(i,j,1,n);
	    break;
	  }
	case Geometry::reflectingBC:
	  {
	    if (n == vzIdx || n == BzIdx)
	      (*this)(i,j,-1,n) = -(*this)(i,j,0,n);
	    else
	      (*this)(i,j,-1,n) = 2 * (*this)(i,j,0,n) -
		(*this)(i,j,1,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(i,j,-1,n) = (*this)(i,j,nGrid[2]-1,n);
	    break;
	  }
	}
      }
    }
  }
      
  // Hi z face
  for (IdxType i=0; i<nGrid[0]; i++) {
    for (IdxType j=0; j<nGrid[1]; j++) {
      for (IdxType n=0; n<nf; n++) {
	switch (geom.bHi(2)) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(i,j,nGrid[2],n) = 2 * (*this)(i,j,nGrid[2]-1,n) -
	      (*this)(i,j,nGrid[2]-2,n);
	    break;
	  }
	case Geometry::reflectingBC:
	  {
	    if (n == vzIdx || n == BzIdx)
	      (*this)(i,j,nGrid[2],n) = -(*this)(i,j,nGrid[2]-1,n);
	    else
	      (*this)(i,j,nGrid[2],n) = 2 * (*this)(i,j,nGrid[2]-1,n) -
		(*this)(i,j,nGrid[2]-2,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(i,j,nGrid[2],n) = (*this)(i,j,0,n);
	    break;
	  }
	}
      }
    }
  }

  // xy edges -- these are the four corners of every xy slice
  for (IdxType k=0; k<nGrid[2]; k++) {

    // (-1,-1,*) and (-1,nGrid[1],*) edges
    for (SignedIdxType j=-1; j<=(SignedIdxType) nGrid[1]; j+=nGrid[1]+1) {
      for (IdxType n=0; n<nf; n++) {
	switch (geom.bLo(0)) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(-1,j,k,n) = 2 * (*this)(0,j,k,n) -
	      (*this)(1,j,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
	  {
	    if (n == vxIdx || n == BxIdx)
	      (*this)(-1,j,k,n) = -(*this)(0,j,k,n);
	    else
	      (*this)(-1,j,k,n) = 2 * (*this)(0,j,k,n) -
		(*this)(1,j,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(-1,j,k,n) = (*this)(nGrid[0]-1,j,k,n);
	    break;
	  }
	}
      }
    }

    // (nGrid[0],-1,*) and (nGrid[0],nGrid[1],*) edges
    for (SignedIdxType j=-1; j<=(SignedIdxType) nGrid[1]; j+=nGrid[1]+1) {
      for (IdxType n=0; n<nf; n++) {
	switch (geom.bHi(0)) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(nGrid[0],j,k,n) = 2 * (*this)(nGrid[0]-1,j,k,n) -
	      (*this)(nGrid[0]-2,j,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
	  {
	    if (n == vxIdx || n == BxIdx)
	      (*this)(nGrid[0],j,k,n) = -(*this)(nGrid[0]-1,j,k,n);
	    else
	      (*this)(nGrid[0],j,k,n) = 2 * (*this)(nGrid[0]-1,j,k,n) -
		(*this)(nGrid[0]-2,j,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(nGrid[0],j,k,n) = (*this)(0,j,k,n);
	    break;
	  }
	}
      }
    }      
  }

  // xz edges -- these are the four corners of every xz slice
  for (IdxType j=0; j<nGrid[1]; j++) {

    // (-1,*,-1) and (-1,*,nGrid[2]) edges
    for (SignedIdxType k=-1; k<=(SignedIdxType) nGrid[2]; k+=nGrid[2]+1) {
      for (IdxType n=0; n<nf; n++) {
	switch (geom.bLo(0)) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(-1,j,k,n) = 2 * (*this)(0,j,k,n) -
	      (*this)(1,j,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
	  {
	    if (n == vxIdx || n == BxIdx)
	      (*this)(-1,j,k,n) = -(*this)(0,j,k,n);
	    else
	      (*this)(-1,j,k,n) = 2 * (*this)(0,j,k,n) -
		(*this)(1,j,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(-1,j,k,n) = (*this)(nGrid[0]-1,j,k,n);
	    break;
	  }
	}
      }
    }

    // (nGrid[0],*,-1) and (nGrid[0],*,nGrid[2]) edges
    for (SignedIdxType k=-1; k<=(SignedIdxType) nGrid[2]; k+=nGrid[2]+1) {
      for (IdxType n=0; n<nf; n++) {
	switch (geom.bHi(0)) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(nGrid[0],j,k,n) = 2 * (*this)(nGrid[0]-1,j,k,n) -
	      (*this)(nGrid[0]-2,j,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
	  {
	    if (n == vxIdx || n == BxIdx)
	      (*this)(nGrid[0],j,k,n) = -(*this)(nGrid[0]-1,j,k,n);
	    else
	      (*this)(nGrid[0],j,k,n) = 2 * (*this)(nGrid[0]-1,j,k,n) -
		(*this)(nGrid[0]-2,j,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(nGrid[0],j,k,n) = (*this)(0,j,k,n);
	    break;
	  }
	}
      }
    }      
  }

  // yz edges -- these are the four corners of every yz slice
  for (IdxType i=0; i<nGrid[0]; i++) {

    // (*,-1,-1) and (*,-1,nGrid[2]) edges
    for (SignedIdxType k=-1; k<=(SignedIdxType) nGrid[2]; k+=nGrid[2]+1) {
      for (IdxType n=0; n<nf; n++) {
	switch (geom.bLo(1)) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(i,-1,k,n) = 2 * (*this)(i,0,k,n) -
	      (*this)(i,1,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
	  {
	    if (n == vyIdx || n == ByIdx)
	      (*this)(i,-1,k,n) = -(*this)(i,0,k,n);
	    else
	      (*this)(i,-1,k,n) = 2 * (*this)(i,0,k,n) -
		(*this)(i,1,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(i,-1,k,n) = (*this)(i,nGrid[1]-1,k,n);
	    break;
	  }
	}
      }
    }

    // (*,nGrid[1],-1) and (*,nGrid[1],nGrid[2]) edges
    for (SignedIdxType k=-1; k<=(SignedIdxType) nGrid[2]; k+=nGrid[2]+1) {
      for (IdxType n=0; n<nf; n++) {
	switch (geom.bHi(1)) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(i,nGrid[1],k,n) = 2 * (*this)(i,nGrid[1]-1,k,n) -
	      (*this)(i,nGrid[1]-2,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
	  {
	    if (n == vyIdx || n == ByIdx)
	      (*this)(i,nGrid[1],k,n) = -(*this)(i,nGrid[1]-1,k,n);
	    else
	      (*this)(i,nGrid[1],k,n) = 2 * (*this)(i,nGrid[1]-1,k,n) -
		(*this)(i,nGrid[1]-2,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(i,nGrid[1],k,n) = (*this)(i,0,k,n);
	    break;
	  }
	}
      }
    }
  }

  // Six corners
  for (SignedIdxType j=-1; j<=(SignedIdxType) nGrid[1]; j+=nGrid[1]+1) {
    for (SignedIdxType k=-1; k<=(SignedIdxType) nGrid[2]; k+=nGrid[2]+1) {

      // (-1,-1,-1), (-1,nGrid[1],-1), (-1,-1,nGrid[2]), and
      // (-1,nGrid[1],nGrid[2]) corners
      for (IdxType n=0; n<nf; n++) {
	switch (geom.bLo(0)) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(-1,j,k,n) = 2 * (*this)(0,j,k,n) -
	      (*this)(1,j,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
	  {
	    if (n == vxIdx || n == BxIdx)
	      (*this)(-1,j,k,n) = -(*this)(0,j,k,n);
	    else
	      (*this)(-1,j,k,n) = 2 * (*this)(0,j,k,n) -
		(*this)(1,j,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(-1,j,k,n) = (*this)(nGrid[0]-1,j,k,n);
	    break;
	  }
	}	
      }

      // (nGrid[0],-1,-1), (nGrid[0],nGrid[1],-1),
      // (nGrid[0],-1,nGrid[2]), and (nGrid[0],nGrid[1],nGrid[2]) corners
      for (IdxType n=0; n<nf; n++) {
	switch (geom.bHi(0)) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(nGrid[0],j,k,n) = 2 * (*this)(nGrid[0]-1,j,k,n) -
	      (*this)(nGrid[0]-2,j,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
	  {
	    if (n == vxIdx || n == BxIdx)
	      (*this)(nGrid[0],j,k,n) = -(*this)(nGrid[0]-1,j,k,n);
	    else
	      (*this)(nGrid[0],j,k,n) = 2 * (*this)(nGrid[0]-1,j,k,n) -
		(*this)(nGrid[0]-2,j,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(nGrid[0],j,k,n) = (*this)(0,j,k,n);
	    break;
	  }
	}
      }
    }
  } 
}

