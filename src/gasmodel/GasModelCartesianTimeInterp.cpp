#include "GasModelCartesianTimeInterp.H"
#include <sstream>

using namespace std;
using namespace criptic;

//////////////////////////////////////////////////////////////////////
// Implementation of the GasModelCartesianTimeInterp class
//////////////////////////////////////////////////////////////////////

// Constructor
GasModelCartesianTimeInterp::
GasModelCartesianTimeInterp(const ParmParser& pp,
			    const Geometry& geom) {

  // Construct the old time and new time gas objects
  gasOld = new GasModelCartesian(pp, geom);
  gasNew = new GasModelCartesian(pp, geom);

  // Read the time slices available; this can be specified either by
  // just giving the time between slices and the total number
  // available, or by giving a list of all times available
  if (!pp.query("grid.times", tSlice)) {
    Real dt;
    pp.get("grid.dt", dt);
    int nSlice;
    pp.get("grid.nslice", nSlice);
    tSlice.resize(nSlice);
    for (int i=0; i<nSlice; i++) tSlice[i] = i*dt;
  }

  // Make sure the first time slice is time zero, and we have at least
  // two slices
  if (tSlice[0] != 0.0 || tSlice.size() < 2)
    MPIUtil::haltRun("GasModelCartesianTimeInterp: need >= 2 slice times"
		     ", starting at t = 0", errBadGridSpecification);

  // Store the initial time step
  pp.get("dt_init", dtSave);

  // Check to make sure initial time step is less than time of first
  // slice; it is an error if not
  if (dtSave > tSlice[1])
    MPIUtil::haltRun("GasModelCartesianTimeInterp: initial simulation "
		     "time step must be < time of second slice",
		     errBadGridSpecification);
}

// Method to read the new data data, and also adjust time steps so
// that the end of a time step always falls on the time when we need
// to read the gas data
void GasModelCartesianTimeInterp::updateState(Real t,
					      Real &tNext,
					      Real &dt) {

  // Safety check
  if (t >= tSlice.back())
    MPIUtil::haltRun("GasModelCartesianTimeInterp: simulation time "
		     "has reached time of last time slice",
		     errBadGridSpecification);
  
  // Examine time to decide what to do
  if (t == 0.0) {

    // First call at t = 0, so inialize by reading first two slices
    loadData(0);     // Load initial slice into gasNew
    swapOldNew();    // Shift gasNew --> gasOld
    loadData(1);     // Read next slice into gasNew
    tPtr = 1;        // Initialize pointer to slice in gasNew
    gasOld->applyBC(); // Fill boundaries in gasOld
    gasNew->applyBC(); // Fill boundaries in gasNew
#ifndef NDEBUG
    checkData();       // Verify that data have been loaded correctly
#endif

  } else if (t == tSlice[tPtr]) {
    
    // Current time matches (to within the expected precision) the
    // time at which we are supposed to load new data, so load it.
    swapOldNew();
    tPtr++;
    loadData(tPtr);
    gasNew->applyBC();
    
  } else if (tNext > tSlice[tPtr]) {
    
    // Check if t + dt will put us over the time at which we're
    // supposed to load new data. If so, throttle the time step.
    tNext = tSlice[tPtr];
    dt = tNext - t;
    
  }
}


// Method to verify that data have been loaded correctly
#ifndef NDEBUG
void GasModelCartesianTimeInterp::checkData() const {
  for (SignedIdxType i=-1; i<(SignedIdxType) gasOld->gridSize()[0]+1;
       i++) {
    for (SignedIdxType j=-1; j<(SignedIdxType) gasOld->gridSize()[1]+1;
	 j++) {
      for (SignedIdxType k=-1; k<(SignedIdxType) gasOld->gridSize()[2]+1;
	   k++) {
	if ((*gasOld)(i,j,k,GasModelCartesian::totDenIdx) <= 0.0 ||
	    (*gasOld)(i,j,k,GasModelCartesian::ionDenIdx) <= 0.0 ||
	    (*gasNew)(i,j,k,GasModelCartesian::totDenIdx) <= 0.0 ||
	    (*gasNew)(i,j,k,GasModelCartesian::ionDenIdx) <= 0.0) {
	  stringstream ss;
	  ss << "GasModelCartesianTimeInterp: invalid data in cell ("
	     << i << ", " << j << ", " << k << "); "
	     << "old (tot, ion) density = "
	     << (*gasOld)(i,j,k,GasModelCartesian::totDenIdx)
	     << ", "
	     << (*gasOld)(i,j,k,GasModelCartesian::ionDenIdx)
	     << ", new (tot, ion) density = "
	     << (*gasNew)(i,j,k,GasModelCartesian::totDenIdx)
	     << ", "
	     << (*gasNew)(i,j,k,GasModelCartesian::ionDenIdx)
	     << endl;
	  MPIUtil::haltRun(ss.str(), errInvalidGridData);
	}
      }
    }
  }
}
#endif
