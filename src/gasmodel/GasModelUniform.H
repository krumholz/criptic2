// This header defines a class that specializes the GasModel class to
// represent a uniform gas, with the gas properties read from the prob
// stanza of the criptic.in file.

#ifndef _GAS_MODEL_UNIFORM_H_
#define _GAS_MODEL_UNIFORM_H_

#include "GasModel.H"
#include "../ParmParser.H"

class GasModelUniform : public GasModel {
public:
  // Constructor -- this reads data from parser
  GasModelUniform(const ParmParser& pp) {

    // Read data from ParmParser
    pp.get("prob.density", rho);
    pp.get("prob.ionDensity", rhoi);
    pp.get("prob.magField", B);
    pp.get("prob.velocity", v);
    pp.get("prob.dx", dx);

    // Store tangent, normal, and binormal vectors
    criptic::Real gradB[9];
    for (int i=0; i<9; i++) gradB[i] = 0.0;
    vec3::TNBVectors(B.data(), gradB, eT, eN, eB);
  }

  // Destructor
  virtual ~GasModelUniform() { };

  // Functions to return gas properties
  virtual void density(const criptic::Real x[3],
		       const criptic::Real t,
		       criptic::Real &density_) const {
    density_ = rho;
  }
  virtual void ionDensity(const criptic::Real x[3],
			  const criptic::Real t,
			  criptic::Real &ionDensity_) const {
    ionDensity_ = rhoi;
  }
  virtual void velocity(const criptic::Real x[3],
			const criptic::Real t,
			criptic::Real velocity_[3]) const {
    for (int i=0; i<3; i++) velocity_[i] = v[i];
  }
  virtual void magField(const criptic::Real x[3],
			const criptic::Real t,
			criptic::Real magField_[3]) const {
    for (int i=0; i<3; i++) magField_[i] = B[i];
  }
  virtual void magFieldGrad(const criptic::Real x[3],
			    const criptic::Real t,
			    criptic::Real magFieldGrad_[9]) const {
    for (int i=0; i<9; i++) magFieldGrad_[i] = 0.0;
  }

  // Return TNB vectors
  virtual void TNBVectors(const criptic::Real x[3],
			  const criptic::Real t,
			  criptic::Real eT_[3],
			  criptic::Real eN_[3],
			  criptic::Real eB_[3]) const {
    for (int i=0; i<3; i++) {
      eT_[i] = eT[i];
      eN_[i] = eN[i];
      eB_[i] = eB[i];
    }
  }

  // Return length scale
  virtual criptic::Real dxScale(const criptic::Real x[3],
				const criptic::Real t) const {
    return dx;
  }
  
private:
  criptic::Real rho, rhoi, dx;
  std::vector<criptic::Real> B, v;
  criptic::Real eT[3], eN[3], eB[3];
};

#endif
// _GAS_MODEL_UNIFORM_H_
