// This namespace defines global control parameters.

#ifndef GLOBALCONTROLS_H_
#define GLOBALCONTROLS_H_

#include "utils/types.H"
#include "ngbKernel.H"

namespace controls {

#ifdef NGBFAC
  static constexpr int numNgb = NGBFAC * ngbKernel::nNgbAvg;
#else
  static constexpr int numNgb = 1.3 * ngbKernel::nNgbAvg;
#endif

  // Block size for MPI messages during neighbor searching
#ifdef MPIBLOCKSIZE
  static constexpr int MPIBlockSize = MPIBLOCKSIZE;
#else
  static constexpr int MPIBlockSize = 128;
#endif

  // Minimum number of subdivisions of packets per MPI rank when
  // building trees
#ifdef MINDIVPERRANK
  static constexpr criptic::Real minDivPerRank = MINDIVPERRANK;
#else
  static constexpr criptic::Real minDivPerRank = 32.0;
#endif

  // Automatically subdivide any node that contains > maxOpenFac times
  // as many packets as its parent
#ifdef MAXOPENFAC
  static constexpr criptic::Real maxOpenFac = MAXOPENFAC;
#else
  static constexpr criptic::Real maxOpenFac = 0.8;
#endif

  // Number of digits used in checkpoint file numbers
#ifdef CHKDIGITS
  static constexpr int chkDigits = CHKDIGITS;
#else
  static constexpr int chkDigits = 5;
#endif
  

  // A typedef for a struct that holds control parameters set an
  // run-time rather than compile-time
  typedef struct {
    criptic::Real cStep;     // Time step size parameter for local steps
    criptic::Real cMinWgt;   // Relative weight between min and mean global step
    criptic::Real errTol;    // Error tolerance in field line trace
    criptic::Real pMin;      // Minimum CR momentum to follow
    criptic::Real wFracMin;  // Minimum fraction of initial CR weight
			     // to retain
    criptic::Real tStop;     // Time at which to stop
    criptic::Real dtInit;    // Starting time step
    criptic::Real dtMin;     // Minimum time step
    criptic::Real dtIncFac;  // Maximum factor which step can increase
    criptic::Real qSamp;     // Index of momentum sampling distribution
    criptic::Real packetRate; // # packets injected / simulation time
    criptic::Real soften;    // Softening length used in tree operations
    int verbosity;           // Verbosity level
    int maxStep;             // Maximum number of steps
  } params;
};

#endif
// _GLOBALCONTROLS_H_
