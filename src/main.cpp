// Main

#include <cstdio>
#include <iostream>
#include "CheckpointManager.H"
#include "CRPacket.H"
#include "CRSource.H"
#include "CRTree.H"
#include "ParmParser.H"
#include "Prob.H"
#include "globalControls.H"
#include "gasmodel/GasModel.H"
#include "propmodel/PropModel.H"
#include "utils/RngVec.H"
#include "utils/MPIUtil.H"
#include "definitions.H"

using namespace std;
using namespace criptic;

int main(int argc, char *argv[]) {

  // Initialize MPI
  MPIUtil::setup(&argc, &argv);

  // Parse the inputs file and extract global control parameters
  ParmParser pp(argc, argv);
  controls::params params = pp.getGlobalPars();

  // Print status
  if (params.verbosity > 0 && MPIUtil::IOProc) {
    cout << "CRIPTIC starting up: ";
#ifdef _OPENMP
    cout << omp_get_max_threads();
    if (omp_get_max_threads() > 1)
      cout << " threads, ";
    else
      cout << " thread, ";
#endif
    cout << MPIUtil::nRank;
    if (MPIUtil::nRank > 1)
      cout << " MPI ranks" << endl;
    else
      cout << " MPI rank" << endl;
  }
  
  // Initialize the random number generator
  RngVec rng(pp);
  
  // Initialize the problem geometry
  Geometry geom(pp, params);

  // Read parameters for the CR propagation model
  PropModel *prop = initProp(pp);

  // Start up the checkpoint manager
  CheckpointManager chk(pp, params);

  // Initialize the background gas state, calling the user's method to
  // provide the background gas state; the type of object returns
  // depends on how the gas model is specified
  if (params.verbosity > 0 && MPIUtil::IOProc)
    cout << "CRIPTIC: initializing background gas" << endl;
  GasModel *gas = initGas(pp, geom);

  // Get list of initial CR packets from user
  if (params.verbosity > 0 && MPIUtil::IOProc)
    cout << "CRIPTIC: initializing CR packets and sources" << endl;
  vector<Real> xPacketInit;
  vector<CRPacket> packetInit;
  initPackets(pp, xPacketInit, packetInit, rng);

  // Get list of initial CR sources from user  
  vector<Real> xSrcInit;
  vector<CRSource> srcInit;
  initSources(pp, xSrcInit, srcInit, rng);
    
  // Build the initial tree of CR packets and sources
  if (params.verbosity > 0 && MPIUtil::IOProc)
    cout << "CRIPTIC: building tree" << endl;
  CRTree tree(geom, *prop, *gas, rng, params,
	      xPacketInit, packetInit,
	      xSrcInit, srcInit);

  // Initialize field quantities at every packet
  if (params.verbosity > 1 && MPIUtil::IOProc)
    cout << "CRIPTIC: initial computation of field quantities"
	 << endl;
  tree.fillFieldQuantities();

  // Write out initial state
  chk.writeCheckpoint(-1, 0.0, tree, rng);

  // Start main simulation loop
  Real t = 0.0;
  Real dt = params.dtInit;
  bool writeCheckpoint = false;
  int step;
  for (step = 0; step < params.maxStep && t < params.tStop; step++) {

    // Save time step estimate before throttling
    Real dtSave = dt;
    Real tNext = t + dt;
    
    // Update the gas state for the new time, and throttle time step
    // if required
    gas->updateState(t, tNext, dt);

    // Throttle time step to avoid overshooting simulation end time or
    // next checkpoint time; also record if we should write a checkpoint
    if (tNext >= params.tStop && params.tStop > 0.0) {
      tNext = params.tStop;
      dt = tNext - t;
    }
    if (tNext >= chk.nextTime()) {
      tNext = chk.nextTime();
      dt = tNext - t;
      writeCheckpoint = true;
    }
    if (step == chk.nextStep()) writeCheckpoint = true;

    // Inject new packets, and compute field quantities for the
    // newly-injected packets
    if (tree.injectPackets(t, tNext) > 0)
      tree.fillFieldQuantities(true);

    // Advance packets, getting an estimate for the next global time
    // step in the process
    Real dtNewEst = tree.advancePackets(t, tNext);
    
    // Increment time
    t = tNext;
    
    // Limit time step increase
    if (dtNewEst / dtSave > params.dtIncFac)
      dtNewEst = dtSave * params.dtIncFac;
    
    // Call the user work function, which allows the user to perform
    // arbitrary modifications of the system (e.g., moving or changing
    // the properties of sources, changing the background gas state,
    // etc.)
    userWork(t, *gas, *prop, tree);
    
    // Rebuild the tree
    tree.rebuild();

    // Fill field quantities for next time step
    tree.fillFieldQuantities();
    
    // Print status if verbose
    if (params.verbosity > 0) {
      cout << "CRIPTIC: completed step " << step+1
	   << ": t = " << t << ", dt_done = " << dt
	   << ", dt_next = " << dtNewEst
	   << ", n_packet = " << tree.nPacket()
	   << endl;
    }
	
    // Write checkpoint if necessary
    if (writeCheckpoint) {
      chk.writeCheckpoint(step, t, tree, rng);
      tree.clearDeletedPackets();  // Clear the deleted packet list
      writeCheckpoint = false;
    }

    // Set next global time step, and bail out if it is too
    // small
    dt = dtNewEst;
    if (dt < params.dtMin) break;
  }

  // Save final state if we didn't just write it
  if (t != chk.lastTime() && step != chk.lastStep())
    chk.writeCheckpoint(step-1, t, tree, rng);

  // Print status
  if (params.verbosity > 0) {
    cout << "CRIPTIC: run complete: ";
    if (step == params.maxStep)
      cout << "reached maximum step number = " << step << endl;
    else if (t >= params.tStop)
      cout << "reached maximum time = " << t << endl;
    else if (dt < params.dtMin)
      cout << "time step dt = " << dt << " below minimum dt = "
	   << params.dtMin << endl;
  }

  // Delete user-constructed objects
  delete gas;
  delete prop;
  
  // Shutdown
  MPIUtil::shutdown();
  exit(0);
}  
