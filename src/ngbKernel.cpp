#include <iostream>
#include "ngbKernel.H"

// Functions used to compute local smoothing length

// This function returns r = sum_i W(q_i, h) - hfac3; the goal of the
// Newton-Raphson solve is to make this residual zero
double ngbKernel::hResid(double h, void *params) {
  struct nrParams *par = (struct nrParams *) params;
  double wSum = W(0.0);
  for (criptic::IdxType i=0; i<par->n; i++) {
    criptic::Real q = par->dist[i] / h;
    if (q > kernelCutoff) break;
    wSum += W(q);
  }
  return wSum - hFac3;
}

// This function returns dr/dh = sum_i d/dh W(q_i, h)
// = -1/h sum_i q W'(q_i, h)
double ngbKernel::hDeriv(double h, void *params) {
  struct nrParams *par = (struct nrParams *) params;
  criptic::Real deriv = 0.0;
  for (criptic::IdxType i=0; i<par->n; i++) {
    criptic::Real q = par->dist[i] / h;
    if (q > kernelCutoff) break;
    deriv += -q * dW(q);
  }
  if (deriv == 0)
    std::cout << "error" << std::endl;
  return deriv / h;
}
  
void ngbKernel::hResidAndDeriv(double h, void *params,
			       double *resid, double *deriv) {
  struct nrParams *par = (struct nrParams *) params;
  criptic::Real w0 = W(0.0);
  *resid = w0 - hFac3;
  *deriv = 0.0;
  for (criptic::IdxType i=0; i<par->n; i++) {
    criptic::Real q = par->dist[i] / h;
    if (q > kernelCutoff) break;
    criptic::Real w, dw;
    WdW(q, w, dw);
    *resid += w;
    *deriv += -q * dw;
  }
  *deriv /= h;
  if (*deriv == 0.0)
    std::cout << "error" << std::endl;
}

