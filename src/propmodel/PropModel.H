// This class defines the propagation model, which is the thing that
// turns a set of gas and CR properties into a set of propagation
// parameters describing the diffusion coefficients along the field,
// across the field, and in momentum space, along with the streaming
// speed. Here we define a generic interface, which can then be
// further sub-classed to describe different physical models.

#ifndef _PROPMODEL_H_
#define _PROPMODEL_H_

#include <vector>
#include "../utils/constants.H"
#include "../utils/types.H"
#include "../ParmParser.H"
#include "../CRPacket.H"
#include "../gasmodel/GasModel.H"

/////////////////////////////////////////////////////////////////
// The generic propmodel class. This is pure abstract class.
/////////////////////////////////////////////////////////////////

class PropModel {

 public:

  // The constructor and destructor
  PropModel() { }
  virtual ~PropModel() { }

  // Function to return the propagation parameters given the gas
  // and CR properties
  virtual void getPropParams(const criptic::Real *x,
			     const criptic::Real t,
			     const GasModel& gas,
			     const CRPacket &packet,
			     const criptic::Real *qty,
			     criptic::Real &kPar,
			     criptic::Real &kPerp,
			     criptic::Real &kPP,
			     criptic::Real &vStr) const = 0;

};

#endif
// _PROPMODEL_H_
