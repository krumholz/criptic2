// This class represents a model where CRs travel purely diffusively,
// and the diffusion coefficients are powerlaws in the CR momentum.

#include "PropModelDiffPowerlaw.H"

using namespace std;
using namespace criptic;

// Constructor
PropModelDiffPowerlaw::PropModelDiffPowerlaw(const ParmParser &pp) {

  // Read parameters from input file
  pp.get("cr.kPar0", kPar0);
  kParIdx = 0.0;
  pp.query("cr.kParIdx", kParIdx);
  pp.get("cr.kPerp0", kPerp0);
  kPerpIdx = 0.0;
  pp.query("cr.kPerpIdx", kPerpIdx);
  pp.get("cr.kPP0", kPP0);
  kPPIdx = 0.0;
  pp.query("cr.kPPIdx", kPPIdx);
}


// Function to return propagation model parameters
void PropModelDiffPowerlaw::getPropParams(const Real *x,
					  const Real t,
					  const GasModel &gas,
					  const CRPacket &packet,
					  const Real *qty,
					  Real &kPar,
					  Real &kPerp,
					  Real &kPP,
					  Real &vStr) const {

  // Set diffusion coefficients
  kPar = kPar0 * pow(packet.p, kParIdx);
  kPerp = kPerp0 * pow(packet.p, kPerpIdx);
  kPP = kPP0 * pow(packet.p, kPPIdx);

  // Set streaming speed
  vStr = 0.0;
}
