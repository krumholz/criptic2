// This class represents a model where CRs stream in one direction, at
// a speed that is a fixed multiple of the ion Alfven speed.

#include "PropModelStreamFixedDir.H"
#include "../utils/constants.H"

using namespace std;
using namespace criptic;

// Constructor
PropModelStreamFixedDir::PropModelStreamFixedDir(const ParmParser &pp) {

  // Read parameters from input file
  pp.get("cr.vStream", vStr_vA);
}


// Function to return propagation model parameters
void PropModelStreamFixedDir::getPropParams(const Real *x,
					    const Real t,
					    const GasModel &gas,
					    const CRPacket &packet,
					    const Real *qty,
					    Real &kPar,
					    Real &kPerp,
					    Real &kPP,
					    Real &vStr) const {

  // Set diffusion coefficients to zero
  kPar = kPerp = kPP = 0.0;

  // Set streaming speed to multiple of ion Alfven speed
  Real rhoi;
  gas.ionDensity(x, t, rhoi);
  Real B[3];
  gas.magField(x, t, B);
  Real Bmag = sqrt(B[0]*B[0] + B[1]*B[1] + B[2]*B[2]);
#ifdef CRIPTIC_UNITS_MKS
  Real vA = Bmag / sqrt(constants::mu0 * rhoi);
#else
  Real vA = Bmag / sqrt(4.0 * M_PI * rhoi);
#endif
  vStr = vStr_vA * vA;
}
