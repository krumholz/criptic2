#include "MPIUtil.H"
#include "../globalControls.H"
#include "../CRTree.H"
#include <unistd.h>

namespace MPIUtil {

  // Declare global variables
  int nRank;         // Total number of MPI ranks
  int myRank;        // My rank number
  bool IOProc;       // Is this the processor that does I/O?
  int log2nRank;     // log_2 (nRank)
#ifdef ENABLE_MPI
  // MPI data types to pass around various structs
  MPI_Datatype CRPacketType;
  MPI_Datatype topNodeType;
  MPI_Datatype ngbSearchReqType;
  MPI_Datatype ngbSearchRespType;
#endif
}

// Method to set up an MPI run
void MPIUtil::setup(int *argc, char **argv[]) {

#ifdef ENABLE_MPI
  // Version if MPI is turned on
    
  // Call the MPI library setup method
#ifdef _OPENMP
  int threadSupportLev;
  MPI_Init_thread(argc, argv, MPI_THREAD_FUNNELED, &threadSupportLev);
  if (threadSupportLev != MPI_THREAD_FUNNELED) {
    std::cerr << "MPIUtil::setup: this MPI implementation does not appear to "
	      << "support funneled threads!" << std::endl;
    exit(2);
  }
#else
  MPI_Init(argc, argv);
#endif

#ifndef NDEBUG
#ifdef MPI_DEBUG_PAUSE
  {
    int dummy = 0;
    char hostname[256];
    gethostname(hostname, sizeof(hostname));
    printf("PID %d on %s ready for attach\n", getpid(), hostname);
    fflush(stdout);
    while (dummy == 0)
      sleep(5);
  }
#endif
#endif
  
  // Set the global variables
  MPI_Comm_size(MPI_COMM_WORLD, &nRank);
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
  IOProc = (myRank == 0);
  for (log2nRank = 0;
       nRank > (1 << log2nRank);
       log2nRank++) { };

  // Define the MPI data types we will need to pass things around
  {
    // CR packet data type
    int nBlock = 8;
    int blockLengths[8] = { 1, 1, 1, 1, 1, 1, 1, 1 };
    MPI_Datatype types[8] = { MPI_INT, MPI_SZ, MPI_SZ,
			      MPI_RL, MPI_RL, MPI_RL, MPI_RL, MPI_RL };
    MPI_Aint offsets[8];
    offsets[0] = offsetof(CRPacket, type);
    offsets[1] = offsetof(CRPacket, src);
    offsets[2] = offsetof(CRPacket, uniqueID);
    offsets[3] = offsetof(CRPacket, tInj);
    offsets[4] = offsetof(CRPacket, wInj);
    offsets[5] = offsetof(CRPacket, p);
    offsets[6] = offsetof(CRPacket, w);
    offsets[7] = offsetof(CRPacket, gr);
    MPI_Type_create_struct(nBlock, blockLengths, offsets, types,
			   &CRPacketType);
    MPI_Type_commit(&CRPacketType);
  }
  
  {
    // Top node data type
    int nBlock = 3;
    int blockLengths[3] = { 2, 5, 1 };
    MPI_Datatype types[3] = { MPI_UNSIGNED_LONG_LONG, MPI_SZ, MPI_RL };
    MPI_Aint offsets[3];
    offsets[0] = offsetof(CRTree::topNode, size);
    offsets[1] = offsetof(CRTree::topNode, count);
    offsets[2] = offsetof(CRTree::topNode, cost);
    MPI_Type_create_struct(nBlock, blockLengths, offsets, types,
			   &topNodeType);
    MPI_Type_commit(&topNodeType);
  }

  {
    // Neighbor search request data type
    int nBlock = 5;
    int blockLengths[5] = { 1, 1, 3, 1, 1 };
    MPI_Datatype types[5] = { MPI_SZ, MPI_INT, MPI_RL, MPI_RL, MPI_RL };
    MPI_Aint offsets[5];
    offsets[0] = offsetof(ngbSearchRequest, target);
    offsets[1] = offsetof(ngbSearchRequest, numNgb);
    offsets[2] = offsetof(ngbSearchRequest, searchCtr);
    offsets[3] = offsetof(ngbSearchRequest, distMax);
    offsets[4] = offsetof(ngbSearchRequest, eMin);
    MPI_Type_create_struct(nBlock, blockLengths, offsets, types,
			   &ngbSearchReqType);
    MPI_Type_commit(&ngbSearchReqType);
  }
  
  {
    // Neighbor search response data type
    int nBlock = 4;
    int blockLengths[4] = { 1, 3*controls::numNgb,
			    controls::numNgb, controls::numNgb };
    MPI_Datatype types[4] = { MPI_SZ, MPI_RL, MPI_RL, CRPacketType };
    MPI_Aint offsets[4];
    offsets[0] = offsetof(ngbSearchResponse, target);
    offsets[1] = offsetof(ngbSearchResponse, x);
    offsets[2] = offsetof(ngbSearchResponse, dist);
    offsets[3] = offsetof(ngbSearchResponse, packets);
    MPI_Type_create_struct(nBlock, blockLengths, offsets, types,
			   &ngbSearchRespType);
    MPI_Type_commit(&ngbSearchRespType);
  }
    
#else

  // Version if MPI is turned off
  nRank = 1;
  myRank = 0;
  log2nRank = 0;
  IOProc = true;
#endif
}

// Method to halt a run
void MPIUtil::haltRun(std::string  msg, errCodes errcode) {
  if (IOProc) std::cerr << msg << std::endl;
#ifdef ENABLE_MPI
  MPI_Abort(MPI_COMM_WORLD, errcode);
#endif
  exit(errcode);
}

// MPI barrier method
void MPIUtil::barrier() {
#ifdef ENABLE_MPI
  MPI_Barrier(MPI_COMM_WORLD);
#endif
}

// Method to finalize
void MPIUtil::shutdown() {
#ifdef ENABLE_MPI
  MPI_Finalize();
#endif
}

  
