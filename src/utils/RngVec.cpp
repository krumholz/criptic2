// Implementation of the RngVec class

#include "RngVec.H"
#if defined (_OPENMP)
#   include <omp.h>
#endif
#include <ctime>
#include <fstream>
#include <fcntl.h>
#include <unistd.h>

using namespace std;

// Constructor
RngVec::RngVec(const ParmParser &pp) {
#ifdef _OPENMP
  // Implementation for openMP
#pragma omp parallel
  {
    const int nthreads = omp_get_num_threads();
    const int ithread = omp_get_thread_num();

#pragma omp single
    // Allocate space
    r.resize(nthreads);

    // Get seed source
    pcg_extras::seed_seq_from<std::random_device> seed_source;

    // Set up generator for this thread
    r[ithread] = new pcg64(seed_source);
  }
#else
  // Non-threaded implementation

  // Allocate space
  r.resize(1);
  
  // Get seed source
  pcg_extras::seed_seq_from<std::random_device> seed_source;

  // Set up generator
  r[0] = new pcg64(seed_source);
#endif

  // If reading the seed from a file, do that now
  if (pp.rngFile().length() > 0) {
    ifstream rngFile;
    rngFile.open(pp.rngFile().c_str(), ios::binary);
    rngFile >> (*this);
  }

  // If we are saving the rng state and this is not a restart, write
  // out the initial state
  int saveRngState;
  if (!pp.query("output.rng", saveRngState))
    saveRngState = 1;
  if (saveRngState && !pp.isRestart()) {
    string rngInitName;
    if (!pp.query("output.chkname", rngInitName))  
      rngInitName = "criptic_";
    rngInitName += "init.rng";
    ofstream rngfile;
    rngfile.open(rngInitName, ios::binary);
    rngfile << (*this);
    rngfile.close();
  }
}

// Destructor; this frees all rng's
RngVec::~RngVec() {
#ifdef _OPENMP
#pragma omp parallel
  {
    const int ithread = omp_get_thread_num();
    delete r[ithread];
  }
#else
  delete r[0];
#endif
}

