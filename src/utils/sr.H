// This header file contains convenience functions to do special
// relativistic transformations between mass, momentum, energy,
// etc. In the following functions, variable names correspond to
// physical quantities as follows:
//    m = rest mass in units of m_p
//    p = momentum in units of m_p c
//    T = kinetic energy in units of m_p c^2
//    E = total energy (kinetic + rest) in units of m_p c^2
//    v = velocity in units of c
//    gamma = Lorentz factor
// All these functions use appropriate series expansions to ensure
// reasonable behavior in both the non-relativistic and
// ultra-relativistic limits. Errors should be << 10^-6 regardless of
// the value of gamma.

#ifndef _SR_H_
#define _SR_H_

#include <cmath>
#include "types.H"
#include "particleTypes.H"

namespace sr {

  // Versions that take mass as an argument
  constexpr criptic::Real gamma_from_p(criptic::Real m, criptic::Real p) {
    criptic::Real pm = p / m;
    if (pm < 1.0e-6)
      return 1.0 + pm*pm / 2.0;
    else
      return std::sqrt(1.0 + pm*pm);
  }
  
  constexpr criptic::Real gamma_from_T(criptic::Real m, criptic::Real T) {
    return 1.0 + T/m;
  }
  
  constexpr criptic::Real gamma_from_E(criptic::Real m, criptic::Real E) {
    return E/m;
  }
  constexpr criptic::Real p_from_gamma(criptic::Real m, criptic::Real gamma) {
    criptic::Real g = gamma-1.0;
    if (g < 1.0e-6)
      return m * (std::sqrt(2.0)*g + (g/2.0)*std::sqrt(g/2.0));
    else
      return m * std::sqrt(gamma*gamma - 1.0);
  }
  
  constexpr criptic::Real p_from_T(criptic::Real m, criptic::Real T) {
    criptic::Real Tm = T / m;
    if (Tm < 1.0e-6)
      return std::sqrt(2.0*Tm) + m * 
	(Tm/2.0) * std::sqrt(Tm/2.0);
    else
      return m * std::sqrt(Tm * (2.0+Tm));
  }
  
  constexpr criptic::Real p_from_E(criptic::Real m, criptic::Real E) {
    criptic::Real Tm = E / m - 1.0;
    if (Tm < 1.0e-6)
      return std::sqrt(2.0*Tm) + m * 
	(Tm/2.0) * std::sqrt(Tm/2.0);
    else
      return m * std::sqrt(Tm * (2.0+Tm));
  }
  
  constexpr criptic::Real T_from_gamma(criptic::Real m, criptic::Real gamma) {
    return m * (gamma-1.0);
  }
  
  constexpr criptic::Real T_from_p(criptic::Real m, criptic::Real p) {
    criptic::Real pm = p / m;
    if (pm < 1.0e-6)
      return p*p / (2.0*m) * (1.0 - pm*pm/4.0);
    else
      return m * (std::sqrt(1.0 + pm*pm) - 1.0);
  }

  constexpr criptic::Real E_from_gamma(criptic::Real m, criptic::Real gamma) {
    return m * gamma;
  }
  
  constexpr criptic::Real E_from_p(criptic::Real m, criptic::Real p) {
    criptic::Real pm = p / m;
    if (pm < 1.0e-6)
      return m + p*p / (2.0*m) * (1.0 - pm*pm/4.0);
    else
      return m * std::sqrt(1.0 + pm*pm);
  }

  constexpr criptic::Real dT_dp(criptic::Real m, criptic::Real p) {
    criptic::Real pm = p / m;
    if (pm < 1.0e-6)
      return pm * (1.0 - pm*pm/2.0);
    else
      return pm / std::sqrt(1.0 + pm*pm);
  }
  
  constexpr criptic::Real v_from_p(criptic::Real m, criptic::Real p) {
    criptic::Real pm = p / m;
    if (pm < 1.0e-6)
      return pm - 0.5 / (pm*pm*pm);
    else
      return 1.0 / std::sqrt(1.0 + 1.0/(pm*pm));
  }

  // Versions that take particle type as an argument
  constexpr criptic::Real gamma_from_p(partTypes::pType pt,
				       criptic::Real p) {
    return gamma_from_p(partTypes::mass[pt], p);
  }
  constexpr criptic::Real gamma_from_T(partTypes::pType pt,
				       criptic::Real T) {
    return gamma_from_T(partTypes::mass[pt], T);
  }
  constexpr criptic::Real p_from_gamma(partTypes::pType pt,
				       criptic::Real gamma) {
    return p_from_gamma(partTypes::mass[pt], gamma);
  }
  constexpr criptic::Real p_from_T(partTypes::pType pt,
				   criptic::Real T) {
    return p_from_T(partTypes::mass[pt], T);
  }
  constexpr criptic::Real T_from_gamma(partTypes::pType pt,
				       criptic::Real gamma) {
    return T_from_gamma(partTypes::mass[pt], gamma);
  }
  constexpr criptic::Real T_from_p(partTypes::pType pt,
				   criptic::Real p) {
    return T_from_p(partTypes::mass[pt], p);
  }
  constexpr criptic::Real dT_dp(partTypes::pType pt,
				criptic::Real p) {
    return dT_dp(partTypes::mass[pt], p);
  }
  constexpr criptic::Real v_from_p(partTypes::pType pt,
				   criptic::Real p) {
    return v_from_p(partTypes::mass[pt], p);
  }
}

#endif
// _SR_H_
